
List from https://kde.org/applications/office/
and https://invent.kde.org/office
(2021-01)

Done
===========

* Okular ---> kde.scm
* KOrganizer ---> pim
* KAddressBook ---> pim


Not yet done
========================

* Calindori - Calendar
* Calligra --- shall this become a single package?
  - Calligra Sheets - Spreadsheet
  - Calligra Stage - Presentation
  - Calligra Words - Word Processor

* (Calligra) Plan - Project Management

* KBibTeX - BibTeX Editor
* KEXI - Visual database apps builder
* KMyMoney - Personal Finance Manager
* KTimeTracker - Personal Time Tracker
* Kile - LaTeX Frontend
* Kongress
* Lokalize - Computer-Aided Translation System
* Skrooge - Manage your money
* Tellico - Collection Manager


Additionally in https://invent.kde.org/office
===================================================

* Alkimia - Library used by KDE finance applications
* Knipptasch - Personal accounting application
* KBibTeX Test Set - Testing data for KBibTex


"Temporary removed from Calligra"
=================================

flow - aka Kivio


Build elswhere
===================

* KDE Itinerary ---> pim
* KMail ---> pim
* Kontact ---> pim
* SieveEditor ---> network
* Trojitá ---> network
* KEuroCalc ---> utilities


Old? package sources
=============================

in /stable/applications:
 libksieve-19.08.2.tar.xz
 pim-sieve-editor-19.08.2.tar.xz
 pim-data-exporter-19.08.2.tar.xz
 pimcommon-19.08.2.tar.xz

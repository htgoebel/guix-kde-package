#!/usr/bin/env bash
# License: AGPLv3
#
# Usage:
#    sh ./00-test-gui-app.sh PKGNAME
#
# This is called by 00-add.sh to run the application in a container.
#
# To ease debugging, the application is run under "strace", trace-output to be
# found in "$PWD/trace.log"
#

APP=$1

IN_CONTAINER=1

if [ -n "$IN_CONTAINER" ] ; then

    ./pre-inst-env \
	guix environment --container --share=/tmp/.X11-unix \
	--ad-hoc bash strace coreutils dbus $APP \
	-- bash -c "DISPLAY=:0 strace -o trace.log -e trace=file -f dbus-launch $APP"

else

    ./pre-inst-env guix environment --ad-hoc $APP -- $APP

fi

(define-public kpimtextedit
  (package
    (name "kpimtextedit")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kpimtextedit-" version ".tar.xz"))
      (sha256
       (base32 "06iz9l8z708kdivzibqkgdrbvw7kfd2lr2grf3v9l6gfl3qs1kbw"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
;; /gnu/store/i02q63l5f6b8v0cw2482vnfnwbsjfh43-kpimtextedit-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdesignerplugin" ,kdesignerplugin)
      ("kemoticons" ,kemoticons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("ksyntaxhighlighting" ,ksyntaxhighlighting)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("sonnet" ,sonnet)))
;; left-overs from CMakeLists.txt:
;; find_package(Grantlee5 "5.1" CONFIG REQUIRED)
;;
;;     find_package(Qt5 TextToSpeech)
    (home-page "")
    (synopsis1 "Kdepimlibs 4 core library ")
    (synopsis2 "library that provides a textedit with PIM-specific features")
    (description1 "A library for PIM-specific text editing utilities.")
    (description2 "")
    (license ) ;; TODO: check this
))
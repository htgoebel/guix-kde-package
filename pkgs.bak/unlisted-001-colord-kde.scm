(define-public colord-kde
  (package
    (name "colord-kde")
    (version "0.5.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/colord-kde/" version "/src/colord-kde-" version ".tar.xz"))
      (sha256
       (base32 "0brdnpflm95vf4l41clrqxwvjrdwhs859n7401wxcykkmw4m0m3c"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unlisted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/bvaklbcqxr6jwfj09w9vx3xh1zmx5xzm-colord-kde-0.5.0.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("plasma-framework" ,plasma-framework)
      ("qtbase" ,qtbase)
      ("qtx11extras" ,qtx11extras)))
;; left-overs from CMakeLists.txt:
;; find_package(X11)
;; find_package(XCB COMPONENTS XCB RANDR)
;; pkg_check_modules(LCMS2 REQUIRED lcms2)
    (home-page "")
    (synopsis1 "Interfaces and session daemon to colord for KDE ")
    (synopsis2 "")
    (description1 "Interfaces and session daemon to colord for KDE.")
    (description2 "")
    (license ) ;; TODO: check this
))
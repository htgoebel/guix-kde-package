;; part of Kontact, but separate package?
(define-public kjots
  (package
    (name "kjots")
    (version "5.0.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kjots/" version "/src/kjots-" version ".tar.xz"))
      (sha256
       (base32 "01kz96aiyj7szakbzgfswg81dh3sf42fcl56r3m8qgd3l76lv150"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/i32dwm8zrizgx5xvc9cqiam4h4z275v2-kjots-5.0.1.tar.xz
    (inputs
     `(("kbookmarks" ,kbookmarks)
      ("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kparts" ,kparts)
      ("kxmlgui" ,kxmlgui)))
;; left-overs from CMakeLists.txt:
;; find_packagE()
;; find_package(KF5 Akonadi ${KDEPIMLIBS_LIB_VERSION})
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiNotes ${KDEPIMLIBS_LIB_VERSION})
;; find_package(KF5 PimTextEdit ${PIMTEXTEDIT_LIB_VERSION})
;; find_package(KF5 KontactInterface ${KONTACTINTERFACE_LIB_VERSION})
;; find_package(Grantlee5 "5.0" CONFIG REQUIRED)
;; find_package(Xsltproc)
    (home-page "")
    (synopsis1 "Note Taker for KDE ")
    (synopsis2 "note-taking utility")
    (description1 "KJots organises all of your notes into separate books
Features :
   - Multiple books handled
   - Each book has many named pages
   - Books and pages can be rearranged by drag-and-drop
   - Keyboard shortcuts are available for many functions
   - Automatic saving means your notes are safe from loss.")
    (description2 "")
    (license ) ;; TODO: check this
))
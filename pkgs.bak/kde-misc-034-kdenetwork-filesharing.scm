(define-public kdenetwork-filesharing
  (package
    (name "kdenetwork-filesharing")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdenetwork-filesharing-" version ".tar.xz"))
      (sha256
       (base32 "0345wq7ayahfd2jlpgfs18c7nrdp9gn9yxig2x75pspqmb5pgxh7"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/99g1zfxkcsadi002n01kxc9cg3wkr70y-kdenetwork-filesharing-16.12.3.tar.xz
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(PackageKitQt5 0.9.5)
    (home-page "")
    (synopsis1 "Kdenetwork filesharing ")
    (synopsis2 "")
    (description1 "Networking applications for the KDE 4.

- kdict: graphical client for the DICT protocol
- kit: AOL instant messenger client, using the TOC protocol
- kpf: public fileserver applet
- krfb: Desktop Sharing server, allow others to access your desktop via VNC
- krdc: a client for Desktop Sharing and other VNC servers.")
    (description2 "")
    (license ) ;; TODO: check this
))
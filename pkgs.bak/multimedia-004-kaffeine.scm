(define-public kaffeine
  (package
    (name "kaffeine")
    (version "2.0.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kaffeine/" version "/src/kaffeine-" version "-1.tar.xz"))
      (sha256
       (base32 "132s0ppv4nijhl86mz65n3x25smy3zsk2ly7z638npj7x4qhjqg1"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Multimedia")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/fg0033zsdpqd85bs7s8k5w5wzaxswz0d-kaffeine-2.0.5-1.tar.xz
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtx11extras" ,qtx11extras)
      ("solid" ,solid)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 # QCommandLineParser, QStringLiteral # QApplication)
;; find_package(KF5 # KAboutData # KLocalizedString # KMessageBox # KActonCollection # KFileWidget)
;; find_package(X11 REQUIRED)
;; find_package(VLC 1.2 REQUIRED)
;;   find_package(Libdvbv5)
;; kdoctools_install(po)
    (home-page "http://kaffeine.kde.org")
    (synopsis1 "Media Player for KDE4 ")
    (synopsis2 "versatile media player for KDE")
    (description1 "Kaffeine is a KDE4 Multi Engine Media Player.
---
Kaffeine is a KDE4 Multi Engine Media Player.")
    (description2 "Kaffeine is a media player for KDE. While it supports multiple Phonon
backends, its default backend is Xine, giving Kaffeine a wide variety of
supported media types and letting Kaffeine access CDs, DVDs, and
network streams easily.

Kaffeine can keep track of multiple playlists simultaneously, and supports
autoloading of subtitle files for use while playing video.")
    (license license:unknown) ;; TODO: check this
))
(define-public kwordquiz
  (package
    (name "kwordquiz")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kwordquiz-" version ".tar.xz"))
      (sha256
       (base32 "1r8q2d6j7bq8jdr4cl9maapadzg7yp0zldjxkcqg08ldwsrrsnqj"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/jj169zn5rjp2i2y0qmvwzdzxmhg3wm3r-kwordquiz-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcrash" ,kcrash)
      ("kdelibs4support" ,kdelibs4support)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kitemviews" ,kitemviews)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kxmlgui" ,kxmlgui)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(LibKEduVocDocument REQUIRED)
    (home-page "https://projects.kde.org/projects/kde/kdeedu/kwordquiz")
    (synopsis1 "A general purpose flash card program ")
    (synopsis2 "flashcard learning program")
    (description1 "KWordQuiz is a general purpose flash card program. It can be used for
vocabulary learning and many other subjects. If you need more advanced
language learning features, please try KVocTrain.")
    (description2 "KWordQuiz is a general purpose flashcard program, typically used for
vocabulary training.

KWordQuiz can open vocabulary data in various formats, including the kvtml
format used by KDE programs such as Parley, the WQL format used by
WordQuiz for Windows, the xml.gz format used by Pauker, and CSV text.

This package is part of the KDE education module.")
    (license fdl-1.2) ;; TODO: check this
))
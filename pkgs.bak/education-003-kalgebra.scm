(define-public kalgebra
  (package
    (name "kalgebra")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kalgebra-" version ".tar.xz"))
      (sha256
       (base32 "0qhini5gm41dlyham4zzqgz6r11s2rfwwphb81xvwp1bgn8q2rqb"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/i4mb8jpir1kdaxnc1bjh0rlba8i43cw1-kalgebra-16.12.3.tar.xz
    (inputs
     `(("kconfigwidgets" ,kconfigwidgets)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)
      ("qtwebkit" ,qtwebkit)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 5.2)
;; find_package(Analitza5 REQUIRED)
;; find_package(OpenGL)
;; find_package(Curses)
;; find_package(Readline)
    (home-page "http://edu.kde.org/")
    (synopsis1 "MathML-based graph calculator ")
    (synopsis2 "algebraic graphing calculator")
    (description1 "KAlgebra is a mathematical calculator based on the content markup language
MathML. It is capable of making simple MathML operations
(arithmetic and logical), and representing 2D and 3D graphs. It is
actually not necessary to know MathML to use KAlgebra.")
    (description2 "KAlgebra is a algebraic graphing calculator with support for 3D graphing and
MathML markup language.

This package is part of the KDE education module.")
    (license bsd-3) ;; TODO: check this
))
(define-public kde-dev-utils
  (package
    (name "kde-dev-utils")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kde-dev-utils-" version ".tar.xz"))
      (sha256
       (base32 "0svbl7yd5vz285gaymxy5mz0ll6fviamrbd6fjhj7rc4qx1gjgin"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/6n0y1mhbgfifmbx4ml220fid4pc8jyd5-kde-dev-utils-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
    (home-page "")
    (synopsis1 "K Desktop Environment - Software Development Kit ")
    (synopsis2 "")
    (description1 "Software Development Kit for the K Desktop Environment.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public amarok
  (package
    (name "amarok")
    (version "2.8.90")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/amarok/" version "/src/amarok-" version ".tar.xz"))
      (sha256
       (base32 "19zrpss5dfzhbqbxcq3a7xk40hbxkqgc3hp6krk2n68dnyd3cmxh"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Multimedia")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/3zl562dwsz267mbvidr81dzdrkv6gyrr-amarok-2.8.90.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; " OFF)
;; find_package(Taglib REQUIRED)
;;  { TagLib::ASF::Tag tag; return 0;}" TAGLIB_ASF_FOUND)
;;  { TagLib::MP4::Tag tag(0, 0); return 0;}" TAGLIB_MP4_FOUND)
;;  { char *s; Mod::Tag tag; Mod::File modfile(s); S3M::File s3mfile(s);
;; IT::File itfile(s); XM::File xmfile(s); return 0; }" TAGLIB_MOD_FOUND)
;;  { char *s; TagLib::Ogg::Opus::File opusfile(s); return 0;}" TAGLIB_OPUS_FOUND)
;; find_package(Taglib-Extras)
;;  # we need a c-compatible name for the include file
;; find_package( Qt4 4.8.3 COMPONENTS QtCore QtGui QtScript QtSvg QtXml QtWebKit REQUIRED )
;; find_package( KDE4 4.8.4 REQUIRED )
;;   # Require C++11
;; find_package( QCA2 )
;; macro_optional_find_package( LibLastFm )
;;     macro_ensure_version( ${LIBLASTFM_MIN_VERSION} ${LIBLASTFM_VERSION} LIBLASTFM_FOUND )
;; macro_optional_find_package( FFmpeg )
;;     macro_optional_find_package( LibOFA )
;;     find_package(MySQLAmarok REQUIRED)
;;     find_package(ZLIB REQUIRED)
;;     macro_optional_find_package(QJSON)
;;     macro_optional_find_package( Mygpo-qt 1.0.7 QUIET )
;;         find_package(Ipod)
;;             macro_ensure_version(${IPOD_MIN_VERSION} ${IPOD_VERSION} IPOD_FOUND)
;;         macro_optional_find_package(GDKPixBuf)
;;     macro_optional_find_package(Mtp)
;;         find_package(CURL)
;;         find_package(LibXml2)
;;         macro_optional_find_package(OpenSSL)
;;         macro_optional_find_package(Libgcrypt)
;;         find_package(Loudmouth)
;;     find_package(GObject)
;;     find_package(GLIB2)
;;     find_program( CLAMZ_FOUND clamz PATH )
;;     find_package(PythonInterp)
;;         ENABLE_TESTING()
;; " FORCE)
    (home-page "http://amarok.kde.org")
    (synopsis1 "A powerful media player for KDE4 ")
    (synopsis2 "easy to use media player based on the KDE Platform")
    (description1 "Feature Overview

* Music Collection:
You have a huge music library and want to locate tracks quickly? Let amaroK's
powerful Collection take care of that! It's a database powered music store,
which keeps track of your complete music library, allowing you to find any
title in a matter of seconds.

* Intuitive User Interface:
You will be amazed to see how easy amaroK is to use! Simply drag-and-drop files
into the playlist. No hassle with complicated  buttons or tangled menus.
Listening to music has never been easier!

* Streaming Radio:
Web streams take radio to the next level: Listen to thousands of great radio
stations on the internet, for free! amaroK provides excellent streaming
support, with advanced features, such as displaying titles of the currently
playing songs.

* Context Browser:
This tool provides useful information on the music you are currently listening
to, and can make listening suggestions, based on your personal music taste. An
innovate and unique feature.

* Visualizations:
amaroK is compatible with XMMS visualization plugins. Allows you to use the
great number of stunning visualizations available on the net. 3d visualizations
with OpenGL are a great way to enhance your music experience.")
    (description2 "Amarok is a powerful music player with an intuitive interface. It makes
playing the music you love and discovering new music easier than ever before
and it looks good doing it! Amarok is based on the powerful Qt4 / KDE4
Platform and nicely integrates with KDE desktop.

Much work has been invested into integrating Amarok 2 with various Web
services:
  - Ampache
  - Jamendo Service
  - Last.fm
  - Librivox
  - MP3tunes
  - Magnatune
  - OPML Podcast Directory

Amarok comes with a lot of features including but not limited to:
  - Scripts - enhance your Amarok experience with community developed scripts.
  - Dynamic Playlists - create playlists that automatically update.
  - Context View - customize interface with the Plasma powered Context View.
  - PopUp Dropper - simplify drag&drop actions with revolutionary menu system.
  - Multiple Language Translations
  - Collection Management - organizing your music collection has never been
    easier with Amarok's powerful tagging, renaming, and sorting abilities.
  - Database Importing - import collections from Amarok 1.4 or iTunes.
  - Scriptable Services - integrate other web services into Amarok.")
    (license license:gpl2+) ;; TODO: check this
))
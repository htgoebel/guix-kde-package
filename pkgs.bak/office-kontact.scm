(define-public kontakt
  (package
    (name "kontakt")
    (version "0.2.3")
    (source
     (origin
      (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version "/src/"
                           name "-" version ".tar.xz"))
       (sha256
        (base32 "127c236s264ckaqjibr2fhg6ynnd7qmkpvi1q000nrxqwg63zqzn"))))
    (properties `((tags . ("Desktop" "KDE" "Office"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
    (home-page "https://kde.org/applications/office/org.kde.kontact")
    (synopsis "Personal information manager")
    (description "")
    (license unknown) ;; TODO: check this
))

(define-public kdesdk-kioslaves
  (package
    (name "kdesdk-kioslaves")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdesdk-kioslaves-" version ".tar.xz"))
      (sha256
       (base32 "17zqp43a1266616h3g6yccjmfgwni6lr8lz4rrvfda275vvwbshj"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/2n20srzxr091nakxv47mh0s1dgndgrnx-kdesdk-kioslaves-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; macro_optional_find_package(SVN)
;; macro_optional_find_package(Perl 5.10)
    (home-page "")
    (synopsis1 "K Desktop Environment - Software Development Kit ")
    (synopsis2 "")
    (description1 "Software Development Kit for the K Desktop Environment.")
    (description2 "")
    (license ) ;; TODO: check this
))
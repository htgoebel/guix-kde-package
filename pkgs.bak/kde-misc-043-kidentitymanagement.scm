(define-public kidentitymanagement
  (package
    (name "kidentitymanagement")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kidentitymanagement-" version ".tar.xz"))
      (sha256
       (base32 "1b23cwdhc7rv260kmn4akff3jfa21hk49p0kp8p0mf6nw6qahbvp"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/46p49wirb8x20bn3kxfq2gwl01xsg40k-kidentitymanagement-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kemoticons" ,kemoticons)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("ktextwidgets" ,ktextwidgets)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 PimTextEdit ${PIMTEXTEDIT_LIB_VERSION})
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
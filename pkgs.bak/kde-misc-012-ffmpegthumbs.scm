(define-public ffmpegthumbs
  (package
    (name "ffmpegthumbs")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ffmpegthumbs-" version ".tar.xz"))
      (sha256
       (base32 "1scyyd85rs21rv3ghcxv7pw3aa2nl703gi4dpikbsd7wjmxixzq9"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/3hzzg9p7j2rzqpgh5vri2z5iydvdrwy7-ffmpegthumbs-16.12.3.tar.xz
    (inputs
     `(("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(FFmpeg COMPONENTS AVCODEC AVFORMAT SWSCALE)
    (home-page "http://www.kde.org/")
    (synopsis1 "Video thumbnail generator for KDE4 file managers ")
    (synopsis2 "video thumbnail generator using ffmpeg")
    (description1 "FFmpegThumbs is a video thumbnails implementation for KDE4 based on
FFmpegThumbnailer.

This thumbnailer uses FFmpeg to decode frames from the video files,
so supported video formats depend on the configuration flags of ffmpeg.

This thumbnailer was designed to be as fast and lightweight as possible.")
    (description2 "FFMpegThumbs is a video thumbnail generator for KDE file managers
like Dolphin and Konqueror.  It enables them to show preview images
of video files using FFMpeg.

This package is part of the KDE multimedia module.")
    (license gpl2+) ;; TODO: check this
))
(define-public kpat
  (package
    (name "kpat")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kpat-" version ".tar.xz"))
      (sha256
       (base32 "04qvv7q7rlsiyff2isy18h2fbz2lnljal5spq5qg9zl6v8hx6qzm"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/8kc8i81izidbldmsldi98dls1vfzs9dc-kpat-16.12.3.tar.xz
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("knewstuff" ,knewstuff)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 KDEGames)
    (home-page "http://games.kde.org/")
    (synopsis1 "Several patience card games ")
    (synopsis2 "solitaire card games")
    (description1 "KPatience is a relaxing card sorting game.
To win the game a player has to arrange a single deck of cards in certain order
amongst each other.")
    (description2 "KPatience is a collection of fourteen solitaire card games, including Klondike,
Spider, and FreeCell.

This package is part of the KDE games module.")
    (license fdl-1.2+) ;; TODO: check this
))
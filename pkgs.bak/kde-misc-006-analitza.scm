(define-public analitza
  (package
    (name "analitza")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/analitza-" version ".tar.xz"))
      (sha256
       (base32 "0ap3sf8bw9f58pzw3zy6w60apd4ccc47zs3v61x4kp1g81rn265w"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/zwz7i5zzhwyhghqjccxpcw5z2akf2wnk-analitza-16.12.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; #FindCurses seems to need that on the CI
;; find_package(OpenGL)
;;
;; find_package(Eigen3) # find and setup Eigen3 if available
    (home-page "http://edu.kde.org/")
    (synopsis1 "Runtime library for analitza ")
    (synopsis2 "common files for Analitza")
    (description1 "The analitza library will let you add mathematical features to your program.
This pakage provide the runtime library for analitza.")
    (description2 "Analitza is a library to parse and work with mathematical expressions. This
library is being used by KAlgebra and Cantor and may be used in other
programs.

This package contains a console interface for Analitza.

This package is part of the KDE education module.")
    (license bsd-3) ;; TODO: check this
))
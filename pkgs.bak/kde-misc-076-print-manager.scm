(define-public print-manager
  (package
    (name "print-manager")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/print-manager-" version ".tar.xz"))
      (sha256
       (base32 "1mjzqq7yhm1s4jbr6nhy1i1lm27134j6g7b04mmzk3hbgd8lkr99"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/zxf8h3718ckz1ysc7fszmbq799l79ybx-print-manager-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("plasma-framework" ,plasma-framework)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; left-overs from CMakeLists.txt:
;; find_package(CUPS "1.5" REQUIRED)
    (home-page "http://www.kde.org/")
    (synopsis1 "Printing management for KDE ")
    (synopsis2 "printer configuration and monitoring tools")
    (description1 "Printer Applet is a system tray utility that shows current print jobs,
shows printer warnings and errors and shows when printers that have
been plugged in for the first time are being auto-configured by
hal-cups-utils.")
    (description2 "This package provides a KDE configuration module and a Plasma widget for
installing and configuring printers, and monitoring printers, print jobs and
print queues.

This package is part of the KDE utilities module.")
    (license gpl2+) ;; TODO: check this
))
(define-public ktp-call-ui
  (package
    (name "ktp-call-ui")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-call-ui-" version ".tar.xz"))
      (sha256
       (base32 "1aq0s4v972kskjnq720364y971iyr0m6pj42fw5rpkl7j8vfg1rd"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/nmxl9pjyqgd6pfb6pwd7fqn0pnvldqjw-ktp-call-ui-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("knotifications" ,knotifications)
      ("kxmlgui" ,kxmlgui)
      ("phonon" ,phonon)
      ("qtdeclarative" ,qtdeclarative)))
;; left-overs from CMakeLists.txt:
;; find_package(KTp REQUIRED)
;; find_package(TelepathyQt5 REQUIRED)
;; find_package(TelepathyQt5Farstream REQUIRED)
;; find_package(Qt5 GStreamer)
;; find_package(PkgConfig REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public step
  (package
    (name "step")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/step-" version ".tar.xz"))
      (sha256
       (base32 "0pyvhlfrklc2xxylb0nlnpqx5xi0pp4zyb3xbzj87wmvcw7v5n6r"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/n3bcc2cxr6kyc9b6fplrq2g4if0ajlap-step-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("kdelibs4support" ,kdelibs4support)
      ("khtml" ,khtml)
      ("knewstuff" ,knewstuff)
      ("kplotting" ,kplotting)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 5.2)
;; find_package(Eigen3 3.2.2 REQUIRED)
;; find_package(GSL)
;; find_package(Qalculate)
;;  # CACHE BOOL "Enable QT-powered features for StepCore")
    (home-page "http://edu.kde.org/")
    (synopsis1 "Interactive physical simulator ")
    (synopsis2 "interactive physical simulator for KDE")
    (description1 "Step is an interactive physical simulator. It works like this:
you place some bodies on the scene, add some forces such as gravity
or springs, then click "Simulate" and Step shows you how your scene
will evolve according to the laws of physics. You can change every
property of bodies/forces in your experiment (even during simulation)
and see how this will change evolution of the experiment. With Step
you can not only learn but feel how physics works !")
    (description2 "With Step you can not only learn but feel how physics works. You place some
bodies on the scene, add some forces such as gravity or springs, then click
"Simulate" and Step shows you how your scene will evolve according to the
laws of physics. You can change every property of bodies/forces in your
experiment (even during simulation) and see how this will change evolution
of the experiment.

This package is part of the KDE education module.")
    (license bsd-3) ;; TODO: check this
))
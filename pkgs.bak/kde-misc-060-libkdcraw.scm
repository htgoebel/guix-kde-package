(define-public libkdcraw
  (package
    (name "libkdcraw")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkdcraw-" version ".tar.xz"))
      (sha256
       (base32 "03ag6vzdj5n7zbb8yb9k84ckm1zwp2i9qqrsfn2mmmhypwknpg4w"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/vfilvj8q7c9122kf1irdjaax1qkjg3hj-libkdcraw-16.12.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(LibRaw ${LIBRAW_MIN_VERSION} REQUIRED)
    (home-page "")
    (synopsis1 "C++ interface around LibRaw library ")
    (synopsis2 "RAW picture decoding library")
    (description1 "Libkdcraw is a C++ interface around LibRaw library used to decode RAW
picture files. More information about LibRaw can be found at
http://www.libraw.org.")
    (description2 "")
    (license ) ;; TODO: check this
))
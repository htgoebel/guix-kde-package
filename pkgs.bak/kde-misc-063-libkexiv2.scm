(define-public libkexiv2
  (package
    (name "libkexiv2")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkexiv2-" version ".tar.xz"))
      (sha256
       (base32 "02fr10prqmpaqqlcj8hf5h4b3vhhiwkfsbpzlag64n5764x1hl3f"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/bl2mvx4dx3hbl5fhcv9h9kx6gy9ddlca-libkexiv2-16.12.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Exiv2 ${EXIV2_MIN_VERSION} REQUIRED)
    (home-page "")
    (synopsis1 "Wrapper around exiv2 library ")
    (synopsis2 "Qt like interface for the libexiv2 library")
    (description1 "Libkexiv2 is a wrapper around Exiv2 library to manipulate pictures
metadata as EXIF/IPTC and XMP.")
    (description2 "")
    (license ) ;; TODO: check this
))
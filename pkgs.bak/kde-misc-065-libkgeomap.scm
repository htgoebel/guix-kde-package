(define-public libkgeomap
  (package
    (name "libkgeomap")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkgeomap-" version ".tar.xz"))
      (sha256
       (base32 "1zz2w7cbabyrvzvw2ph38mxw7khyhjzg86na2lcywla7japlbsd9"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/81vdy60b6xy2fwbx5qw9hc1k3407r3v8-libkgeomap-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)
      ("qtwebkit" ,qtwebkit)))
;; left-overs from CMakeLists.txt:
;; find_package(Marble CONFIG REQUIRED)
    (home-page "")
    (synopsis1 "Runtime library for digikam ")
    (synopsis2 "")
    (description1 "Librairie File needed by digikam

Libkgeomap is a wrapper around world map components as Marble, OpenstreetMap and
GoogleMap,for browsing and arranging photos on a map.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public skrooge
  (package
    (name "skrooge")
    (version "2.5.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/skrooge/skrooge-" version ".tar.xz"))
      (sha256
       (base32 "03ayrrr7rrj1jl1qh3sgn56hbi44wn4ldgcj08b93mqw7wdvpglp"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Office")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)
      ("qttools" ,qttools)))
;; /gnu/store/i37bzmsh1zhd59spz4ypja25k34v1gji-skrooge-2.5.0.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdesignerplugin" ,kdesignerplugin)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("kjobwidgets" ,kjobwidgets)
      ("knewstuff" ,knewstuff)
      ("kparts" ,kparts)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtscript" ,qtscript)
      ("qtsvg" ,qtsvg)
      ("qtwebkit" ,qtwebkit)))
;; left-overs from CMakeLists.txt:
;; OPTION(SKG_BUILD_TEST "Build the test" ON)
;; OPTION(SKG_DESIGNER   "Build designer library" ON)
;;
;; FIND_PACKAGE()
;; FIND_PACKAGE()
;;   FIND_PACKAGE()
;;   FIND_PACKAGE(KF5 # Tier 3)
;; FIND_PACKAGE(Grantlee5 0.5 REQUIRED)
;; FIND_PACKAGE(KF5 # Tier 1 # Tier 1 # Tier 1 # Tier 1 # Tier 1 # Tier 1 # Tier 1 # Tier 2 # Tier 2 # Tier 3 # Tier 3 # Tier 3 # Tier 3 # Tier 3 # Tier 3 # Tier 3)
;; FIND_PACKAGE()
;; OPTION(SKG_CIPHER "Build in sqlcipher mode" ON)
;;     FIND_PACKAGE(PkgConfig REQUIRED)
;;     PKG_CHECK_MODULES(SQLCIPHER REQUIRED sqlcipher)
;;     FIND_PACKAGE(Sqlite 3.7.0 REQUIRED)
;; SET_PACKAGE_PROPERTIES(Sqlcipher PROPERTIES DESCRIPTION "Support for encryption"
;;                        URL "https://www.zetetic.net/sqlcipher/"
;;                        TYPE REQUIRED)
;; FIND_PROGRAM(SKG_BASH bash)
;; REMOVE_DEFINITIONS(-DQT_NO_CAST_FROM_ASCII)
;;
;;
;;
;; FEATURE_SUMMARY(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
    (home-page "http://skrooge.org/")
    (synopsis1 "Personal Finance Management Tool ")
    (synopsis2 "personal finance manager for KDE")
    (description1 "Skrooge is a personal finance management tool for KDE4, with the aim of
being highly intuitive, while providing powerful functions such as
graphics, persistent Undo/Redo, infinite category levels, and much more...")
    (description2 "Skrooge allows you to manage your personal finances. It is intended to be used
by individuals who want to keep track of their incomes, expenses and
investments. Its philosophy is to stay simple and intuitive.

Here is the list of Skrooge main features:
 * QIF, CSV, KMyMoney, Skrooge,  import/export
 * OFX, QFX, GnuCash, Grisbi, HomeBank import
 * Advanced Graphical Reports
 * Several tabs to help you organize your work
 * Infinite undo/redo
 * Instant filtering on operations and reports
 * Infinite categories levels
 * Mass update of operations
 * Scheduled operations
 * Track refund of your expenses
 * Automatically process operations based on search conditions
 * Multi currencies
 * Dashboard")
    (license unknown) ;; TODO: check this
))
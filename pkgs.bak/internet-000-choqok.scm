(define-public choqok
  (package
    (name "choqok")
    (version "1.6")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/choqok/" version "/src/choqok-" version ".0.tar.xz"))
      (sha256
       (base32 "03ri4y1wzyqlixnhczsls5gmy7jzzm67bb5gz8bav51ngc32fxca"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Internet")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/hhcd3rk5clhyhy19ijjbiy5f3vblhymw-choqok-1.6.0.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kemoticons" ,kemoticons)
      ("kglobalaccel" ,kglobalaccel)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("ktextwidgets" ,ktextwidgets)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("sonnet" ,sonnet)))
;; left-overs from CMakeLists.txt:
;; find_package(Qca-qt5 REQUIRED)
;; find_package(QtOAuth 2.0.1 REQUIRED)
    (home-page "http://choqok.gnufolks.org")
    (synopsis1 "KDE Micro-Blogging Client ")
    (synopsis2 "KDE micro-blogging client")
    (description1 "Choqok is a Free/Open Source micro-blogging client for K Desktop")
    (description2 "Choqok is a fast, efficient and simple to use micro-blogging client for KDE.
It currently supports the twitter.com and identi.ca microblogging services.

Other notable features include:
   * Support for user + friends time-lines.
   * Support for @Reply time-lines.
   * Support for sending and receiving direct messages.
   * Twitpic.com integration.
   * The ability to use multiple accounts simultaneously.
   * Support for search APIs for all services.
   * KWallet integration.
   * Support for automatic shortening urls with more than 30 characters.
   * Support for configuring status lists appearance.")
    (license unknown) ;; TODO: check this
))
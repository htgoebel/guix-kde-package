(define-public ktp-common-internals
  (package
    (name "ktp-common-internals")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-common-internals-" version ".tar.xz"))
      (sha256
       (base32 "0g3vmds5f7wmffp9rv915bll8ky7qf3lz172ymc6q9i1xvghp215"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/d9c3j932slb1h1m08ca1pxcyj5ngb327-ktp-common-internals-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kpeople" ,kpeople)
      ("ktexteditor" ,ktexteditor)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; left-overs from CMakeLists.txt:
;; # SO 9 is for 15.08 release
;; find_package (TelepathyQt5 ${REQUIRED_TPQT_VERSION} REQUIRED)
;; find_package (TelepathyQt5Service ${REQUIRED_TPQT_VERSION} REQUIRED) #used for the otr-proxy
;; find_package (TelepathyLoggerQt)
;; find_package (KAccounts)
;; find_package (AccountsQt5 1.10 CONFIG)
;; find_package (SignOnQt5 8.55 CONFIG)
;; find_package (LibOTR 4.0.0)
;; find_package (Libgcrypt)
;; find_package (telepathy-accounts-signon)
    (home-page "https://projects.kde.org/projects/extragear/network/telepathy/ktp-common-internals")
    (synopsis1 "")
    (synopsis2 "KDE Telepathy common internal library")
    (description1 "")
    (description2 "Internal library which consists of the most reused
parts across KDE Telepathy.

This package contains development headers for
the ktp-common-internals library.

You should not use this library for developing applications
outside of the KDE Telepathy project umbrella. This package
is only provided for compiling the rest of the components.")
    (license gpl2+) ;; TODO: check this
))
(define-public kmime
  (package
    (name "kmime")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kmime-" version ".tar.xz"))
      (sha256
       (base32 "0gzbh0g075ml5x0qy4zd1cg1qygdsnssl5ahk9pcgc0fik4p9j20"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/yik64d3c9cap8lsz0zjrwlmr8v73hprv-kmime-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("ki18n" ,ki18n)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "Akonadi MIME handling library
library for handling MIME data")
    (description1 "A client access library for using the Akonadi PIM data server.
---
A library for MIME handling.")
    (description2 "")
    (license ) ;; TODO: check this
))
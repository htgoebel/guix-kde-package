(define-public audiocd-kio
  (package
    (name "audiocd-kio")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/audiocd-kio-" version ".tar.xz"))
      (sha256
       (base32 "0bmmi2rxx368nss8ciwj32dspc1ailc0shm06ynmhw3slrplnx1y"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/5bz6n4l5cmh0lvckbl6x3aghfam66jxp-audiocd-kio-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("kio" ,kio)))
;; left-overs from CMakeLists.txt:
;; # handled by release scripts
;; find_package(KF5 Cddb)
;; find_package(KF5 CompactDisc)
;; find_package(Cdparanoia REQUIRED)
;; check_struct_has_member("struct cdrom_drive" "ioctl_device_name" "cdda_interface.h" CDDA_IOCTL_DEVICE_EXISTS)
    (home-page "http://www.kde.org/")
    (synopsis1 "")
    (synopsis2 "transparent audio CD access for applications using the KDE Platform")
    (description1 "")
    (description2 "This package includes the audiocd KIO plugin, which allows applications using
the KDE Platform to read audio from CDs and automatically convert it into other
formats.

This package is part of the KDE multimedia module.")
    (license lgpl2.0+) ;; TODO: check this
))
(define-public ktp-contact-list
  (package
    (name "ktp-contact-list")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-contact-list-" version ".tar.xz"))
      (sha256
       (base32 "05asblclq6sjd3d9faaj2ya37srf4lhg8jx705aw13wd5d6pk75w"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/n1aal37gqvdw48hxq8xmnxzx9wcmn3xn-ktp-contact-list-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kpeople" ,kpeople)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package (Qt5 5.0)
;; find_package (TelepathyQt5 0.9.3 REQUIRED)
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
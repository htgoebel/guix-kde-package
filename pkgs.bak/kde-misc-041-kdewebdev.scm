(define-public kdewebdev
  (package
    (name "kdewebdev")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdewebdev-" version ".tar.xz"))
      (sha256
       (base32 "0r9qsm3idfhm7aglik7whzcwjjn572gwmlcgxy2cvpn15nqa344l"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/0imdybz11krmzm4pi4am62i5f6qzyhga-kdewebdev-16.08.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
    (home-page "")
    (synopsis1 "A web editor for the KDE Desktop Environment ")
    (synopsis2 "")
    (description1 "A web editor for the KDE Desktop Environment.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public artikulate
  (package
    (name "artikulate")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/artikulate-" version ".tar.xz"))
      (sha256
       (base32 "113k6c0yrir61j258gn9n6k7fifa6g9g8wxf3zq18jy3liwdl97s"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/ms05yvwv3v3r4z58dzm6b35a6s5i2mkv-artikulate-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("knewstuff" ,knewstuff)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtxmlpatterns" ,qtxmlpatterns)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "Learning software to improve pronunciation skills ")
    (synopsis2 "Language learning application")
    (description1 "Artikulate is a learning software that helps improving pronunciation skills
by listening to native speakers.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public marble
  (package
    (name "marble")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/marble-" version ".tar.xz"))
      (sha256
       (base32 "08dykrmiq1jk9yv83sjj6s3gps56bw0hxjbvb90bzd1g0kh0c82j"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/izfak1grdf6pjk1svvb6cv095x8bxhd3-marble-16.12.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtlocation" ,qtlocation)
      ("qtmultimedia" ,qtmultimedia)
      ("qtsvg" ,qtsvg)
      ("qtwebkit" ,qtwebkit)))
;; left-overs from CMakeLists.txt:
;; elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel" AND NOT WIN32)
;;
;;
;;
;; ENABLE_TESTING()
;; elseif(APPLE)
;; elseif(CMAKE_SYSTEM_NAME STREQUAL Android)
;;  # Linux / bsd etc...
;; ?")
;;         REMOVE_DEFINITIONS( -DQT_NO_DEBUG )
    (home-page "http://edu.kde.org/")
    (synopsis1 "A virtual globe and world atlas ")
    (synopsis2 "globe and map widget")
    (description1 "Marble is a Virtual Globe and World Atlas that you can use to learn more
about Earth: You can pan and zoom around and you can look up places and
roads. A mouse click on a place label will provide the respective
Wikipedia article.")
    (description2 "Marble is a generic geographical map widget and framework for KDE
applications. The Marble widget shows the earth as a sphere but does not
require hardware acceleration.  A minimal set of geographical data is
included, so it can be used without an internet connection.

This package is part of the KDE education module.")
    (license fdl-1.2) ;; TODO: check this
))
(define-public kldap
  (package
    (name "kldap")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kldap-" version ".tar.xz"))
      (sha256
       (base32 "0a7pm9dzcvyyznqs4apwdl6dpg87qhxcr3a06grjwxhqvfdl52na"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/vjzlcg178symch5bcri29y0f21pivkfa-kldap-16.12.3.tar.xz
    (inputs
     `(("kcompletion" ,kcompletion)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Mbox ${MBOXLIB_VERSION})
;; find_package(Ldap)
;; find_package(Sasl2)
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "library for accessing LDAP")
    (description1 "This library provides an API for IMAP support.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public symboleditor
  (package
    (name "symboleditor")
    (version "2.0.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/symboleditor/" version "/src/SymbolEditor-" version ".tar.bz2"))
      (sha256
       (base32 "053f9n4pzv0k57icycwd50pplw6h34w8wvjjj49bxg79r8xfmwvw"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/5imy5k7idlpwnmjkp4kw70c7dl2iaykr-SymbolEditor-2.0.0.tar.bz2
    (inputs
     `(("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package (Doxygen)
;; find_package (SharedMimeInfo)
;; qt5_wrap_ui (SymbolEditor_SRCS
;;     ui/EditorConfigPage.ui
;; )
;;     update_xdg_mimetypes (${XDG_MIME_INSTALL_DIR})
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
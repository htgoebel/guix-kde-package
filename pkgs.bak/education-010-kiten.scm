(define-public kiten
  (package
    (name "kiten")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kiten-" version ".tar.xz"))
      (sha256
       (base32 "0skwgv3v79p5z1livjbdsg7i18ky8vc49z53dmgsgbziqvs0s2y4"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/gxap89ik1lpxas2j8pzz101b0gvnm5an-kiten-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "http://edu.kde.org/")
    (synopsis1 "A Japanese reference/learning tool ")
    (synopsis2 "Japanese reference and study aid for KDE")
    (description1 "Kiten is a Japanese reference/learning tool.

Kiten features:
* Search with english keyword, Japanese reading, or a Kanji string on a
  list of EDICT files.
* Search with english keyword, Japanese reading, number of strokes, grade
  number, or a Kanji on a list of KANJIDIC files.
* Comes with all necessary files.
* Very fast.
* Limit searches to only common entries.
* Nested searches of results possible.
* Compact, small, fast interface.
* Global KDE keybindings for searching highlighted strings.
* Learning dialog. (One can even open up multiple ones and have them sync
  between each other.)
* Browse Kanji by grade.
* Add Kanji to a list for later learning.
* Browse list, and get quizzed on them.")
    (description2 "Kiten is a collection of Japanese reference tools and study aids for KDE,
including a Japanese/English dictionary, Kanji dictionary, and Kanji quiz.

This package is part of the KDE education module.")
    (license bsd-3) ;; TODO: check this
))
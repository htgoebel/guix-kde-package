(define-public kimap
  (package
    (name "kimap")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kimap-" version ".tar.xz"))
      (sha256
       (base32 "1jlva17hy500jpb5mg6m3vrcy6mqikcy8m1pgy68d2ip0m93rb5f"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/qg82xjs43w0dxy6gvwjklzad8ns5h4w4-kimap-16.12.3.tar.xz
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Mime ${KMIME_LIBS_VERSION})
;; find_package(Sasl2)
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "library for handling IMAP data")
    (description1 "This library provides an API for IMAP support.")
    (description2 "")
    (license ) ;; TODO: check this
))
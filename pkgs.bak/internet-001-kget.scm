(define-public kget
  (package
    (name "kget")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kget-" version ".tar.xz"))
      (sha256
       (base32 "0h8nklsl6gddwzgjig5cwp463s96ffn5688zjlsyx4hphnvbj8kb"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Internet")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/z19hdkagxm7cqlwwsljajskq19y2hxl7-kget-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; macro_optional_find_package(NepomukCore)
;; macro_optional_find_package(NepomukWidgets)
;; macro_optional_find_package(Sqlite)
;; macro_optional_find_package(QCA2)
;; macro_optional_find_package(QGpgme)
;; macro_optional_find_package(KTorrent)
;; macro_optional_find_package(LibMms)
;;
;;     find_package(Soprano)
;;     find_package(SharedDesktopOntologies 0.4)
;;     find_package(Boost REQUIRED)
;; macro_optional_find_package(KDE4Workspace)
;;     NEPOMUK2_ADD_ONTOLOGY_CLASSES(
;;         kgetcore_SRCS
;;         ONTOLOGIES
;;         ${CMAKE_CURRENT_SOURCE_DIR}/kget_history.trig
;;     )
;; qt4_add_dbus_adaptor(kgetcore_SRCS dbus/org.kde.kget.transfer.xml dbus/dbustransferwrapper.h DBusTransferWrapper)
;; qt4_add_dbus_adaptor(kgetcore_SRCS dbus/org.kde.kget.verifier.xml dbus/dbusverifierwrapper.h DBusVerifierWrapper)
;; kde4_add_library(kgetcore SHARED ${kgetcore_SRCS})
;; set_target_properties(kgetcore PROPERTIES VERSION ${GENERIC_LIB_VERSION} SOVERSION ${GENERIC_LIB_SOVERSION} )
;; qt4_add_dbus_adaptor(kget_SRCS dbus/org.kde.kget.main.xml dbus/dbuskgetwrapper.h DBusKGetWrapper)
    (home-page "http://www.kde.org/")
    (synopsis1 "Versatile and user-friendly download manager ")
    (synopsis2 "download manager")
    (description1 "KGet is a versatile and user-friendly download manager.
Features:
  - Downloading files from FTP and HTTP(S) sources.
  - Pausing and resuming of downloading files, as well as the ability to restart
    a download.
  - Tells lots of information about current and pending downloads.
  - Embedding into system tray.
  - Integration with the Konqueror web browser.
  - Metalink support which contains multiple URLs for downloads, along with
    checksums and other information.")
    (description2 "KGet is an advanced download manager with support for Metalink and Bittorrent.
Downloads are added to the list, where they can be paused, queued, or
scheduled for later.

This package is part of the KDE networking module.")
    (license lgpl2.1+) ;; TODO: check this
))
(define-public ktp-send-file
  (package
    (name "ktp-send-file")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-send-file-" version ".tar.xz"))
      (sha256
       (base32 "1m7cj3q4lzj8qj2cla6wm1crpjid77b3f3yywri167f1zd4p51z6"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/4whschmsn95y72b39w3n1qjr0c123a8b-ktp-send-file-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 5.0)
;; find_package(KTp REQUIRED)
;; qt5_wrap_ui(KTP_SEND_FILE_SRCS mainwindow.ui)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public minuet
  (package
    (name "minuet")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/minuet-" version ".tar.xz"))
      (sha256
       (base32 "15c0mw8qj7r6192h39lvsmiy3pqlfqzari4qjg2x44j5gq02ab3c"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/nsj4s3k4qg80h5nq8kxdklyssr6z5r0q-minuet-16.12.3.tar.xz
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtquickcontrols2" ,qtquickcontrols2)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
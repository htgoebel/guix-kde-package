(define-public kmag
  (package
    (name "kmag")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kmag-" version ".tar.xz"))
      (sha256
       (base32 "1llv9vd1557h4lz2sdd1wjlqb9wzrk9jxn4731iac2b5wdwpihii"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/qkfh4n91vm0g2irrcxz9xb3kp7kbkb79-kmag-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; macro_optional_find_package(QAccessibilityClient)
    (home-page "http://www.kde.org")
    (synopsis1 "Screen magnifier for KDE 4 ")
    (synopsis2 "screen magnifier tool")
    (description1 "KMag is a small utility for Linux to magnify a part of the screen. KMag is very
useful for people with visual disabilities and for those working in the fields
of image analysis, web development etc.")
    (description2 "KDE's screen magnifier tool.

You can use KMagnifier to magnify a part of the screen just as you would use
a lens to magnify a newspaper fine-print or a photograph.  This application is
useful for a variety of people: from researchers to artists to web-designers to
people with low vision.

This package is part of the KDE accessibility module.")
    (license GFDL-NIV-1.2+) ;; TODO: check this
))
(define-public kalarmcal
  (package
    (name "kalarmcal")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kalarmcal-" version ".tar.xz"))
      (sha256
       (base32 "064sihcsbdi1w6viv5gq6sw2m8r7x3nn1hl2aji1109pf5913vbr"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/knf8dmirv272jg100yl9iw8smgk70h66-kalarmcal-16.12.3.tar.xz
    (inputs
     `(("kdelibs4support" ,kdelibs4support)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Akonadi ${AKONADI_LIB_VERSION})
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGER_LIB_VERSION})
;; find_package(KF5 Holidays ${HOLIDAY_LIB_VERSION})
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "library for handling kalarm calendar data")
    (description1 "This library provides an API for KAlarm alarms.")
    (description2 "")
    (license ) ;; TODO: check this
))
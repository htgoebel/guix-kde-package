;; Dated 2010
(define-public kcoloredit
  (package
    (name "kcoloredit")
    (version "2.0.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/extragear/kcoloredit-" version "-kde4.4.0.tar.bz2"))
      (sha256
       (base32 "0vlyyzy0l0qszbdgj3a50rm77p046x35pra37bmhv6f6ql020w9p"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/p4n4gbgzgz9ynidqhn2x3xsmyig4r4wy-kcoloredit-2.0.0-kde4.4.0.tar.bz2
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package ( KDE4 REQUIRED )
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public kde-gtk-config
  (package
    (name "kde-gtk-config")
    (version "2.2.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kde-gtk-config/" version "/src/kde-gtk-config-" version ".tar.xz"))
      (sha256
       (base32 "11aw86jcjcg3ywnzrxy9x8dvd73my18k0if52fnvyvmb75z0v2cw"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unlisted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/7jjq7fcrw7in4jfknibv35r6dm71fl3h-kde-gtk-config-2.2.1.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis1 "GTK2 and GTK3 configurator for KDE ")
    (synopsis2 "")
    (description1 "Configuration dialog to adapt GTK applications appearance to your taste
under KDE. Among its many features, it lets you:
- Choose which theme is used for GTK2 and GTK3 applications.
- Tweak some GTK applications behaviour.
- Select what icon theme to use in GTK applications.
- Select GTK applications default fonts.
- Easily browse and install new GTK2 and GTK3 themes.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public kdgantt2
  (package
    (name "kdgantt2")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdgantt2-" version ".tar.xz"))
      (sha256
       (base32 "01p3cqqhghvx42mrmxkrsm7cp23972gi86sagjw070scw71fpv0c"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/qhjz8jbzmgbqmxjwhbv6yk6iia0dnkxn-kdgantt2-16.08.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "KDEPIM 4 library ")
    (synopsis2 "Gantt chart library")
    (description1 "KDEPIM 4 library.")
    (description2 "")
    (license ) ;; TODO: check this
))
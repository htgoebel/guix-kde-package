(define-public keurocalc
  (package
    (name "keurocalc")
    (version "1.2.3")
    (source
     (origin
      (method url-fetch)
       (uri (string-append "mirror://kde/stable/keurocalc/"
                           name "-" version ".tar.xz"))
      (sha256
       (base32 "127c236s264ckaqjibr2fhg6ynnd7qmkpvi1q000nrxqwg63zqzn"))))
    (properties `((tags . ("Desktop" "KDE" "Office"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
    (home-page "http://opensource.bureau-cornavin.com/keurocalc/")
    (synopsis "Universal currency converter and calculator")
    (description " KEurocalc is a universal currency converter and calculator.
It can convert from and to many currencies, either with a fixed conversion
rate or a variable conversion rate.  It directly downloads the latest variable
rates through the Internet, e.g. from the European Central Bank and Time
Genie.")
    (license license:gpl2)
))

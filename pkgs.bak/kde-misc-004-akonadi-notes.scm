(define-public akonadi-notes
  (package
    (name "akonadi-notes")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/akonadi-notes-" version ".tar.xz"))
      (sha256
       (base32 "0405nkz9ri9qlclgvwycvdx1gsms2pm1fn6xhvyrj2v4s8brb3r7"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/dfbv0gr8h3mbrk9kvaihw7sx0klv6cnx-akonadi-notes-16.12.3.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Mime ${KMIMELIB_VERSION})
;; find_package(KF5 Akonadi)
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "Akonadi notes access library")
    (description1 "A client access library for using the Akonadi PIM data server.")
    (description2 "")
    (license ) ;; TODO: check this
))
;; Alternative notifications for KDE Plasma Desktop
(define-public colibri
  (package
    (name "colibri")
    (version "0.3.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/colibri/colibri-" version ".tar.bz2"))
      (sha256
       (base32 "1zsym71x3q1k98bhmh4q2dbslz3r0kiajr4dkjjmw0ks3aph4d1r"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unlisted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/2nvcriqw5fm5sikiq02jc649myw1c4ir-colibri-0.3.0.tar.bz2
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 4.4 REQUIRED)
    (home-page "http://kde-apps.org/content/show.php?content=117147")
    (synopsis1 "Alternative notifications for KDE4 ")
    (synopsis2 "passive notification system for kde4")
    (description1 "Colibri provides an alternative to KDE4 Plasma notifications.

Colibri notifications look lighter and are completely passive: they do not
provide any buttons. You may or may not like this.

Since they are completely passive, they smoothly fade away when you mouse over
them, allowing you to interact with any window behind them.

They also do not stack each others: if multiple notifications happen, they will
be shown one at a time.

Colibri can be configured from its System Settings module.
If you need help follow the setup howto at
http://gitorious.org/colibri/pages/SetupHowto.")
    (description2 "colibri is a passive notification system for KDE4 desktop

Colibri notifications look lighter and are completely passive:
they do not provide any buttons. You may or may not like this.
Since they are completely passive, they smoothly fade away when
you mouse over them, allowing you to interact with any window behind them.

They also do not stack each others: if multiple notifications happen,
they will be shown one at a time.")
    (license unknown) ;; TODO: check this
))
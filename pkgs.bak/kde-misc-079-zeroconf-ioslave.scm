(define-public zeroconf-ioslave
  (package
    (name "zeroconf-ioslave")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/zeroconf-ioslave-" version ".tar.xz"))
      (sha256
       (base32 "0p7kfx7bg3yvd44vg608s2znzfahkihan67zgyf3gmjllbzvp55b"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/b6hn408d97h48mkf6l8rrc1l2m8p4asi-zeroconf-ioslave-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
    (home-page "")
    (synopsis1 "DNS-SD Service Discovery Monitor ")
    (synopsis2 "")
    (description1 "DNS-SD Service Discovery Monitor.")
    (description2 "")
    (license ) ;; TODO: check this
))
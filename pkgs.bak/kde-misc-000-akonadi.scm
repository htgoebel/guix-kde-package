(define-public akonadi
  (package
    (name "akonadi")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/akonadi-" version ".tar.xz"))
      (sha256
       (base32 "00sbchj3yjbqdjckrciv2c7j9i7kc5yqvyvbmjlxacbkq80a4j7w"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
;; /gnu/store/0nxfrzlrxm569jnzlb8wlj3ba844wicd-akonadi-16.12.3.tar.xz
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kdesignerplugin" ,kdesignerplugin)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemmodels" ,kitemmodels)
      ("kitemviews" ,kitemviews)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 Private)
;;
;; find_package(Boost ${Boost_MINIMUM_VERSION})
;; find_package(SharedMimeInfo ${SMI_MIN_VERSION} REQUIRED)
;; find_program(XSLTPROC_EXECUTABLE xsltproc)
;; find_program(MYSQLD_EXECUTABLE NAMES mysqld
;;     PATHS /usr/sbin /usr/local/sbin /usr/libexec /usr/local/libexec /opt/mysql/libexec /usr/mysql/bin /opt/mysql/sbin
;;     DOC "The mysqld executable path. ONLY needed at runtime"
;; )
;; find_path(POSTGRES_PATH NAMES pg_ctl
;;     HINTS /usr/lib${LIB_SUFFIX}/postgresql/8.4/bin
;;           /usr/lib${LIB_SUFFIX}/postgresql/9.0/bin
;;           /usr/lib${LIB_SUFFIX}/postgresql/9.1/bin
;;           DOC "The pg_ctl executable path. ONLY needed at runtime by the PostgreSQL backend"
;; )
;;     find_package(Sqlite ${SQLITE_MIN_VERSION})
;; find_program(XMLLINT_EXECUTABLE xmllint)
;;   # Calls enable_testing().
;; set_default_db_backend(${DATABASE_BACKEND})
;; )
;; update_xdg_mimetypes(${XDG_MIME_INSTALL_DIR})
    (home-page "http://pim.kde.org/akonadi")
    (synopsis1 "An extensible cross-desktop storage service for PIM ")
    (synopsis2 "Akonadi PIM storage service")
    (description1 "An extensible cross-desktop storage service for PIM data and meta data
providing concurrent read, write, and query access.")
    (description2 "Akonadi is an extensible cross-desktop Personal Information Management (PIM)
storage service. It provides a common framework for applications to store and
access mail, calendars, addressbooks, and other PIM data.

This package contains the Akonadi PIM storage server and associated programs.")
    (license unknown) ;; TODO: check this
))
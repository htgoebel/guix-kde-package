(define-public libkcddb
  (package
    (name "libkcddb")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkcddb-" version ".tar.xz"))
      (sha256
       (base32 "0iaascv9a5g8681mjc6b7f2fd8fdi9p3dx8l9lapkpzcygy1fwpc"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/pnbnb50hji96v6v6ynsvm0rj9vzn7hin-libkcddb-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; # handled by release scripts
;; find_package(MusicBrainz5)
    (home-page "")
    (synopsis1 "libkcddb library ")
    (synopsis2 "CDDB library for KDE Platform (runtime)")
    (description1 "A library for retrieving and sending cddb information.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public zanshin
  (package
    (name "zanshin")
    (version "0.4.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "http://files.kde.org/zanshin/zanshin-" version ".tar.bz2"))
      (sha256
       (base32 "1llqm4w4mhmdirgrmbgwrpqyn47phwggjdghf164k3qw0bi806as"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/b9662vw6xhypd3r26sj2pnr46gzy5lgx-zanshin-0.4.1.tar.bz2
    (inputs
     `(("krunner" ,krunner)
      ("kwallet" ,kwallet)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; left-overs from CMakeLists.txt:
;; find_package(Boost REQUIRED)
;;  AND
;; 	    "${CMAKE_CXX_COMPILER_VERSION}" VERSION_LESS "6.0.0.6000058")
;;  # Enable C++14, with cmake >= 3.1
;;  # Don't enable gcc-specific extensions
;;    link_libraries("asan")
;; find_package(KF5 AkonadiCalendar AkonadiNotes AkonadiSearch IdentityManagement KontactInterface Ldap)
;; find_package(KF5 Akonadi "5.1")
    (home-page "")
    (synopsis1 "Getting Things Done application ")
    (synopsis2 "to-do list manager")
    (description1 "A Getting Things Done application which aims at getting your mind like water.")
    (description2 "")
    (license ) ;; TODO: check this
))
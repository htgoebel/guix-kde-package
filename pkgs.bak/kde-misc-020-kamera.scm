(define-public kamera
  (package
    (name "kamera")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kamera-" version ".tar.xz"))
      (sha256
       (base32 "04p19qv5ssf5wlpfqzhqsi8281pzcdjkd0jy177f9r7qgqq4lkgr"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/zqcs503csr65qh59chgw56zgzj1r3kcd-kamera-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Gphoto2 REQUIRED)
    (home-page "https://projects.kde.org/projects/kde/kdegraphics/kamera")
    (synopsis1 "Kamera ioslave ")
    (synopsis2 "digital camera support for KDE applications")
    (description1 "KDE integration for gphoto2 cameras.")
    (description2 "This package allows any KDE application to access and manipulate pictures on
a digital camera.

This package is part of the KDE graphics module.")
    (license gpl2+) ;; TODO: check this
))
(define-public konsole
  (package
    (name "konsole")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/konsole-" version ".tar.xz"))
      (sha256
       (base32 "10k7ryvsssbskpxk04iqx2mrp2a91291r8nzvg1780lrhql5rdj7"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "System")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/q34c0cpq0bnajsjf5w349yivbcb7dsvw-konsole-16.12.3.tar.xz
    (inputs
     `(("kbookmarks" ,kbookmarks)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kinit" ,kinit)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kparts" ,kparts)
      ("kpty" ,kpty)
      ("kservice" ,kservice)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtscript" ,qtscript)))
;; left-overs from CMakeLists.txt:
;; find_package(X11)
    (home-page "http://www.kde.org/")
    (synopsis1 "A terminal emulator similar to xterm for KDE ")
    (synopsis2 "X terminal emulator")
    (description1 "A terminal emulator, similar to xterm, for KDE.")
    (description2 "Konsole is a terminal emulator built on the KDE Platform. It can contain
multiple terminal sessions inside one window using detachable tabs.

Konsole supports powerful terminal features, such as customizable schemes,
saved sessions, and output monitoring.

This package is part of the KDE base applications module.")
    (license unknown) ;; TODO: check this
))
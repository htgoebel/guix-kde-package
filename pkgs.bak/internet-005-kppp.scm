(define-public kppp
  (package
    (name "kppp")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kppp-" version ".tar.xz"))
      (sha256
       (base32 "0lqv95zfzcik8k95a39s6whjsnq6g15amnxlzy898liyxkr499bc"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Internet")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/fh7vk11xj487f4bkxs3c2g872zhymk3n-kppp-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;;
;;
;;
;;
;;
;; qt4_add_dbus_interfaces(kppp_SRCS org.kde.kppp.xml)
;; qt4_add_dbus_adaptor(kppp_SRCS org.kde.kppp.xml kpppwidget.h KPPPWidget)
;;     macro_add_compile_flags(kppp ${KDE4_CXX_FPIE_FLAGS})
;;     macro_add_link_flags(kppp ${KDE4_PIE_LDFLAGS})
    (home-page "http://www.kde.org/")
    (synopsis1 "Internet Dial-Up Tool ")
    (synopsis2 "modem dialer for KDE")
    (description1 "KPPP is used to setup PPP (Point-to-Point Protocol) connections. This is most
useful for connecting with a cell phone "modem" card these days. It is also use
to configure real modem connections.")
    (description2 "KPPP is a modem dialer for connecting to a dial-up Internet Service Provider.
It displays statistics and accounting information to help users keep track of
connection costs.

This package is part of the KDE 4 networking module.")
    (license lgpl2.0+) ;; TODO: check this
))
(define-public ktp-text-ui
  (package
    (name "ktp-text-ui")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-text-ui-" version ".tar.xz"))
      (sha256
       (base32 "0ssxr35vcqjppnppyjxwzrkzvb5sx45fpnvbzsv9md5rnlf2xh1k"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/zgi26h7zp85mhb6z4fpicyjz9i4lgb4g-ktp-text-ui-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kcmutils" ,kcmutils)
      ("kdbusaddons" ,kdbusaddons)
      ("kemoticons" ,kemoticons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kpeople" ,kpeople)
      ("kservice" ,kservice)
      ("ktextwidgets" ,ktextwidgets)
      ("kwebkit" ,kwebkit)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtwebkit" ,qtwebkit)
      ("sonnet" ,sonnet)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 TextToSpeech)
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
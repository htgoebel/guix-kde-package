(define-public kmplayer
  (package
    (name "kmplayer")
    (version "0.12.0b")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kmplayer/0.12/kmplayer-" version ".tar.bz2"))
      (sha256
       (base32 "0wzdxym4fc83wvqyhcwid65yv59a2wvp1lq303cn124mpnlwx62y"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Multimedia")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/63yi43kxy6xacylv294nry7ckxh31lhb-kmplayer-0.12.0b.tar.bz2
    (inputs
     `(("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("kinit" ,kinit)
      ("kio" ,kio)
      ("kparts" ,kparts)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)
      ("qtx11extras" ,qtx11extras)))
;; left-overs from CMakeLists.txt:
;; Include(CheckIncludeFiles)
;; find_package(KF5 MediaPlayer)
;; find_package(X11 REQUIRED)
;;   OPTION(KMPLAYER_BUILT_WITH_CAIRO "Enable Cairo support" ON)
;;   OPTION(KMPLAYER_BUILT_WITH_NPP "Build NPP player" ON)
;;   OPTION(KMPLAYER_BUILT_WITH_EXPAT "Use expat XML parser" OFF)
;;     PKGCONFIG(cairo CAIROIncDir CAIROLinkDir CAIROLinkFlags CAIROCflags)
;;     PKGCONFIG(dbus-glib-1 GLibDBusIncDir GLibDBusLinkDir GLibDBusLinkFlags GLibDBusCflags)
;;     PKGCONFIG(gmodule-2.0 GModuleIncDir GModuleLinkDir GModuleLinkFlags GModuleCflags)
;;       PKGCONFIG(gtk+-x11-2.0 GTKIncDir GTKLinkDir GTKLinkFlags GTKCflags)
;;       PKGCONFIG(gthread-2.0 GThreadIncDir GThreadLinkDir GThreadLinkFlags GThreadCflags)
    (home-page "http://kmplayer.kde.org")
    (synopsis1 "A multimedia mplayer/phonon frontend for KDE ")
    (synopsis2 "media player for KDE")
    (description1 "Kmplayer can play all the audio/video supported by mplayer/phonon from a
local file or URL and be embedded in Konqueror and KHTML.
It also plays DVDs.")
    (description2 "KMPlayer is a simple frontend for MPlayer/FFMpeg/Phonon.

Some features:
 * play DVD/VCD movies (from file or url and from a video device)
 * embed inside konqueror (movie is played inside konqueror)
 * embed inside khtml (movie playback inside a html page)
 * Movie recording using mencoder (part of the mplayer package)
 * No video during recording, but you can always open a new window and play it
 * Broadcasting, http streaming, using ffserver/ffmpeg
 * For TV sources, you need v4lctl (part of the xawtv package)")
    (license license:unknown) ;; TODO: check this
))
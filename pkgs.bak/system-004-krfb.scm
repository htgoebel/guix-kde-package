(define-public krfb
  (package
    (name "krfb")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/krfb-" version ".tar.xz"))
      (sha256
       (base32 "1ijz0zdlkxb9mldgd5a5k7aa2ikmmg023mafmxrjwymsig7ya4hv"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "System")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/11i6680f4kkpw5sr4zrkjc3as161bhxh-krfb-16.12.3.tar.xz
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kdnssd" ,kdnssd)
      ("ki18n" ,ki18n)
      ("knotifications" ,knotifications)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtx11extras" ,qtx11extras)))
;; left-overs from CMakeLists.txt:
;; find_package(X11 REQUIRED)
;; find_package(LibVNCServer REQUIRED)
;;  to be built")
    (home-page "http://www.kde.org/")
    (synopsis1 "Remote Desktop Server ")
    (synopsis2 "Desktop Sharing utility")
    (description1 "KDE Desktop Sharing is a server application that allows you to share your
current session with a user on another machine, who can use a VNC client to view
or even control the desktop.")
    (description2 "KDE Desktop Sharing is a manager for easily sharing a desktop session with
another system.  The desktop session can be viewed or controlled remotely by
any VNC or RFB client, such as the KDE Remote Desktop Connection client.

KDE Desktop Sharing can restrict access to only users who are explicitly
invited, and will ask for confirmation when a user attempts to connect.

This package is part of the KDE networking module.")
    (license lgpl2.1+) ;; TODO: check this
))
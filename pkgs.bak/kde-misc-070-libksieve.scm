(define-public libksieve
  (package
    (name "libksieve")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libksieve-" version ".tar.xz"))
      (sha256
       (base32 "1riqsfl3x4vpwqv7398gj86hnwfzqwfj7d69wsxk3r02jp3xd9i2"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/a2gvmsnn9f2awksn5v1337yw0jnvr9gj-libksieve-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("kwindowsystem" ,kwindowsystem)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Akonadi)
;; find_package(Qt5 WebEngine WebEngineWidgets)
;; find_package(KF5 PimCommon ${PIMCOMMON_LIB_VERSION})
;; find_package(KF5 Libkdepim ${LIBKDEPIM_LIB_VERSION})
;; find_package(KF5 Mime)
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
;; find_package(KF5 MailTransport ${KMAILTRANSPORT_LIB_VERSION})
;; find_package(KF5 PimTextEdit ${KPIMTEXTEDIT_LIB_VERSION})
;; find_package(Sasl2)
    (home-page "")
    (synopsis1 "KSieve runtime library ")
    (synopsis2 "Sieve, the mail filtering language, library")
    (description1 "KSieve (a Sieve parser and interpreter library) runtime library.

Sieve is a programming language that can be used to create filters for email.")
    (description2 "")
    (license ) ;; TODO: check this
))
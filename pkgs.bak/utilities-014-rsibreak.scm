(define-public rsibreak
  (package
    (name "rsibreak")
    (version "0.12.4")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/rsibreak/0.12/rsibreak-" version ".tar.xz"))
      (sha256
       (base32 "0zbj0a04rw3xwhl6llgdd1jvai5lf9wmb5q9qrz83n84cfrdrw2k"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/hrynwp8z48qafqjyzzfbxznmz9n4vzx8-rsibreak-0.12.4.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kidletime" ,kidletime)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("ktextwidgets" ,ktextwidgets)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "Assists in the Recovery and Prevention of Repetitive Strain Injury ")
    (synopsis2 "")
    (description1 "Repetitive Strain Injury is an illness which can occur as a result of
working with a mouse and keyboard. This utility can be used to remind
you to take a break now and then.")
    (description2 "")
    (license ) ;; TODO: check this
))
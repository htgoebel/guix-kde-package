(define-public dolphin-plugins
  (package
    (name "dolphin-plugins")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/dolphin-plugins-" version ".tar.xz"))
      (sha256
       (base32 "18azlmzw33praz4z6lamamj79gyxbbdgz7lp1cimxyddhmacc2x9"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/cx91b0gbf4fqfh51qy4ijp0xx3ryq7xn-dolphin-plugins-16.12.3.tar.xz
    (inputs
     `(("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(DolphinVcs)
    (home-page "http://www.kde.org/")
    (synopsis1 "Plugins for dolphin ")
    (synopsis2 "dolphin VCS plugins")
    (description1 "This package contains some scripts plugins for dolphin")
    (description2 "This package contains plugins that offer integration in Dolphin with the
following version control systems:

 * Bzr
 * Git
 * Mercurial
 * Subversion

This package is part of the KDE Software Development Kit module.")
    (license gpl2+) ;; TODO: check this
))
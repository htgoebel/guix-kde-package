(define-public ktp-filetransfer-handler
  (package
    (name "ktp-filetransfer-handler")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-filetransfer-handler-" version ".tar.xz"))
      (sha256
       (base32 "0ddmrpnh6myl7m3drh3cxx9pp06al98g8mppnysmgswgkwafg6cq"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/w9wy38npwqcz4c3phd3y6silvzpahgcm-ktp-filetransfer-handler-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public libkface
  (package
    (name "libkface")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkface-" version ".tar.xz"))
      (sha256
       (base32 "068xixlw0hfhi3c9nxik2y6xyci1ilwwfq4sjm1paqfszp0f4rq8"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/cpvnq4qkjncvxfrca7q9ziji02rx88qr-libkface-16.12.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; " OFF)
;;     DETECT_OPENCV(${OPENCV_MIN_VERSION} core face highgui objdetect imgproc)
;;     DETECT_OPENCV(${OPENCV_MIN_VERSION} core highgui objdetect contrib legacy imgproc)
    (home-page "")
    (synopsis1 "Runtime library for digikam ")
    (synopsis2 "")
    (description1 "Librairie File needed by digikam

Libkface is a Qt/C++ wrapper around LibFace library to perform face recognition
and detection over pictures.")
    (description2 "")
    (license ) ;; TODO: check this
))
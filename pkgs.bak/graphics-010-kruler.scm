(define-public kruler
  (package
    (name "kruler")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kruler-" version ".tar.xz"))
      (sha256
       (base32 "08w7pb7wyaqnhwvqczxzbrbnm8930wzkl8y4lpimp5mqzb94i8qx"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/a2mgpm86irnab672cbp1mxmk140nb6mv-kruler-16.12.3.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("knotifications" ,knotifications)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtx11extras" ,qtx11extras)))
;; left-overs from CMakeLists.txt:
;; find_package(X11)
;;         find_package(XCB COMPONENTS XCB)
    (home-page "http://www.kde.org/applications/graphics/kruler/")
    (synopsis1 "KDE Screen Ruler ")
    (synopsis2 "screen ruler")
    (description1 "KRuler displays on screen a ruler measuring pixels.
Features :
    - Integrated color picker
    - Change the length of the ruler
    - Change the orientation of the ruler
    - Change the color, transparency and font of the ruler")
    (description2 "KRuler is a tool for measuring the size, in pixels, of items on the screen.

This package is part of the KDE graphics module.")
    (license gpl2+) ;; TODO: check this
))
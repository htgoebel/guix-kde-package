(define-public akonadi-search
  (package
    (name "akonadi-search")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/akonadi-search-" version ".tar.xz"))
      (sha256
       (base32 "0wf22rmfz471iw6zxl7yz279fkks2pj5jfw4bs5185kr3xw2q7zp"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/mmv45c7byl8kfyyc9x9q7nrmjqh05xk7-akonadi-search-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("krunner" ,krunner)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Xapian CONFIG)
;; find_package(KF5 Akonadi)
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiMime ${AKONADI_MIMELIB_VERSION})
;; find_package(KF5 CalendarCore ${KCALENDARCORE_LIB_VERSION})
    (home-page "")
    (synopsis1 "")
    (synopsis2 "Akonadi search library - runtime binaries")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
;; Found on download-server, maybe already included in main package
(define-public kdev-php
  (package
    (name "kdev-php")
    (version "5.0.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kdevelop/" version "/src/kdev-php-" version ".tar.xz"))
      (sha256
       (base32 "1ixf03d03yc0dk29n0i6df912gw5xc58p3hsw6680m68xns8kv14"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Development")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/whm532vsg8il5xl6gxij880n8ny4p4bh-kdev-php-5.0.2.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kcmutils" ,kcmutils)
      ("ki18n" ,ki18n)
      ("kitemmodels" ,kitemmodels)
      ("ktexteditor" ,ktexteditor)
      ("qtbase" ,qtbase)
      ("qtwebkit" ,qtwebkit)
      ("threadweaver" ,threadweaver)))
;; left-overs from CMakeLists.txt:
;; find_package(KDevPlatform ${KDEVPLATFORM_VERSION} REQUIRED)
;; find_package(KDevelop-PG-Qt REQUIRED)
    (home-page "")
    (synopsis1 "PHP plugin for kdevelop ")
    (synopsis2 "")
    (description1 "This plugin adds PHP language support (including classview and code-completion)
to KDevelop.
---
This plugin adds PHP language support (including classview and code-completion)
to KDevelop.")
    (description2 "")
    (license ) ;; TODO: check this
))
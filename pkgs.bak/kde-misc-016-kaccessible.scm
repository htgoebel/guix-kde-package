(define-public kaccessible
  (package
    (name "kaccessible")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kaccessible-" version ".tar.xz"))
      (sha256
       (base32 "162a4pw61b3rhq5mf7zdhgyyhbrxhr9fjw7bidicw7aljiy2cwb9"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/8brqp2hh6c1xzhhj7kh95b6h6wpw57kz-kaccessible-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;;     foreach (_i ${_sources})
;;     endforeach (_i ${ARGN})
;; dbus_add_activation_service(org.kde.kaccessible.service.in)
;; macro_optional_find_package(Speechd)
;; qt4_wrap_cpp(kaccessibleapp_SRCS kaccessibleapp.h)
    (home-page "http://www.kde.org")
    (synopsis1 "Accessibility services like focus reading and a screenreader for kde ")
    (synopsis2 "accessibility services for Qt applications")
    (description1 "Kaccessible implements a QAccessibleBridgePlugin to provide accessibility
services like focus tracking and a screenreader.")
    (description2 "kaccessible implements a QAccessibleBridgePlugin to provide accessibility
services like focus tracking and a screen reader.

Components:
 * kaccessibleapp: a D-Bus activation service that acts as proxy.
 * kaccessiblebridge: a Qt plugin which will be loaded by the QAccessible
   framework in each Qt and KDE application.

This package is part of the KDE accessibility module.")
    (license bsd-3) ;; TODO: check this
))
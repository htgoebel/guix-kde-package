(define-public kcalutils
  (package
    (name "kcalutils")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kcalutils-" version ".tar.xz"))
      (sha256
       (base32 "0449029fa1w496fmb81csb5amk801n11ish450drqw34lp9qla4n"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/6f9776ql98w2qkpvz60jd6cr874j2ic3-kcalutils-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)))
;; left-overs from CMakeLists.txt:
;; find_package(Grantlee5 "5.1" CONFIG REQUIRED)
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGER_LIB_VERSION})
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "library with utility functions for the handling of calendar data")
    (description1 "This library provides a utility and user interface functions for accessing
calendar data using the kcalcore API.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public kbruch
  (package
    (name "kbruch")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kbruch-" version ".tar.xz"))
      (sha256
       (base32 "12zmp9ix8q9mf3a4xpbsr0y9h4g1srwx48604x1phdwwdhgx0gsj"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/dk9fg5jxbqpaxi566bv60yj9alzqyb6b-kbruch-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "https://projects.kde.org/projects/kde/kdeedu/kbruch")
    (synopsis1 "Practice calculating with fractions ")
    (synopsis2 "fraction learning aid for KDE")
    (description1 "KBruch is a small program to practice calculating with fractions.")
    (description2 "KBruch is an aid for learning how to calculate with fractions.

This package is part of the KDE education module.")
    (license GFDL-NIV-1.2+) ;; TODO: check this
))
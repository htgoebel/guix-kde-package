(define-public kblog
  (package
    (name "kblog")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kblog-" version ".tar.xz"))
      (sha256
       (base32 "0q1qswg7dgy0jvk9kaz6cps6sfddsmv0lp5r3nhk751l497bbl3x"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/p3vprlcaq5spn064zq5d37ac85ayn0l8-kblog-16.12.3.tar.xz
    (inputs
     `(("kio" ,kio)
      ("kxmlrpcclient" ,kxmlrpcclient)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 Syndication ${SYNDICATION_LIB_VERSION})
;; find_package(KF5 ${XMLRPCCLIENT_LIB_VERSION})
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "client-side support library for web application remote blogging APIs")
    (description1 "This library provides client-side support for web application remote blogging APIs.")
    (description2 "")
    (license ) ;; TODO: check this
))
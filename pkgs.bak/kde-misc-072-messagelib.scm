;; ?
(define-public messagelib
  (package
    (name "messagelib")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/messagelib-" version ".tar.xz"))
      (sha256
       (base32 "1s5r5w231lzy3csljd5qil54ibm9mjs7c9hbw1whqmn4kd0vaz15"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/m22x7z6xwk4w5m3snf1pa11g90fn0drz-messagelib-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("kjobwidgets" ,kjobwidgets)
      ("kservice" ,kservice)
      ("ksyntaxhighlighting" ,ksyntaxhighlighting)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtwebkit" ,qtwebkit)
      ("sonnet" ,sonnet)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Mime)
;; find_package(QGpgme ${GPGME_LIB_VERSION} CONFIG REQUIRED)
;;       find_package(KF5 Libkleo ${LIBKLEO_LIB_VERSION})
;;    find_package(KF5 Libkleo ${LIBKLEO_LIB_VERSION})
;;    find_package(Qt5 WebEngine WebEngineWidgets)
;;
;;
;;
;;
;;
;;
;;
;;
;;    
;;    
;;    
;;    
;;    
;;    
;;    
;;    find_package(Grantlee5 "5.1" CONFIG REQUIRED)
;;    find_package(KF5 Akonadi)
;;    find_package(KF5 AkonadiMime ${AKONADIMIME_LIB_VERSION})
;;    find_package(KF5 AkonadiNotes ${AKONADINOTES_LIB_VERSION})
;;    find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;;    find_package(KF5 FollowupReminder ${KDEPIM_APPS_LIB_VERSION})
;;    find_package(KF5 GrantleeTheme ${GRANTLEETHEME_LIB_VERSION})
;;    find_package(KF5 Gravatar ${GRAVATAR_LIB_VERSION})
;;    find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
;;    find_package(KF5 KaddressbookGrantlee ${KDEPIM_APPS_LIB_VERSION})
;;    find_package(KF5 Ldap ${KLDAP_LIB_VERSION})
;;    find_package(KF5 Libkdepim ${LIBKDEPIM_LIB_VERSION})
;;    find_package(KF5 MailTransport ${KMAILTRANSPORT_LIB_VERSION})
;;    find_package(KF5 Mbox ${KMBOX_LIB_VERSION})
;;    find_package(KF5 PimCommon ${PIMCOMMON_LIB_VERSION})
;;    find_package(KF5 PimTextEdit ${KPIMTEXTEDIT_LIB_VERSION})
;;    find_package(KF5 SendLater ${KDEPIM_APPS_LIB_VERSION})
;;    find_package(KF5 AkonadiSearch "5.4.3")
;;   find_package(Git)
    (home-page "")
    (synopsis1 "KDEPIM 4 library ")
    (synopsis2 "message core library")
    (description1 "KDEPIM 4 library.")
    (description2 "")
    (license ) ;; TODO: check this
))
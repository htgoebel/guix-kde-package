(define-public kde-runtime
  (package
    (name "kde-runtime")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kde-runtime-" version ".tar.xz"))
      (sha256
       (base32 "1sqp827l30adiqrp12djx3xk6mlz2lb46hmxnbnzv52mv2whcr3y"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/z9q7j94j5s8v7qkvk1ny6dkp22cr7f0i-kde-runtime-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 4.9.95 REQUIRED)
;; find_package(KDeclarative QUIET CONFIG)
;;  support from kdelibs"
;;                        URL "http://www.kde.org"
;;                        TYPE REQUIRED
;;                        PURPOSE "Required by corebindingsplugin (org.kde.plasma.core)"
;;                       )
;; macro_optional_find_package(NepomukCore)
;; macro_optional_find_package(SLP)
;;  implementation"
;;                        URL "http://www.openslp.org/"
;;                        TYPE OPTIONAL
;;                        PURPOSE "Provides SLP support in the network:/ kioslave."
;;                       )
;; find_package(LibAttica 0.1.4)
;; macro_optional_find_package(QCA2 2.0.0)
;; find_package(LibGcrypt 1.5.0 REQUIRED QUIET)
    (home-page "")
    (synopsis1 "K Desktop Environment - Base Runtime ")
    (synopsis2 "")
    (description1 "KDE 4 application runtime components.")
    (description2 "")
    (license ) ;; TODO: check this
))
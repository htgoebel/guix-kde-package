(define-public ktp-auth-handler
  (package
    (name "ktp-auth-handler")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-auth-handler-" version ".tar.xz"))
      (sha256
       (base32 "08ryqkba9zngjabsp1b9w13psp0n97qhjd31id007hc6r6s1nxxx"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/9i5rrrc7yxh95dhhdnl7k32vnikkp53z-ktp-auth-handler-16.12.3.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwallet" ,kwallet)
      ("kwebkit" ,kwebkit)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; #Network for QSsl
;; find_package (TelepathyQt5 0.8.9 REQUIRED)
;; find_package (KTp REQUIRED)
;; find_package (Qca-qt5 REQUIRED)
;; find_package(AccountsQt5 1.10 REQUIRED CONFIG)
;; find_package(SignOnQt5 8.55 REQUIRED CONFIG)
;; find_package(KAccounts REQUIRED)
;; find_package(Qca-qt5-ossl QUIET)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
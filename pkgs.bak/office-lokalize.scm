(define-public lokalize
  (package
    (name "lokalize")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/lokalize-" version ".tar.xz"))
      (sha256
       (base32 "17ikk89680jjzv95gnrzah4bi3xnyp5mi64prhblhw7kz6vlf8x0"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Development")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/65pp82958f2gnbyby6gq3drl51win9qd-lokalize-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("kross" ,kross)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtscript" ,qtscript)
      ("sonnet" ,sonnet)))
;; left-overs from CMakeLists.txt:
;; find_package(HUNSPELL)
    (home-page "http://www.kde.org/")
    (synopsis1 "Computer-Aided Translation Tool ")
    (synopsis2 "computer-aided translation system")
    (description1 "Lokalize is a computer-aided translation system that focuses on
productivity and performance. The translator does only creative work
(of delivering message in his/her mother language in laconic and
easy to understand form). Lokalize implies a paragraph-by-paragraph
translation approach (when translating documentation) and
message-by-message approach (when translating GUI strings).")
    (description2 "Lokalize is a computer-aided translation system that focuses on productivity
and quality assurance. It has components usual for CAT tools: translation
memory, glossary, and also a unique translation merging (synchronization)
capability. It is primarily targeted at software translation and also
integrates external conversion tools for office document translation.
Lokalize is a replacement of KBabel.

This package is part of the KDE Software Development Kit module.")
    (license fdl-1.2+) ;; TODO: check this
))
(define-public ktp-desktop-applets
  (package
    (name "ktp-desktop-applets")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-desktop-applets-" version ".tar.xz"))
      (sha256
       (base32 "1pd8vzwmxcsw6pq80r9casi07wwisr70l5ffm231v9d73k4fw7kv"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/1ry1clhkpwll22vzabgsqmqcx67x6mn7-ktp-desktop-applets-16.12.3.tar.xz
    (inputs
     `(("kwindowsystem" ,kwindowsystem)
      ("plasma-framework" ,plasma-framework)
      ("qtdeclarative" ,qtdeclarative)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
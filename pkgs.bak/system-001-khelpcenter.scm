(define-public khelpcenter
  (package
    (name "khelpcenter")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/khelpcenter-" version ".tar.xz"))
      (sha256
       (base32 "100xcjjjbszhbwgydbylk9m9lrxikjyazmhaq2rvv2mfzsbijjm7"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "System")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/wybqpm7g9qlnqwr2syqvdng2ykv9v9xk-khelpcenter-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kbookmarks" ,kbookmarks)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("kinit" ,kinit)
      ("kservice" ,kservice)
      ("kwindowsystem" ,kwindowsystem)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Grantlee5 REQUIRED)
;; find_package(Xapian REQUIRED)
;; find_package(LibXml2 REQUIRED)
;; target_compile_definitions(kdeinit_khelpcenter PRIVATE -DPROJECT_VERSION="${PROJECT_VERSION}")
    (home-page "")
    (synopsis1 "Khelpcenter ")
    (synopsis2 "KDE documentation viewer")
    (description1 "Khelpcenter.")
    (description2 "")
    (license ) ;; TODO: check this
))
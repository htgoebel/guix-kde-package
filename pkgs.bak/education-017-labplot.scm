(define-public labplot
  (package
    (name "labplot")
    (version "2.3.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/labplot/" version "/labplot-" version "-kf5.tar.xz"))
      (sha256
       (base32 "0cvgcavn1iqnmxb8sq59bygn38i7j8ps9barncq895szvq3dg9x5"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/cyhy10lsh6wv6xvlkbkhvdhyg9p4p9cd-labplot-2.3.0-kf5.tar.xz
    (inputs
     `(("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; FIND_PATH (FFTW_INCLUDE_DIR fftw3.h
;; 	/usr/include
;; 	/usr/local/include
;; )
;; FIND_PATH (GSL_INCLUDE_DIR gsl/gsl_multimin.h
;; 	/usr/include
;; 	/usr/local/include
;; )
;; FIND_PROGRAM (GSL_CONFIG gsl-config
;; 	/usr/bin
;; 	/usr/local/bin
;; )
;; 	EXEC_PROGRAM (${GSL_CONFIG} ARGS "--version" OUTPUT_VARIABLE gsl_version)
;; FIND_PATH (CANTOR_INCLUDE_DIR worksheetaccess.h
;; 	/usr/include/cantor
;; 	/usr/local/include/cantor
;; )
;; FIND_PATH (HDF5_INCLUDE_DIR hdf5.h
;; 	/usr/include
;; 	/usr/local/include
;; 	/usr/include/hdf5/serial
;; )
;;  5 Library: ${HDF5_INCLUDE_DIR} ${HDF5_LIBRARY}")
;;  5 Library not found.")
;; FIND_PATH (NETCDF_INCLUDE_DIR netcdf.h
;; 	/usr/include/
;; 	/usr/local/include/
;; )
;;  Library: ${NETCDF_INCLUDE_DIR} ${NETCDF_LIBRARY}")
;;  Library not found.")
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis1 "KDE-application for interactive graphing and analysis of scientific data ")
    (synopsis2 "interactive graphing and analysis of scientific data")
    (description1 "LabPlot provides an easy way to create, manage and edit plots.
It allows you to produce plots based on data from a spreadsheet or on
data imported from external files.
Plots can be exported to several pixmap and vector graphic formats.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public krecipes
  (package
    (name "krecipes")
    (version "2.1.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/krecipes/" version "/src/krecipes-" version ".tar.xz"))
      (sha256
       (base32 "0x38xkabgq7dkzq9rk6ayjlaap1lpsklmqxkzgn1kx4cl0cd7i7x"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/0x826sgra0g22bib0nqa4p9b3biqm566-krecipes-2.1.0.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
    (home-page "http://krecipes.sourceforge.net/")
    (synopsis1 "Krecipes - Your Way to Cook with Tux ")
    (synopsis2 "recipes manager for KDE")
    (description1 "A highly configurable recipe manager, designed to make organizing your
personal recipes collection fast and easy. Features include: shopping
lists, nutrient analysis, advanced search, recipe ratings, import/export
various formats, and more.")
    (description2 "Krecipes is a KDE application designed to manage recipes. It can help you to
do your shopping list, search through your recipes to find what you can do
with available ingredients and a diet helper. It can also import or export
recipes from files in various format (eg RecipeML or Meal-Master) or from
databases.")
    (license BSD) ;; TODO: check this
))
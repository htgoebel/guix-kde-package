(define-public libksane
  (package
    (name "libksane")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libksane-" version ".tar.xz"))
      (sha256
       (base32 "0406jdzs193scmb8rkx2fqcr2a0svz53sf1av9qi2nq9dnil9hcq"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/sr72zijzax50jwi781f2r6bmzhi7z4ba-libksane-16.12.3.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("ktextwidgets" ,ktextwidgets)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Sane)
;; configure_package_config_file(
;;   "${CMAKE_CURRENT_SOURCE_DIR}/KF5SaneConfig.cmake.in"
;;   "${CMAKE_CURRENT_BINARY_DIR}/KF5SaneConfig.cmake"
;;   INSTALL_DESTINATION "${CMAKECONFIG_INSTALL_DIR}"
;; )
    (home-page "")
    (synopsis1 "A library for dealing with scanners ")
    (synopsis2 "scanner library (runtime)")
    (description1 "LibKSane is a KDE interface for SANE library to control flat scanner.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public kompare
  (package
    (name "kompare")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kompare-" version ".tar.xz"))
      (sha256
       (base32 "18z6424xidpx5kmbjiasvhagh2b1a9pxizc0dsvba47v9xcavsaw"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Development")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/sqbw3rq10khgc2j9qb7dr0say8dyv286-kompare-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kiconthemes" ,kiconthemes)
      ("kjobwidgets" ,kjobwidgets)
      ("kparts" ,kparts)
      ("ktexteditor" ,ktexteditor)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(LibKompareDiff2 REQUIRED)
    (home-page "http://www.kde.org/")
    (synopsis1 "A diff graphic tool ")
    (synopsis2 "file difference viewer")
    (description1 "Kompare is a GUI front-end program that enables differences between source files
to be viewed and merged. It can be used to compare differences on files or the
contents of folders, and it supports a variety of diff formats and provide many
options to customize the information level displayed.

Features :
   -Comparing directories
   -Reading diff files
   -Creating and applying patches")
    (description2 "Kompare displays the differences between files.  It can compare the
contents of files or directories, as well as create, display, and merge patch
files.

This package is part of the KDE Software Development Kit module.")
    (license fdl-1.2+) ;; TODO: check this
))
(define-public grantleetheme
  (package
    (name "grantleetheme")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/grantleetheme-" version ".tar.xz"))
      (sha256
       (base32 "19mka62p75qnv6r9ib70y25jjj3bpziz0xqihfkb6jvafrgy8p8k"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/w9j09gfa00wcs7p6n5wa22lpp6428w87-grantleetheme-16.12.3.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("knewstuff" ,knewstuff)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Grantlee5 ${GRANTLEE_VERSION} CONFIG REQUIRED)
    (home-page "")
    (synopsis1 "Library providing grantlee theme support ")
    (synopsis2 "KDE PIM grantlee theme support - library")
    (description1 "This library provides grantlee theme support.")
    (description2 "")
    (license ) ;; TODO: check this
))
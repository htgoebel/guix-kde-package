(define-public kdepim
  (package
    (name "kdepim")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdepim-" version ".tar.xz"))
      (sha256
       (base32 "1l0rvi33i9hzr9c3nqzbr3cnz046ccf7s3v54mdmxfdk5x0ynkms"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)
      ("qttools" ,qttools)))
;; /gnu/store/v5c1bamxnqgagi5pxqmwwwfvky1la9fj-kdepim-16.08.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kauth" ,kauth)
      ("kcmutils" ,kcmutils)
      ("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("kdnssd" ,kdnssd)
      ("kglobalaccel" ,kglobalaccel)
      ("knewstuff" ,knewstuff)
      ("knotifyconfig" ,knotifyconfig)
      ("kservice" ,kservice)
      ("ktexteditor" ,ktexteditor)
      ("kwallet" ,kwallet)
      ("kxmlrpcclient" ,kxmlrpcclient)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("qtx11extras" ,qtx11extras)
      ("sonnet" ,sonnet)))
;; left-overs from CMakeLists.txt:
;; " FALSE)
;;     sb_add_project(kmail)
;;     sb_add_project(grantleeeditor)
;;     sb_add_project(korganizer)
;;     sb_add_project(sieveeditor)
;;     sb_add_project(storageservicemanager)
;;     sb_add_project(akregator)
;;     sb_add_project(importwizard)
;;     sb_add_project(kaddressbook)
;;     sb_add_project(mboximporter)
;;     sb_add_project(knotes)
;;     sb_add_project(ktnef)
;;     sb_add_project(pimsettingexporter)
;;     sb_add_project(kalarm)
;;     sb_add_project(blogilo)
;;     sb_add_project(kontact)
;;     sb_add_project(akonadiconsole)
;;     sb_add_project(console)
;;     sb_add_project(accountwizard)
;;     sb_end()
;;
;;     find_package(Qt5 WebEngine WebEngineWidgets)
;;
;;     find_package(Grantlee5 "5.1" CONFIG REQUIRED)
;;     find_package(KF5 WebEngineViewer ${MESSAGELIB_LIB_VERSION_LIB})
;;
;;
;;
;;
;;
;;     
;;     
;;     
;;     
;;     
;;     
;;     
;;     
;;     
;;     
;;     
;;     
;;     
;;     find_package(KF5 PimTextEdit ${KPIMTEXTEDIT_LIB_VERSION})
;;     find_package(KF5 Akonadi)
;;     find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;;     find_package(KF5 CalendarCore ${KCALENDARCORE_LIB_VERSION})
;;     find_package(KF5 AkonadiContact ${AKONADI_CONTACT_VERSION})
;;     find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
;;     find_package(KF5 Ldap ${KLDAP_LIB_VERSION})
;;     find_package(KF5 MailTransport ${KMAILTRANSPORT_LIB_VERSION})
;;     find_package(KF5 AkonadiMime ${AKONADI_MIMELIB_VERSION})
;;     find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;;     find_package(KF5 Holidays ${KHOLIDAYS_LIB_VERSION})
;;     find_package(KF5 Tnef ${KTNEF_LIB_VERSION})
;;     find_package(KF5 IMAP ${KIMAP_LIB_VERSION})
;;     find_package(KF5 Mbox ${KMBOX_LIB_VERSION})
;;     find_package(KF5 AkonadiCalendar ${AKONADICALENDAR_LIB_VERSION})
;;     find_package(KF5 Syndication ${SYNDICATION_LIB_VERSION})
;;     find_package(KF5 Gpgmepp ${GPGMEPP_LIB_VERSION})
;;     find_package(KF5 KontactInterface ${KONTACTINTERFACE_LIB_VERSION})
;;     find_package(KF5 AlarmCalendar ${AKONADIKALARM_LIB_VERSION})
;;     find_package(KF5 Mime)
;;     find_package(KF5 ${XMLRPCCLIENT_LIB_VERSION})
;;     find_package(KF5 Blog ${KBLOG_LIB_VERSION})
;;     find_package(KF5 AkonadiNotes ${AKONADINOTES_LIB_VERSION})
;;     find_package(KF5 Gravatar ${LIBGRAVATAR_VERSION_LIB})
;;     find_package(KF5 MailImporter ${LIBMAILIMPORTER_VERSION_LIB})
;;     find_package(KF5 MailCommon ${MAILCOMMON_LIB_VERSION_LIB})
;;     find_package(KF5 KaddressbookGrantlee ${KDEPIM_APPS_LIB_VERSION_LIB})
;;     find_package(KF5 MessageViewer ${MESSAGELIB_LIB_VERSION_LIB})
;;     find_package(KF5 Libkleo ${LIBKLEO_LIB_VERSION_LIB})
;;     find_package(KF5 GrantleeTheme ${LIBGRANTLEETHEME_LIB_VERSION_LIB})
;;     find_package(KF5 PimCommon ${PIMCOMMON_LIB_VERSION_LIB})
;;     find_package(KF5 Libkdepim ${LIBKDEPIM_LIB_VERSION_LIB})
;;     find_package(KF5 IncidenceEditor ${LIBINCIDENCEEDITOR_LIB_VERSION_LIB})
;;     find_package(KF5 MessageCore ${MESSAGELIB_LIB_VERSION_LIB})
;;     find_package(KF5 MessageComposer ${MESSAGELIB_LIB_VERSION_LIB})
;;     find_package(KF5 MessageList ${MESSAGELIB_LIB_VERSION_LIB})
;;     find_package(KF5 TemplateParser ${MESSAGELIB_LIB_VERSION_LIB})
;;     find_package(KF5 CalendarSupport ${CALENDARSUPPORT_LIB_VERSION_LIB})
;;     find_package(KF5 EventViews ${EVENTVIEW_LIB_VERSION_LIB})
;;     find_package(KF5 KDGantt2 ${KDGANTT2_LIB_VERSION_LIB})
;;     find_package(KF5 FollowupReminder ${KDEPIM_APPS_LIB_VERSION_LIB})
;;     find_package(KF5 SendLater ${KDEPIM_APPS_LIB_VERSION_LIB})
;;     find_package(KF5 KdepimDBusInterfaces ${KDEPIM_APPS_LIB_VERSION_LIB})
;;     find_package(KF5 LibKSieve ${LIBKSIEVE_LIB_VERSION_LIB})
;;     find_package(MailTransportDBusService CONFIG REQUIRED)
;;         find_package(X11)
;;     find_package(KF5 GAPI "5.3.0")
;;     find_package(KF5 AkonadiSearch "5.3.0")
;;     find_package(Boost 1.34.0)
    (home-page "http://pim.kde.org/")
    (synopsis1 "K Desktop Environment ")
    (synopsis2 "KDE PIM library")
    (description1 "Information Management applications for the K Desktop Environment.
	- kaddressbook: The KDE addressbook application.
	- korganizer: a calendar-of-events and todo-list manager
	- kalarm: gui for setting up personal alarm/reminder messages
	- kalarmd: personal alarm/reminder messages daemon, shared by korganizer
	  and kalarm.
	- kaplan: A shell for the PIM apps, (still experimental).
	- ktimetracker: Time tracker.
	- kfile-plugins: vCard KFIleItem plugin.
	- knotes: yellow notes application
	- konsolecalendar: Command line tool for accessing calendar files.
	- kmail: universal mail client
	- kmailcvt: a tool to convert addressbooks to kmail format")
    (description2 "KDE (the K Desktop Environment) is a powerful Open Source graphical
desktop environment for Unix workstations. It combines ease of use,
contemporary functionality, and outstanding graphical design with the
technological superiority of the Unix operating system.

This metapackage includes a collection of Personal Information Management
(PIM) desktop applications provided with the official release of KDE.")
    (license CC-BY or BSD) ;; TODO: check this
))
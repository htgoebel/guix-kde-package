;; Powerful libraries (KChart, KGantt) for creating business diagrams
(define-public kdiagram
  (package
    (name "kdiagram")
    (version "2.6.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kdiagram/" version "/src/kdiagram-" version ".tar.xz"))
      (sha256
       (base32 "10hqk12wwgbiq4q5145s8v7v96j621ckq1yil9s4pihmgsnqsy02"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unlisted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/jz0i1yg8i0gxn50cpy0mm2lpgkcxg5i0-kdiagram-2.6.0.tar.xz
    (inputs
     `(("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
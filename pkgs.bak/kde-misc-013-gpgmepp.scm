(define-public gpgmepp
  (package
    (name "gpgmepp")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/gpgmepp-" version ".tar.xz"))
      (sha256
       (base32 "0sda2ib5ymyzsji2v7aqdgfy6mhhfs6s3wyngb2k3q8hshv4xi5z"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/p7xwjwg4qy659ynhz9xnxh7643b61nzm-gpgmepp-16.08.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Gpgme)
;; find_package(Boost REQUIRED)
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "c++ wrapper library for gpgme")
    (description1 "KDE 4 core library.")
    (description2 "")
    (license ) ;; TODO: check this
))
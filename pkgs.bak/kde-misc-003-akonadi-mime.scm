(define-public akonadi-mime
  (package
    (name "akonadi-mime")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/akonadi-mime-" version ".tar.xz"))
      (sha256
       (base32 "1xay3rlygdhf9lp356wjb92wnmxdpaxgm3prxnfxcdg5ql6xcg07"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/vcjm8zva5x45lzkclax4rz90lwqa7lp5-akonadi-mime-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kitemmodels" ,kitemmodels)
      ("kxmlgui" ,kxmlgui)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Mime ${KF5_MIME_VERSION})
;; find_package(KF5 ${KF5_MIME_VERSION})
;; find_package(KF5 Akonadi)
;; find_package( SharedMimeInfo REQUIRED )
;; find_package(LibXslt)
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "Akonadi MIME handling library")
    (description1 "A client access library for using the Akonadi PIM data server.")
    (description2 "")
    (license ) ;; TODO: check this
))
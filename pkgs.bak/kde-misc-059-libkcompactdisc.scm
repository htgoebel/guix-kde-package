(define-public libkcompactdisc
  (package
    (name "libkcompactdisc")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkcompactdisc-" version ".tar.xz"))
      (sha256
       (base32 "021yws9854w6fwwfw31b87rpz92ach5xyq427968m3mc3c430d4l"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/qk45mshkplrypfxwja54h85pzagl0r82-libkcompactdisc-16.12.3.tar.xz
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("solid" ,solid)))
;; left-overs from CMakeLists.txt:
;; ﻿cmake_minimum_required(VERSION 2.8.12)
;;  # handled by release scripts
    (home-page "")
    (synopsis1 "KDE library for playing & ripping CDs ")
    (synopsis2 "CD drive library for KDE Platform (runtime)")
    (description1 "KDE library for playing & ripping CDs.")
    (description2 "")
    (license ) ;; TODO: check this
))
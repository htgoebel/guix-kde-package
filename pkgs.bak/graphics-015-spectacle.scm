(define-public spectacle
  (package
    (name "spectacle")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/spectacle-" version ".tar.xz"))
      (sha256
       (base32 "1fca71a59sgicq9zi8d0im0xpg7iz93s96h3clxxc6p493vsjkx6"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/9xyvljly6vwlmjx1ri91k4851lbzv9wl-spectacle-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtx11extras" ,qtx11extras)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Kipi)
;; find_package(KDEExperimentalPurpose)
;; find_package(XCB COMPONENTS XFIXES IMAGE UTIL CURSOR)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "RPM Spec file generator and management tool")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
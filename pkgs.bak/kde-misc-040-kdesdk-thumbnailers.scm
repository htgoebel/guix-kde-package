(define-public kdesdk-thumbnailers
  (package
    (name "kdesdk-thumbnailers")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdesdk-thumbnailers-" version ".tar.xz"))
      (sha256
       (base32 "0p2s6pyq16jmjv29r8n9ygvsh1dxgz9zk90mk138cxxhbx9nks9h"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/7gf4f92663is8zym00v2an125nlnk5wc-kdesdk-thumbnailers-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(GettextPO)
    (home-page "")
    (synopsis1 "K Desktop Environment - Software Development Kit ")
    (synopsis2 "")
    (description1 "Software Development Kit for the K Desktop Environment.")
    (description2 "")
    (license ) ;; TODO: check this
))
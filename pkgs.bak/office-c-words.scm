(define-public words
  (package
    (name "words")
    (version "3.0.0.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/calligra-latest/calligra-" version ".tar.xz"))
      (sha256
       (base32 "04ghq422lw8kypqbraz0b2k3gnyrfavhqiada4vxlz9gqkpm3p6y"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Office")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/5apyk3kfvnsh15anc0idqig48w0h53jg-calligra-3.0.0.1.tar.xz
    (inputs
     `(("kactivities" ,kactivities)
      ("karchive" ,karchive)
      ("kcmutils" ,kcmutils)
      ("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("kguiaddons" ,kguiaddons)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kparts" ,kparts)
      ("kross" ,kross)
      ("ktexteditor" ,ktexteditor)
      ("ktextwidgets" ,ktextwidgets)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)
      ("qtwebkit" ,qtwebkit)
      ("qtx11extras" ,qtx11extras)
      ("sonnet" ,sonnet)
      ("threadweaver" ,threadweaver)))
;; left-overs from CMakeLists.txt:
;; . With sources from a file bundle (like a zip file), delete the source folder and unbundle the sources again.")
;;  # 3 for 3.x, 4 for 4.x, etc.
;;  # 0 for 3.0, 1 for 3.1, etc.
;;      # 89 for Alpha, increase for next test releases, set 0 for first Stable, etc.
;;  # update every year
;;  # do not edit
;; calligra_set_productset(${PRODUCTSET})
;;     find_package(Attica 3.0)
;;     find_package(NewStuff)
;; find_package(X11)
;;
;; find_package(Perl REQUIRED)
;; find_package(ZLIB REQUIRED)
;; find_package(Boost REQUIRED COMPONENTS system) # for pigment and stage
;; macro_optional_find_package(OpenEXR)
;; macro_optional_find_package(GSL 1.7)
;; find_package(KF5 CalendarCore)
;; find_package(KF5 Contacts)
;; find_package(KF5 AkonadiContact)
;; find_package(KF5 Akonadi)
;; macro_optional_find_package(KGantt 2.6.0 QUIET)
;; "
;;     URL "https://www.kde.org/"
;;     PURPOSE "Required by Plan"
;;     TYPE RECOMMENDED
;; )
;; macro_optional_find_package(KChart 2.6.0 QUIET)
;; "
;;     URL "https://www.kde.org/"
;;     PURPOSE "Required by Chart shape and Plan"
;;     TYPE RECOMMENDED
;; )
;; macro_optional_find_package(KReport 3.0.0 EXACT QUIET)
;; macro_optional_find_package(KPropertyWidgets 3.0.0 EXACT QUIET)
;; macro_optional_find_package(Eigen3)
;; macro_optional_find_package(Qca-qt5 2.1.0 QUIET)
;; "
;;     TYPE OPTIONAL
;; )
;; macro_optional_find_package(Marble CONFIG)
;; macro_optional_find_package(LCMS2)
;;     macro_optional_find_package(Vc 1.1.0)
;;     elseif (NOT MSVC)
;;     macro_optional_find_package(Fontconfig)
;;     macro_optional_find_package(Freetype)
;; test_big_endian(CMAKE_WORDS_BIGENDIAN)
;; macro_optional_find_package(SharedMimeInfo)
;; macro_optional_find_package(Okular5 0.99.60 QUIET)
;; macro_optional_find_package(LibRevenge)
;; macro_optional_find_package(LibOdfGen)
;; macro_optional_find_package(LibWpd)
;; macro_optional_find_package(LibWpg)
;; macro_optional_find_package(LibWps)
;; macro_optional_find_package(LibVisio)
;; macro_optional_find_package(LibEtonyek)
;; macro_optional_find_package(Poppler)
;; macro_optional_find_package(PopplerXPDFHeaders)
;; macro_optional_find_package(Libgit2)
;; macro_optional_find_package(Libqgit2)
;; calligra_drop_unbuildable_products()
    (home-page "")
    (synopsis1 "A dictionary of English words for the /usr/dict directory ")
    (synopsis2 "word processor for the Calligra Suite")
    (description1 "The words file is a dictionary of English words for the
/usr/share/dict directory. Some programs use this database of
words to check spelling. Password checkers use it to look for bad
passwords.
---
The words file is a dictionary of English words for the
/usr/share/dict directory. Some programs use this database of
words to check spelling. Password checkers use it to look for bad
passwords.")
    (description2 "")
    (license ) ;; TODO: check this
))
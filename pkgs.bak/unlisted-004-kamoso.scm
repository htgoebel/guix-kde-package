;; Web-cam program
(define-public kamoso
  (package
    (name "kamoso")
    (version "3.2.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kamoso/3.2/src/kamoso-" version ".tar.xz"))
      (sha256
       (base32 "1rvka31d786gik1sdlz72gmbvm216zr478czk45c93d6m6vmqsm7"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unlisted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/0h9is4n9259q73pfn72hbxkgqayxwmh5-kamoso-3.2.1.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 GStreamer 1.1.90)
;; find_package(KDEExperimentalPurpose REQUIRED)
;; find_package(UDev REQUIRED)
    (home-page "http://www.kde-apps.org/content/show.php/Kamoso?content=111750")
    (synopsis1 "Application to take pictures and videos out of your webcam ")
    (synopsis2 "tool to take pictures and videos from your webcam")
    (description1 "Kamoso is an application to take pictures and videos out of your webcam.")
    (description2 "Kamoso is a utility that does the very simple actions a webcam offers,
like taking pictures or recording videos and adds some extra features that will
make the webcam usage both funnier and more useful.")
    (license gpl2+) ;; TODO: check this
))
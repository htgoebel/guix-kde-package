(define-public ktp-approver
  (package
    (name "ktp-approver")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-approver-" version ".tar.xz"))
      (sha256
       (base32 "08aj2j9bjdyigl4cx65v5fjsdayid07mx0mq72iy6l17d8z4b39a"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/k1ck2shpckzb4sgmlk18kwk8hwqnwpcc-ktp-approver-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("knotifications" ,knotifications)
      ("kservice" ,kservice)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(TelepathyQt5 REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public libkmahjongg
  (package
    (name "libkmahjongg")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkmahjongg-" version ".tar.xz"))
      (sha256
       (base32 "0fhz7jp4wcvjcicyiwc4qq4vviwagfjdy6n5bhv7bmlccq4xfk5x"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/0bml98n95w0c3ss2lxns931yfn8zfazg-libkmahjongg-16.12.3.tar.xz
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; target_include_directories(KF5KMahjongglib INTERFACE "$<INSTALL_INTERFACE:${KF5_INCLUDE_INSTALL_DIR}/KF5KMahjongg>" INTERFACE "$<INSTALL_INTERFACE:${KF5_INCLUDE_INSTALL_DIR}>")
;; set_target_properties(KF5KMahjongglib PROPERTIES VERSION ${KMAHJONGGLIB_VERSION}
;;                                                  EXPORT_NAME KF5KMahjongglib)
;; export(TARGETS KF5KMahjongglib
;;     FILE "${PROJECT_BINARY_DIR}/KF5KMahjonggLibraryDepends.cmake")
    (home-page "")
    (synopsis1 "Runtime library for kmahjongg ")
    (synopsis2 "shared library for kmahjongg and kshisen")
    (description1 "Runtime library for kmahjongg.")
    (description2 "")
    (license ) ;; TODO: check this
))
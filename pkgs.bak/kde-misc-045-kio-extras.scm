(define-public kio-extras
  (package
    (name "kio-extras")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kio-extras-" version ".tar.xz"))
      (sha256
       (base32 "1mfpllrmc88khlpg3yd4sghs8igg8dh0x568jw46vv90qgdb9xss"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/cnimdwxpvzapdi6w4by1sf2ck5bx9mg2-kio-extras-16.12.3.tar.xz
    (inputs
     `(("kactivities" ,kactivities)
      ("karchive" ,karchive)
      ("kbookmarks" ,kbookmarks)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("kdnssd" ,kdnssd)
      ("kguiaddons" ,kguiaddons)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kpty" ,kpty)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)
      ("solid" ,solid)))
;; left-overs from CMakeLists.txt:
;; find_package(SLP)
;;  implementation"
;;                        URL "http://www.openslp.org/"
;;                        TYPE OPTIONAL
;;                        PURPOSE "Provides SLP support in the network:/ kioslave."
;;                       )
;;     find_package(Samba)
;;  and smbc_option_set()"
;;                         URL "http://www.samba.org"
;;                         TYPE OPTIONAL
;;                         PURPOSE "Needed to build the SMB kioslave"
;;                         )
;; find_package(LibSSH 0.6.0)
;; find_package(Mtp)
;;  ? 1 : -1];
;;         int main() { return 0; }
;;     "
;;     OFFT_IS_64BIT)
    (home-page "")
    (synopsis1 "Extra kio plugins needed by KF5 ")
    (synopsis2 "Extra functionality for kioslaves.")
    (description1 "Extra kio plugins needed by KF5")
    (description2 "")
    (license ) ;; TODO: check this
))
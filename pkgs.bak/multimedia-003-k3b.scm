(define-public k3b
  (package
    (name "k3b")
    (version "2.0.3a")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/k3b/k3b-" version ".tar.xz"))
      (sha256
       (base32 "10f07465g9860chfnvrp9w3m686g6j9f446xgnnx7h82d1sb42rd"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Multimedia")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/zmqb81fl7fgakm8bpw2zppx8r8v9fysz-k3b-2.0.3a.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;;   find_package(DvdRead)
;;   find_package(Taglib)
;; macro_optional_find_package(Samplerate)
;;   find_package(MusicBrainz)
;; find_package(Kcddb)
;;   find_package(FFmpeg)
;;     check_c_source_compiles("
;;       int main() { enum AVMediaType t = AVMEDIA_TYPE_UNKNOWN; return 0; }
;;       " HAVE_FFMPEG_AVMEDIA_TYPE)
;;     check_c_source_compiles("
;;       int main() {
;;       enum CodecID t = CODEC_ID_MP3;
;;       return 0;
;;       }
;;       " HAVE_FFMPEG_CODEC_MP3)
;;   find_package(Flac)
;;   find_package(Flac++)
;;   find_package(Mad)
;;   find_package(Muse)
;;   find_package(Sndfile)
;;   find_package(Lame)
;;   find_package(OggVorbis)
;;   find_package(Doxygen)
    (home-page "http://www.k3b.org")
    (synopsis1 "CD-Burner for KDE4 ")
    (synopsis2 "Sophisticated CD/DVD burning application")
    (description1 "K3b is CD-writing software which intends to be feature-rich and
provide an easily usable interface. Features include burning
audio CDs from .WAV and .MP3 audio files, configuring external
programs and configuring devices.
---
K3b is CD-writing software which intends to be feature-rich and
provide an easily usable interface. Features include burning
audio CDs from .WAV and .MP3 audio files, configuring external
programs and configuring devices.")
    (description2 "K3b provides a comfortable user interface to perform most CD/DVD burning
tasks. While the experienced user can take influence in all steps
of the burning process the beginner may find comfort in the automatic settings
and the reasonable k3b defaults which allow a quick start.")
    (license license:unknown) ;; TODO: check this
))
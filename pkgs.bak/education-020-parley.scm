(define-public parley
  (package
    (name "parley")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/parley-" version ".tar.xz"))
      (sha256
       (base32 "0vmbn8188brp4bysyzmkgsa8nnn9zdqb7q6x3mi1xg7ralzfjw71"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/lj7fxqjqfmi2mpkm68ijcb05fbx0iwag-parley-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("kross" ,kross)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtmultimedia" ,qtmultimedia)
      ("qtsvg" ,qtsvg)
      ("sonnet" ,sonnet)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 #to produce the docbook)
;; find_package(LibKEduVocDocument)
;; find_package(LibXslt)
;; find_package(LibXml2)
    (home-page "https://projects.kde.org/projects/kde/kdeedu/parley")
    (synopsis1 "KDE Vocabulary training application ")
    (synopsis2 "vocabulary trainer")
    (description1 "Parley is a program to help you memorize things.

Parley supports many language specific features but can be used for other
learning tasks just as well. It uses the spaced repetition learning method,
also known as flash cards.")
    (description2 "Parley is a utility to help train vocabulary when learning a foreign language.
It is intended as a replacement for flash cards.

This package is part of the KDE education module.")
    (license expat) ;; TODO: check this
))
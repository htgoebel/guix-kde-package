(define-public svgpart
  (package
    (name "svgpart")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/svgpart-" version ".tar.xz"))
      (sha256
       (base32 "0frzqp504dzqwqs9lh544xxa8i6sqi6qj533mqbzkqbjx310ka3w"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/ihp58w2x1wfqxql2m5fz3vnqwkbz36sf-svgpart-16.12.3.tar.xz
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("kparts" ,kparts)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "https://projects.kde.org/projects/kde/kdegraphics/svgpart")
    (synopsis1 "A SVG KPart ")
    (synopsis2 "KDE SVG KPart")
    (description1 "A SVG KPart Service.")
    (description2 "SvgPart is a small KDE KPart component to display SVG images in Gwenview
and in any other KDE application which uses the KPart system.

This package is part of the KDE graphics module.")
    (license gpl2+) ;; TODO: check this
))
(define-public mailimporter
  (package
    (name "mailimporter")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/mailimporter-" version ".tar.xz"))
      (sha256
       (base32 "08yj4i6jy08hk62mxw299sh2n5pknzxanmzr96n6lf8g1jcf2l21"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/49cslyjwqqlj88af9bknck68qr8djzd9-mailimporter-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Akonadi)
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiMime ${AKONADIMIME_LIB_VERSION})
;; find_package(KF5 Libkdepim ${KDEPIM_LIB_VERSION})
    (home-page "")
    (synopsis1 "Mail Importer Library ")
    (synopsis2 "mailimporter library")
    (description1 "Mail Importer Library")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public gwenview
  (package
    (name "gwenview")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/gwenview-" version ".tar.xz"))
      (sha256
       (base32 "069fblw9g9h6r9gy05nj2n93jpnsx610jncwl6403q01rwlbrgbr"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/80gvv6pfmvf46d2axbs7ilvyqgnwk06x-gwenview-16.12.3.tar.xz
    (inputs
     `(("baloo" ,baloo)
      ("kactivities" ,kactivities)
      ("kdelibs4support" ,kdelibs4support)
      ("kio" ,kio)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)
      ("qtx11extras" ,qtx11extras)))
;; left-overs from CMakeLists.txt:
;; ")
;; elseif(GWENVIEW_SEMANTICINFO_BACKEND STREQUAL "Fake")
;; find_package(JPEG)
;; find_package(PNG)
;; find_package(Exiv2)
;; find_package(KF5 Kipi)
;; find_package(LCMS2)
;;
;;         unset(GWENVIEW_SEMANTICINFO_BACKEND_BALOO)
;; find_package(KF5 KDcraw)
;; find_package(X11)
    (home-page "http://www.kde.org/")
    (synopsis1 "Fast and easy to use image viewer for KDE ")
    (synopsis2 "image viewer")
    (description1 "Gwenview is a fast and easy to use image viewer/browser for KDE.
All common image formats are supported, such as PNG(including transparency),
JPEG(including EXIF tags and lossless transformations), GIF, XCF (Gimp
image format), BMP, XPM and others. Standard features include slideshow,
fullscreen view, image thumbnails, drag'n'drop, image zoom, full network
transparency using the KIO framework, including basic file operations and
browsing in compressed archives, non-blocking GUI with adjustable views.
Gwenview also provides image and directory KParts components for use e.g. in
Konqueror. Additional features, such as image renaming, comparing,
converting, and batch processing, HTML gallery and others are provided by the
KIPI image framework.")
    (description2 "Gwenview is an image viewer, ideal for browsing and displaying a collection of
images.  It is capable of showing images in a full-screen slideshow view and
making simple adjustments, such as rotating or cropping images.

This package is part of the KDE graphics module.")
    (license unknown) ;; TODO: check this
))
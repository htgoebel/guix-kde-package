(define-public kgpg
  (package
    (name "kgpg")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kgpg-" version ".tar.xz"))
      (sha256
       (base32 "0abh15j2p0vinskd8f1yvjkyi1a70k0wf1sdldrfdwpdgq1pqsxw"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/27aqyglfpckb70dmk9agnsx74yni8i5h-kgpg-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kcodecs" ,kcodecs)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kjobwidgets" ,kjobwidgets)
      ("knotifications" ,knotifications)
      ("kservice" ,kservice)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 5.28)
;; find_package(KF5 AkonadiContact Contacts)
;; find_package(Gpgme REQUIRED)
;; qt5_add_dbus_interface(kgpg_SRCS org.kde.kgpg.Key.xml kgpg_interface )
;; 	set_property(SOURCE ${kgpg_transaction_SRCS} APPEND PROPERTY COMPILE_DEFINITIONS "KGPG_DEBUG_TRANSACTIONS")
;; set_property(SOURCE main.cpp APPEND PROPERTY COMPILE_DEFINITIONS "KGPG_VERSION=\"${KDE_APPLICATIONS_VERSION}\"")
    (home-page "http://www.kde.org/")
    (synopsis1 "Control your GPG keys ")
    (synopsis2 "graphical front end for GNU Privacy Guard")
    (description1 "KGpg is a simple interface for GnuPG, a powerful encryption utility.")
    (description2 "Kgpg manages cryptographic keys for the GNU Privacy Guard, and can encrypt,
decrypt, sign, and verify files.  It features a simple editor for applying
cryptography to short pieces of text, and can also quickly apply cryptography
to the contents of the clipboard.

This package is part of the KDE Utilities module.")
    (license gpl-2+3+kdeev) ;; TODO: check this
))
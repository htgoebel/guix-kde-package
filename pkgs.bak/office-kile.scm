(define-public kile
  (package
    (name "kile")
    (version "2.1.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "http://sourceforge.net/projects/kile/files/stable/kile-" version "/kile-" version ".tar.bz2"))
      (sha256
       (base32 "18nfi37s46v9xav7vyki3phasddgcy4m7nywzxis198vr97yqqx0"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Office")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/wn8qcgwbk5kr5s7dgrz340mx47vryj52-kile-2.1.3.tar.bz2
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 4.2.0 REQUIRED)
;; find_package( SharedMimeInfo REQUIRED )
;; ENDMACRO( KILE_ADD_DOCBOOK_DIRECTORY _sources _docbook _language )
;; 	GET_FILENAME_COMPONENT(_base_name ${_file} NAME_WE)
;; ENDMACRO( KILE_ADD_TRANSLATION_DIRECTORY _sources _relative_dir _file _language )
;; 	FOREACH( _entry ${_subdirectories} )
;; 			GET_FILENAME_COMPONENT( _language ${_entry} NAME )
;; 				KILE_ADD_TRANSLATION_DIRECTORY( ${_translation_sources} translations/${_language}/messages ${kile_PO_FILE} ${_language} )
;; 				KILE_ADD_DOCBOOK_DIRECTORY( ${_documentation_sources} translations/${_language}/doc ${kile_DOCBOOK} ${_language} )
;; 	ENDFOREACH( _entry ${_subdirectories} )
;; ENDMACRO( KILE_ADD_TRANSLATION_DIRECTORIES _documentation_sources _translation_sources _dir )
;; 	FIND_PROGRAM( KILE_MSGFMT "msgfmt" DOC "path of the msgfmt executable")
;; ENDMACRO( KILE_FIND_DOC_TOOLS )
;; 	KILE_FIND_DOC_TOOLS( )
;; 		KILE_ADD_DOCBOOK_DIRECTORY( kile_DOCUMENTATION_SOURCES doc index.docbook en )
;; 		KILE_ADD_TRANSLATION_DIRECTORIES( kile_DOCUMENTATION_SOURCES kile_TRANSLATION_SOURCES ${kile_SOURCE_DIR}/translations )
;; update_xdg_mimetypes( ${XDG_MIME_INSTALL_DIR} )
    (home-page "http://kile.sourceforge.net")
    (synopsis1 "Integrated LaTeX Environment for KDE4 ")
    (synopsis2 "KDE Integrated LaTeX Environment")
    (description1 "Kile is an integrated LaTeX Environment for KDE4.")
    (description2 "Kile is a user-friendly LaTeX source editor and TeX shell for KDE.

The source editor is a multi-document editor designed for .tex and .bib
files.  Menus, wizards and auto-completion are provided to assist with
tag insertion and code generation.  A structural view of the document
assists with navigation within source files.

The TeX shell integrates the various tools required for TeX processing.
It assists with LaTeX compilation, DVI and postscript document viewing,
generation of bibliographies and indices and other common tasks.

Kile can support large projects consisting of several smaller files.")
    (license unknown) ;; TODO: check this
))
(define-public kanagram
  (package
    (name "kanagram")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kanagram-" version ".tar.xz"))
      (sha256
       (base32 "183hkxxwf7h335gmfvi2lppsyhpv80lvlg1fg4whq79f1d2lrw47"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/b9qlp2jphfk93zy3xxlqcby49mxx8icc-kanagram-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("sonnet" ,sonnet)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 TextToSpeech)
;; find_package(LibKEduVocDocument REQUIRED)
    (home-page "https://projects.kde.org/projects/kde/kdeedu/kanagram")
    (synopsis1 "Word learning program ")
    (synopsis2 "jumble word puzzle")
    (description1 "Kanagram is a replacement for KMessedWords. Kanagram mixes up the letters
of a word (creating an anagram), and you have to guess what the mixed up
word is. Kanagram features several built-in word lists, hints, and a cheat
feature which reveals the original word. Kanagram also has a vocabulary
editor, so you can make your own vocabularies, and distribute them through
Kanagram's KNewStuff download service.")
    (description2 "KAnagram is a game where a random word is shown with its letters scrambled.
To win, the player must rearrange the letters into the correct order.

This package is part of the KDE education module")
    (license gpl2+) ;; TODO: check this
))
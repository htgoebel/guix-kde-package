(define-public cervisia
  (package
    (name "cervisia")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/cervisia-" version ".tar.xz"))
      (sha256
       (base32 "14r55ngvz4rci1h3iqdwbfcyxmm2b04c5smkv9x0bqxq4zz2yfvk"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Development")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/6yw28yr0jz5w8jvabma6i4cysv986mj3-cervisia-16.12.3.tar.xz
    (inputs
     `(("kdesu" ,kdesu)
      ("kiconthemes" ,kiconthemes)
      ("kinit" ,kinit)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("kparts" ,kparts)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "http://www.kde.org/")
    (synopsis1 "K Desktop Environment - Software Development Kit ")
    (synopsis2 "graphical CVS client")
    (description1 "Software Development Kit for the K Desktop Environment.")
    (description2 "Cervisia is a front-end for the CVS version control system client.

In addition to basic and advanced CVS operations, it provides a convenient
graphical interface for viewing, editing, and manipulating files in a CVS
repository or working directory.  It includes tools designed to ease the use
of CVS, such as a log browser, conflict resolver, and changelog editor that
checks for incorrect formatting.

This package is part of the KDE Software Development Kit module.")
    (license fdl-1.2+) ;; TODO: check this
))
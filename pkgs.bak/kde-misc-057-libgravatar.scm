(define-public libgravatar
  (package
    (name "libgravatar")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libgravatar-" version ".tar.xz"))
      (sha256
       (base32 "0m726ixss72rz3gwgn7q5s34xwbghi877y7gxa1ilcrk9rhyxv2f"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/458ka4avdxnsrplqhw5b39hakkp33x82-libgravatar-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 PimCommon ${PIMCOMMON_LIB_VERSION})
    (home-page "")
    (synopsis1 "")
    (synopsis2 "Perl interface to make URLs for Gravatars from an email address")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public telepathy-logger-qt
  (package
    (name "telepathy-logger-qt")
    (version "15.04.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/telepathy-logger-qt/15.04/src/telepathy-logger-qt-" version ".tar.xz"))
      (sha256
       (base32 "163qwlpdgpfgdmk8qzvnc4jhq4j0lrmwag7rqq38i7yx1vjppkl3"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/j0ifpi0i6ijc84grysv853fna8ywlzr1-telepathy-logger-qt-15.04.0.tar.xz
    (inputs
     `(("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(TelepathyQt5 ${TELEPATHY_QT_MIN_VERSION} REQUIRED)
;; find_package(GLIB2 REQUIRED)
;; find_package(GObject REQUIRED)
;; find_package(DBus REQUIRED)
;; find_package(DBusGLib REQUIRED)
;; find_package(LibXml2 REQUIRED)
;; find_package(TelepathyGlib ${TELEPATHY_GLIB_MIN_VERSION} REQUIRED)
;; find_package(PythonLibrary REQUIRED)
;; find_package(TelepathyLogger ${TELEPATHY_LOGGER_MIN_VERSION} REQUIRED)
    (home-page "https://projects.kde.org/projects/extragear/network/telepathy/telepathy-logger-qt")
    (synopsis1 "Telepathy Logging for Qt ")
    (synopsis2 "Qt bindings for the telepathy-logger library")
    (description1 "Telepathy Logging for Qt")
    (description2 "This library provides Qt-style C++ bindings for the telepathy-logger library.

The telepathy-logger library provides access to IM chat and event logs
that are recorded by the telepathy-logger daemon.")
    (license lgpl2.1+) ;; TODO: check this
))
(define-public kturtle
  (package
    (name "kturtle")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kturtle-" version ".tar.xz"))
      (sha256
       (base32 "1mpdwb6999nar16mpha30cf4qzmpbsdha44aw77gn301v215rwj3"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/i82wmpifg4i3gkx58gh1w1x1mjd95vcn-kturtle-16.12.3.tar.xz
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("ktextwidgets" ,ktextwidgets)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "https://projects.kde.org/projects/kde/kdeedu/kturtle")
    (synopsis1 "An educational programming environment ")
    (synopsis2 "educational programming environment")
    (description1 "KTurtle is an educational programming environment for the KDE Desktop.
KTurtle aims to make programming as easy and touchable as possible, and
therefore can be used to teach kids the basics of math, geometry
and... programming.")
    (description2 "KTurtle is an educational programming environment which uses the TurtleScript
programming language (inspired by Logo) to make programming as easy and
accessible as possible.

The user issues TurtleScript language commands to control the "turtle", which
draws on the canvas, making KTurtle suitable for teaching elementary
mathematics, geometry and programming.

This package is part of the KDE education module.")
    (license gpl2+) ;; TODO: check this
))
(define-public libkleo
  (package
    (name "libkleo")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkleo-" version ".tar.xz"))
      (sha256
       (base32 "0rzp2bxqij9fkdsghskd2f05vgcybdc9l7wdrjqhhcngi8qxl0nn"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/z3lz5gvsilval0a9b6ckh4j2c5y6k920-libkleo-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kitemmodels" ,kitemmodels)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 PimTextEdit ${KDEPIMTEXTEDIT_VERSION})
;; find_package(Gpgmepp ${GPGMEPP_LIB_VERSION})
;; find_package(Boost 1.34.0)
    (home-page "")
    (synopsis1 "KDE keymanagement library ")
    (synopsis2 "KDE PIM cryptographic library")
    (description1 "KDE keymanagement library.")
    (description2 "")
    (license ) ;; TODO: check this
))
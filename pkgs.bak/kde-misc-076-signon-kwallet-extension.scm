(define-public signon-kwallet-extension
  (package
    (name "signon-kwallet-extension")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/signon-kwallet-extension-" version ".tar.xz"))
      (sha256
       (base32 "0fnqdcin471hlw694vb6z9ybgw31778rhnryc7zps40kjwfdxhcc"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/b18ql9qq5pfrsja8kmx6hwf63g13h34n-signon-kwallet-extension-16.12.3.tar.xz
    (inputs
     `(("kwallet" ,kwallet)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(SignOnExtension REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "KWallet extension for signond")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
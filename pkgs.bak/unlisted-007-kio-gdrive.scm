;; Found on download-server, maybe already included in another package
(define-public kio-gdrive
  (package
    (name "kio-gdrive")
    (version "1.1.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kio-gdrive/" version "/src/kio-gdrive-" version ".tar.xz"))
      (sha256
       (base32 "1sj0nlfslxlcaqmzffl5kf9rrgbqmh4iqxvmymgi772ghhg1ah68"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unlisted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/l9ispr7fxzkw68wbjrs24508sqfrk521-kio-gdrive-1.1.1.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 GAPI ${KGAPI_MIN_VERSION})
;; find_package(Qt5 Keychain)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
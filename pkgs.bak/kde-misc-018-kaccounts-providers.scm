(define-public kaccounts-providers
  (package
    (name "kaccounts-providers")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kaccounts-providers-" version ".tar.xz"))
      (sha256
       (base32 "0iqqwfjadsf7nhrqvpzypazipris4ljhf6daprxwxqa2bfigr5j2"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/n5am32v9dxwx0mqffbdargz8jdl2nxm0-kaccounts-providers-16.12.3.tar.xz
    (inputs
     `(("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kpackage" ,kpackage)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; left-overs from CMakeLists.txt:
;; find_package(Intltool REQUIRED)
;; find_package(KAccounts REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "KDE providers for accounts sign-on")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
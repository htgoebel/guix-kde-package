(define-public kde-baseapps
  (package
    (name "kde-baseapps")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kde-baseapps-" version ".tar.xz"))
      (sha256
       (base32 "18a2r2g5cz4s923p3369aam4qr00gsngk6nn4lk3k5a4qqavpsa7"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/9mgn8bnsi35jmdngwgh0xp09163hspqg-kde-baseapps-16.08.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 4.10.60 REQUIRED)
;; find_package(ZLIB)
;; find_package(KActivities 6.1.0)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
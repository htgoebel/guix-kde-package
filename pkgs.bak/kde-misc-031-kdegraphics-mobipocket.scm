(define-public kdegraphics-mobipocket
  (package
    (name "kdegraphics-mobipocket")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdegraphics-mobipocket-" version ".tar.xz"))
      (sha256
       (base32 "06zqny8idaw7s85h7iprbwdp7y1qspfp7yh5klwav12p72nn54w2"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/8zxhndscdczvq06jrl6c2jq3zsk0d7xj-kdegraphics-mobipocket-16.12.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "A collection of plugins to handle mobipocket files ")
    (synopsis2 "")
    (description1 "A collection of plugins to handle mobipocket files.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public kcalcore
  (package
    (name "kcalcore")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kcalcore-" version ".tar.xz"))
      (sha256
       (base32 "09i43vs6jpjmmk52df6pzclh0jn8shn3wfnaivw2wlirwa60d901"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/07ll4yhdmyf77hmkspjj3sw8c3j3c079-kcalcore-16.12.3.tar.xz
    (inputs
     `(("kdelibs4support" ,kdelibs4support)))
;; left-overs from CMakeLists.txt:
;; find_package(LibIcal ${LibIcal_MIN_VERSION})
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "library for handling calendar data")
    (description1 "An API for the iCalendar and vCalendar formats.")
    (description2 "")
    (license ) ;; TODO: check this
))
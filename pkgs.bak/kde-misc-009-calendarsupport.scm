(define-public calendarsupport
  (package
    (name "calendarsupport")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/calendarsupport-" version ".tar.xz"))
      (sha256
       (base32 "0r30z2wzyms7m7s8y0livsfy5pj82g988bs6amaj2571hz55d8y0"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
;; /gnu/store/c2zsjqrnqsmi3b1lqcc0yc762q01n55m-calendarsupport-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Akonadi)
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiMime ${AKONADI_MIMELIB_VERSION})
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 CalendarCore ${KCALENDARCORE_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
;; find_package(KF5 Holidays ${KHOLIDAYS_LIB_VERSION})
;; find_package(KF5 AkonadiCalendar ${AKONADICALENDAR_LIB_VERSION})
;; find_package(KF5 PimCommon ${PIMCOMMON_LIB_VERSION})
;; find_package(KF5 KdepimDBusInterfaces ${KDEPIM_LIB_VERSION})
    (home-page "")
    (synopsis1 "KDEPIM 4 library ")
    (synopsis2 "KDE PIM Calendar support - library")
    (description1 "KDEPIM 4 library.")
    (description2 "")
    (license ) ;; TODO: check this
))
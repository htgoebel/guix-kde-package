(define-public juk
  (package
    (name "juk")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/juk-" version ".tar.xz"))
      (sha256
       (base32 "1ssgia5sknam2hnjflzglv0khxbwyyvzm4w1wmxw04rbjzs4gi4h"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Multimedia")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/ggw41w9ssyylj79x5zqnf9rpb0lxjyrr-juk-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package( KDE4 REQUIRED )
;; find_package( Taglib 1.6 )
;; macro_optional_find_package(TunePimp)
;;
;;
;;
;; macro_push_required_vars()
;; macro_pop_required_vars()
;;
;; qt4_add_dbus_adaptor( juk_SRCS org.kde.juk.collection.xml
;;     dbuscollectionproxy.h DBusCollectionProxy )
;; qt4_add_dbus_adaptor( juk_SRCS org.kde.juk.player.xml playermanager.h PlayerManager)
;; qt4_add_dbus_adaptor( juk_SRCS org.kde.juk.search.xml searchwidget.h SearchWidget)
;;  )
;;  )
    (home-page "http://www.kde.org/")
    (synopsis1 "A music player and manager for KDE ")
    (synopsis2 "music jukebox / music player")
    (description1 "JuK is an audio jukebox application, supporting collections of MP3, Ogg
Vorbis, and FLAC audio files. It allows you to edit the "tags" of your
audio files, and manage your collection and playlists. It's main focus,
in fact, is on music management.")
    (description2 "JuK is a powerful music player capable of managing a large music collection.

Some of JuK's features include:
 * Music collection, playlists, and smart playlists
 * Tag editing support, including the ability to edit multiple files at once
 * Tag-based music file organization and renaming
 * CD burning support using k3b
 * Album art using Google Image Search

This package is part of the KDE multimedia module.")
    (license license:gpl2+) ;; TODO: check this
))
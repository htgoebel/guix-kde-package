(define-public kmouth
  (package
    (name "kmouth")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kmouth-" version ".tar.xz"))
      (sha256
       (base32 "1afvjds1kfb8zvv3ma8c6svan6zlbd1w9a164vxwp7gl3ycjyj52"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/105x726m2a140ryfcbzgfyna2w5whqqz-kmouth-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; qt4_add_dbus_interface(kmouth_SRCS ${kspeech_xml} kspeech_interface)
    (home-page "http://www.kde.org")
    (synopsis1 "A type-and-say front end for speech synthesizers ")
    (synopsis2 "type-and-say frontend for speech synthesizers")
    (description1 "KMouth is a program which enables persons that cannot speak to let their
computer speak, e.g. mutal people or people who have lost their voice. It has a
text input field and speaks the sentences that you enter. It also has support
for user defined phrasebooks.")
    (description2 "KDE's type-and-say frontend for speech synthesizers.

It includes a history of spoken sentences from which the user can select
sentences to be re-spoken.

This package is part of the KDE accessibility module.")
    (license GFDL-NIV-1.2+) ;; TODO: check this
))
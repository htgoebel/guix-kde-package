(define-public kde-dev-scripts
  (package
    (name "kde-dev-scripts")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kde-dev-scripts-" version ".tar.xz"))
      (sha256
       (base32 "0kiddn0wg90p98zgnpq3x2hcfw8xczn98nyd21zbkzz357v14pya"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/ljjaiyyf72d0lqmdwjz1i8l836fyjc1x-kde-dev-scripts-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; function(create_manpage)
;;     kdoctools_create_manpage(${ARGN})
;;   endfunction()
;;   find_package(KDE4)
;;   function(create_manpage)
;;     kde4_create_manpage(${ARGN})
;;   endfunction()
    (home-page "")
    (synopsis1 "K Desktop Environment - Software Development Kit ")
    (synopsis2 "")
    (description1 "This package contains the scripts for KDE development which are contained in the
kdesdk module.")
    (description2 "")
    (license ) ;; TODO: check this
))
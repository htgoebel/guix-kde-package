(define-public kmousetool
  (package
    (name "kmousetool")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kmousetool-" version ".tar.xz"))
      (sha256
       (base32 "1678sjj96f22sn60ccyj6hqi2vghkf4facnx8l15x4xx05yq1vgg"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/jmkiwd81q9kvdfyva3zdq8isfqa9zn4q-kmousetool-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
    (home-page "http://www.kde.org")
    (synopsis1 "Automatic Mouse Click ")
    (synopsis2 "mouse manipulation tool for the disabled")
    (description1 "KMouseTool is a tool that clicks the mouse for you.")
    (description2 "KMouseTool clicks the mouse whenever the mouse cursor pauses briefly. It was
designed to help those with repetitive strain injuries, for whom pressing
buttons hurts.

This package is part of the KDE accessibility module.")
    (license MIT) ;; TODO: check this
))
;; Found on download-server, maybe already included in main package
(define-public kdev-python
  (package
    (name "kdev-python")
    (version "5.0.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kdevelop/" version "/src/kdev-python-" version ".tar.xz"))
      (sha256
       (base32 "1zqi9rg4w4m3nr5hwl24f9b84fn9w0j9vqa3li16fbryjd8hj5jx"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Development")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/h4hcxbz41h5j9k9q2raxy8k7z6s62v3k-kdev-python-5.0.2.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("ki18n" ,ki18n)
      ("kitemmodels" ,kitemmodels)
      ("knewstuff" ,knewstuff)
      ("ktexteditor" ,ktexteditor)
      ("qtbase" ,qtbase)
      ("threadweaver" ,threadweaver)))
;; left-overs from CMakeLists.txt:
;; find_package(Python 3.5 REQUIRED)
;;  OR "${PYTHON_VERSION_MINOR}" GREATER 5 )
;; find_package(KDevPlatform ${KDEVPLATFORM_VERSION} REQUIRED)
;; find_package(KDevelop ${KDEVPLATFORM_VERSION} REQUIRED)
    (home-page "")
    (synopsis1 "Python plugin for KDevelop ")
    (synopsis2 "")
    (description1 "Python plugin for KDevelop.")
    (description2 "")
    (license ) ;; TODO: check this
))
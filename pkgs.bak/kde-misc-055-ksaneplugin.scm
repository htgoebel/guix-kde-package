(define-public ksaneplugin
  (package
    (name "ksaneplugin")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ksaneplugin-" version ".tar.xz"))
      (sha256
       (base32 "1z0ziapcjmi7fqfnb0zsbjgd1q05np1s7smj1k8cd8c6f169yrld"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/n7kj9hb3y2663jypj1awan20nqzhqr41-ksaneplugin-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; find_package(KSane REQUIRED)
    (home-page "https://projects.kde.org/projects/kde/kdegraphics/ksaneplugin")
    (synopsis1 "KDE Scan Service ")
    (synopsis2 "KScan plugin for scanning through libksane")
    (description1 "This is a KScan plugin that implements the scanning through libksane.")
    (description2 "This is a KScan plugin that implements the scanning through libksane.

This package is part of the KDE graphics module.")
    (license bsd-3) ;; TODO: check this
))
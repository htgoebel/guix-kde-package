(define-public sweeper
  (package
    (name "sweeper")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/sweeper-" version ".tar.xz"))
      (sha256
       (base32 "1vf4840l233gji4sjkg9gz2pr98kin5sz37kj645z75vikwmk3al"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/fl0pk0bb7153p1m3sivdhdvp1sab0hgv-sweeper-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4)
;;
;; qt4_add_dbus_adaptor( sweeper_SRCS org.kde.sweeper.xml sweeper.h Sweeper)
    (home-page "http://www.kde.org/")
    (synopsis1 "KDE System Cleaner ")
    (synopsis2 "history and temporary file cleaner")
    (description1 "Sweeper helps to clean unwanted traces the user leaves on the system and to
regain disk space removing unused temporary files.")
    (description2 "Sweeper can quickly remove temporary information, such as web page cookies,
browser history, or the list of recently-opened documents.  It helps provide
additional privacy on a system shared between multiple users.

This package is part of the KDE SC utilities module.")
    (license lgpl2.1) ;; TODO: check this
))
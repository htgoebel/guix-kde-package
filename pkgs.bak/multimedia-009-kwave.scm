(define-public kwave
  (package
    (name "kwave")
    (version "0.9.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "https://sourceforge.net/projects/kwave/files/kwave/" version "/kwave-" version "-1.tar.bz2/download"))
      (sha256
       (base32 "0mwn061lccfj02scx79lm17bh9d61fx22jq2hbc3sy3d5kniv875"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Multimedia")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/b8as9qkczxizw88zzwxzs3rs0psmvsk0-download
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kservice" ,kservice)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtmultimedia" ,qtmultimedia)))
;; left-overs from CMakeLists.txt:
;; FIND_REQUIRED_PROGRAM(CAT_EXECUTABLE cat)
;; EXECUTE_PROCESS(
;;     COMMAND ${CMAKE_C_COMPILER} -dumpversion
;;     OUTPUT_VARIABLE COMPILER_VERSION
;;     OUTPUT_STRIP_TRAILING_WHITESPACE
;; )
;; GET_FILENAME_COMPONENT(COMPILER_SHORT "${CMAKE_C_COMPILER}" NAME_WE CACHE)
;; EXECUTE_PROCESS(
;;     COMMAND ${CMAKE_C_COMPILER} -dumpmachine
;;     OUTPUT_VARIABLE MACHINE
;;     OUTPUT_STRIP_TRAILING_WHITESPACE
;; )
;; OPTION(WITH_OPTIMIZED_MEMCPY "enable optimized memcpy [default=on]" ON)
;;     STRING(REGEX MATCH "(i.86-*)|(athlon-*)|(pentium-*)" _mach_x86 ${MACHINE})
;;  for X86 (from xine)")
;;     STRING(REGEX MATCH "(x86_64-*)|(X86_64-*)|(AMD64-*)|(amd64-*)" _mach_x86_64 ${MACHINE})
;;  for X86_64 (from xine)")
;;     STRING(REGEX MATCH "(ppc-*)|(powerpc-*)" _mach_ppc ${MACHINE})
;;  for PPC (from xine)")
;; CHECK_C_COMPILER_FLAG(" ${CMAKE_SHARED_LIBRARY_C_FLAGS}" C_HAVE_PIC)
;; LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/libgui)
;; LINK_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR}/libkwave)
;; FIND_PACKAGE()
;; OPTION(WITH_QT_AUDIO "enable playback via Qt Multimedia [default=on]" ON)
;;     FIND_PACKAGE()
;; FIND_PACKAGE()
;; FIND_PACKAGE()
;;     SET_PACKAGE_PROPERTIES(KF5DocTools
;;         PROPERTIES DESCRIPTION "Tools to generate documentation"
;;         TYPE OPTIONAL
;;     )
;; FEATURE_SUMMARY(WHAT ALL INCLUDE_QUIET_PACKAGES FATAL_ON_MISSING_REQUIRED_PACKAGES)
;; INCLUDE_DIRECTORIES(
;;     ${INTERFACE_INCLUDE_DIRECTORIES}
;;     ${CMAKE_CURRENT_SOURCE_DIR}
;;     ${CMAKE_CURRENT_BINARY_DIR}
;; )
;; CHECK_INCLUDE_FILES(sys/times.h HAVE_SYS_TIMES_H)
;; CHECK_INCLUDE_FILES(signal.h HAVE_SIGNAL_H)
;; CHECK_INCLUDE_FILES("${_inc_c}" HAVE_REQUIRED_STD_C_HEADERS)
;; CHECK_INCLUDE_FILES_CXX("${_inc_cpp}")
;; CHECK_FUNCTION_EXISTS(unlink HAVE_UNLINK)
;; OPTION(DEBUG "build debug code [default=off]" OFF)
;;     CHECK_C_COMPILER_FLAG("-Og" C_HAVE_OG)
;; OPTION(DEBUG_MEMORY "enable memory management debug code [default=off]" OFF)
;; OPTION(WITH_DOC "build online documentation [default=on]" ON)
;; FIND_PROGRAM(SED_EXECUTABLE NAMES sed)
;; FIND_REQUIRED_PROGRAM(RMDIR_EXECUTABLE rmdir)
;; FIND_PROGRAM(RM_EXECUTABLE rm)
;; FIND_PROGRAM(DOXYGEN_EXECUTABLE doxygen)
;;     SET_DIRECTORY_PROPERTIES(PROPERTIES
;; 	ADDITIONAL_MAKE_CLEAN_FILES
;; 	"${KWAVE_ADDITIONAL_CLEAN_FILES}"
;;     )
    (home-page "http://kwave.sourceforge.net")
    (synopsis1 "A sound editor for KDE ")
    (synopsis2 "sound editor for KDE")
    (description1 "Kwave is a sound editor designed for the KDE Desktop Environment.

With Kwave you can edit many sorts of wav-files including multi-channel
files. You are able to alter and play back each channel on its own.
Kwave also includes many plugins (most are still under development) to
transform the wave-file in several ways and presents a graphical view
with a complete zoom- and scroll capability.")
    (description2 "Kwave is a sound editor designed for the KDE Desktop Environment.

With Kwave you can record, play back, import and edit many sorts of audio
files including multi-channel files.

Kwave includes some plugins to transform audio files in several ways and
presents a graphical view with a complete zoom- and scroll capability.

Its features include:
 * 24 Bit Support
 * Undo/Redo
 * Use of multicore CPUs (SMP, hyperthreading)
 * Simple Drag & Drop
 * Realtime Pre-Listen for some effects
 * Support for multi-track files
 * Playback and recording via native ALSA (or OSS, deprecated)
 * Playback via PulseAudio and Phonon
 * Load and edit-capability for large files (can use virtual memory)
 * Reading and auto-repair of damaged wav-files
 * Supports multiple windows
 * Extendable Plugin interface
 * a nice splashscreen
 * some label handling")
    (license license:unknown) ;; TODO: check this
))
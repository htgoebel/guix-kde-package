(define-public dolphin
  (package
    (name "dolphin")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/dolphin-" version ".tar.xz"))
      (sha256
       (base32 "0y35ljbjib4pvyngdgbq1yx3rfmy94crpa7v1zzwfav94lm3kwb2"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "System")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/4k9bnaf5d3bc9lpikzhwlr426dqp4mm5-dolphin-16.12.3.tar.xz
    (inputs
     `(("baloo" ,baloo)
      ("kactivities" ,kactivities)
      ("kbookmarks" ,kbookmarks)
      ("kcmutils" ,kcmutils)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kinit" ,kinit)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("kparts" ,kparts)
      ("ktextwidgets" ,ktextwidgets)
      ("kwindowsystem" ,kwindowsystem)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("solid" ,solid)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 BalooWidgets)
;; find_package(KF5 FileMetaData)
;;     find_package(KF5 # for KFileMetaDataWidget)
    (home-page "")
    (synopsis1 "File manager for KDE focusing on usability ")
    (synopsis2 "file manager")
    (description1 "Dolphin is a file manager for KDE focusing on usability.
The main features of Dolphin are:
- Navigation bar for URLs, which allows to navigate quickly
     through the file hierarchy.
- View properties are remembered for each folder.
- Split of views is supported.
- Network transparency.
- Undo/redo functionality.
- Renaming of a variable number of selected items in one step.

Dolphin is not intended to be a competitor to Konqueror: Konqueror
acts as universal viewer being able to show HTML pages, text documents,
directories and a lot more, whereas Dolphin focuses on being only a file
manager. This approach allows to optimize the user interface for the task
of file management.")
    (description2 "")
    (license ) ;; TODO: check this
))
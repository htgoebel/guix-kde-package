(define-public khangman
  (package
    (name "khangman")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/khangman-" version ".tar.xz"))
      (sha256
       (base32 "03ffigr9a6n3aj1a7lxcw9wgf1pafdjwqjnwnny2ric5vn6lpq1z"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/hmr2qlcs8lqkxikyr4ls6f62qh66036v-khangman-16.12.3.tar.xz
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; find_package(LibKEduVocDocument REQUIRED)
    (home-page "http://edu.kde.org/")
    (synopsis1 "Classical hangman game ")
    (synopsis2 "Hangman word puzzle")
    (description1 "KHangman is the classical hangman game. The child should guess a word
letter by letter. At each miss, the picture of a hangman appears. After
10 tries, if the word is not guessed, the game is over and the answer
is displayed.")
    (description2 "KHangMan is the well-known Hangman game, aimed towards children aged 6 and
above.

It picks a random word which the player must reveal by guessing if it contains
certain letters.  As the player guesses letters, the word is gradually
revealed, but 10 wrong guesses will end the game.

This package is part of the KDE education module.")
    (license gpl2+) ;; TODO: check this
))
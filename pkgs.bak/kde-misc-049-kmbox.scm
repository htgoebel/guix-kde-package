(define-public kmbox
  (package
    (name "kmbox")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kmbox-" version ".tar.xz"))
      (sha256
       (base32 "0khvf4kqf9i425fjczl1miqsz0pxbyryxy32bf9knr3k5kmwbn24"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/xvgsxv0cpzqva71h524fgi9gpffd87bp-kmbox-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Mime)
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "library for handling mbox mailboxes")
    (description1 "A library for accessing mail storages in MBox format.")
    (description2 "")
    (license ) ;; TODO: check this
))
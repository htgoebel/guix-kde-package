(define-public kdelibs
  (package
    (name "kdelibs")
    (version "4.14.26")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/16.08.3/src/kdelibs-" version ".tar.xz"))
      (sha256
       (base32 "043asa5i30s3j1knpshhxw1a5a1alh1b5b719gdzpm1hxgn3f79r"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/1rn1kaf21ywm6n8m4g3a8mymyhj3immd-kdelibs-4.14.26.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4Internal REQUIRED)
;;    find_package(Carbon REQUIRED)
;;  development package could not be found.\nPlease install libSM.\n")
;; find_package(Perl)
;; find_package(ZLIB)
;; find_package(Strigi 0.6.3)
;; find_package(LibAttica 0.4.2)
;; " X11_Xrender_FOUND "Support for compositing, rendering operations, and alpha-blending. STRONGLY RECOMMENDED")
;; " HAVE_XSCREENSAVER "Support for KIdleTime (fallback mode)")
;; " HAVE_XSYNC "Efficient operation of KIdleTime. STRONGLY RECOMMENDED")
;;  nor XScreensaver (libXss) development package was found.\nPlease install one of them (XSync is recommended)\n")
;; macro_optional_find_package(OpenSSL)
;; "
;;                        URL "http://openssl.org"
;;                        TYPE RECOMMENDED
;;                        PURPOSE "KDE uses OpenSSL for the bulk of secure communications, including secure web browsing via HTTPS"
;;                       )
;; macro_optional_find_package(Libintl)
;; macro_optional_find_package(Soprano 2.7.56 COMPONENTS PLUGIN_RAPTORPARSER PLUGIN_REDLANDBACKEND)
;; macro_optional_find_package(SharedDesktopOntologies 0.10)
;; macro_optional_find_package(QCA2 2.0.0)
;; find_package(DBusMenuQt)
;; macro_ensure_out_of_source_build("kdelibs requires an out of source build. Please create a separate build directory and run 'cmake path_to_kdelibs [options]' there.")
;; macro_optional_find_package(ACL)
;; "
;;         CODE "set(EXPORT_INSTALL_DIR \"${DATA_INSTALL_DIR}/cmake/modules\")"
;;         SCRIPT "${CMAKE_SOURCE_DIR}/cmake/modules/check_installed_exports_file.cmake" )
    (home-page "")
    (synopsis1 "KDE 4 system core files ")
    (synopsis2 "Porting aid from KDELibs4.")
    (description1 "KDE 4 system core files.
---
KDE 4 system core files.")
    (description2 "")
    (license ) ;; TODO: check this
))
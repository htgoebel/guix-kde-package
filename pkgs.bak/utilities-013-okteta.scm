(define-public okteta
  (package
    (name "okteta")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/okteta-" version ".tar.xz"))
      (sha256
       (base32 "14wmacbxc5kc3y5y5lsdxgsswi1jdvlxsd0xqcims50xjpb8znpd"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)
      ("qttools" ,qttools)))
;; /gnu/store/h8vwf7n03kkm22w6kj89db7gx900zmlp-okteta-16.12.3.tar.xz
    (inputs
     `(("kbookmarks" ,kbookmarks)
      ("kcmutils" ,kcmutils)
      ("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("kparts" ,kparts)
      ("kservice" ,kservice)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtscript" ,qtscript)))
;; left-overs from CMakeLists.txt:
;; add_compile_options(-Wall)
;; find_package(Qca-qt5 2.1.0)
;;  #${OKTETALIBS_ABI_VERSION} )
;;  #${OKTETALIBS_ABI_VERSION} )
    (home-page "http://www.kde.org/")
    (synopsis1 "Editor for raw file data as Hex values ")
    (synopsis2 "hexadecimal editor for binary files")
    (description1 "Okteta is a simple editor for the raw data of files. This type of
program is also called hex editor or binary editor.")
    (description2 "Okteta is a simple editor for the raw data of files. This type of program is
also called hexadecimal editor or binary editor.

The data is displayed in the traditional view with two columns: one with the
numeric values and one with the assigned characters. Editing can be done both
in the value column and the character column. Besides the usual editing
capabilities Okteta also brings a small set of tools, like a table listing
decodings into common simple data types, a table listing all possible bytes
with its character and value equivalents, a info view with a statistic and a
filter tool. All modifications to the data loaded can be endlessly undone or
redone.

This package is part of the KDE Software Development Kit module.")
    (license fdl-1.2+) ;; TODO: check this
))
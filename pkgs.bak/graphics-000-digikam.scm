(define-public digikam
  (package
    (name "digikam")
    (version "5.5.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/digikam/digikam-" version ".tar.xz"))
      (sha256
       (base32 "0lk2rjndfbxlkwxzviq5m72rh56b5z07fzn9xdf27fdzildvz76z"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/yd5mamyyxpb8blgba2kc2z4x1dpc47pj-digikam-5.5.0.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; "            OFF)
;; "          OFF)
;; "                                                  ON)
;; "                                                      ON)
;; "           OFF)
;; "          OFF)
;; "      OFF)
;; "     OFF)
;; "                                                        ON)
;; "                                                        ON)
;;     find_package(Ruby)
;;     find_package(Subversion)
;;  want mean uncomplete translations files, which will be removed from compilation.")
;;     set_property(GLOBAL PROPERTY ALLOW_DUPLICATE_CUSTOM_TARGETS 1)
;; ")
;; ")
;; ")
;; ")
;; ")
;; ")
;;     find_package(Gettext REQUIRED)
    (home-page "http://www.digikam.org")
    (synopsis1 "A KDE photo management utility ")
    (synopsis2 "digital photo management application for KDE")
    (description1 "DigiKam is an advanced digital photo management application for KDE.
Photos can be collected into albums which can be sorted chronologically,
by directory layout or by custom collections.
DigiKam also provides tagging functionality. Images can be tagged despite of
their position and digiKam provides fast and intuitive ways to browse them.
User comments and customized meta-information added to images, are stored
into a database and retrieved to make them available into the user interface.
As soon as the camera is plugged in digikam allows you to preview, download,
upload and delete images.
Digikam also includes tools like Image Editor, to modify photos using plugins
such as red eye correction or Gamma correction, exif management,...
Light Table to make artistic photos and an external image editor such
as Showfoto.
Digikam also uses KIPI plugins (KDE Image Plugin Interface) to increase
its functionalities.")
    (description2 "This package contains private libraries to be used by digikam and
kipi-plugins.

This has no use for anything else than digikam, since the libraries are
installed in a private library path.")
    (license unknown) ;; TODO: check this
))
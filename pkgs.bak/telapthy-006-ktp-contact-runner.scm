(define-public ktp-contact-runner
  (package
    (name "ktp-contact-runner")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-contact-runner-" version ".tar.xz"))
      (sha256
       (base32 "1lcx9smfv2dqrwsbdd4srcq7dqap8bclz788p6jjn04xi6wcbbiq"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/xpf0h7ps6dqs2fmcyz21g1myprn8sq63-ktp-contact-runner-16.12.3.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("krunner" ,krunner)
      ("kservice" ,kservice)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
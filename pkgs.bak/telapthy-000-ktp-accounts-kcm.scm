(define-public ktp-accounts-kcm
  (package
    (name "ktp-accounts-kcm")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-accounts-kcm-" version ".tar.xz"))
      (sha256
       (base32 "0pr191i9fmz66wlv8krmy1pba7igd1qwa0akb4kdzkm4bx3z8wq3"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/cngz8ayhrn2m835hmyynlr4spldjsq2x-ktp-accounts-kcm-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kcodecs" ,kcodecs)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package (TelepathyQt5 REQUIRED)
;; find_package (KAccounts REQUIRED)
;; find_package (Intltool REQUIRED) #needed to generate service and provider files
;;  #for KAccountsMacros
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
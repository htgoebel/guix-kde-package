(define-public kfilereplace
  (package
    (name "kfilereplace")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kfilereplace-" version ".tar.xz"))
      (sha256
       (base32 "0gym9bmkyjwg97khy6xxiaidjp6wi98fzmk7wa97wbdqc0qvswja"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/dn6j4lalywab44qpg8lkyzqb6lwv5d3c-kfilereplace-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; qt4_add_dbus_adaptor( kfilereplacepart_PART_SRCS org.kde.kfilereplace.xml kfilereplacepart.h KFileReplacePart )
;; kde4_add_ui3_files(kfilereplacepart_PART_SRCS
;;    kfilereplaceviewwdg.ui
;;    kaddstringdlgs.ui
;;    knewprojectdlgs.ui
;;    koptionsdlgs.ui )
    (home-page "")
    (synopsis1 "KDE utility that replace some strings in many of files in one operation ")
    (synopsis2 "batch search-and-replace component")
    (description1 "KFileReplace is a very capable multi-line, mutli-file, multi-directory find and
replace that can create backups, use wildcards and regexps.")
    (description2 "")
    (license ) ;; TODO: check this
))
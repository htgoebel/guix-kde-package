(define-public ktp-kded-module
  (package
    (name "ktp-kded-module")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktp-kded-module-" version ".tar.xz"))
      (sha256
       (base32 "06zw8padspbn8ydcbs69v3ggmfpqrb3axxd2v1sgg4p4kdp0jyml"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Telapthy")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/pwhj17kbyfah02kvqcvf9fjf2ncivnq7-ktp-kded-module-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kidletime" ,kidletime)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
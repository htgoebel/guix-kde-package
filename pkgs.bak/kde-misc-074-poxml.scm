(define-public poxml
  (package
    (name "poxml")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/poxml-" version ".tar.xz"))
      (sha256
       (base32 "1cpc49hnslc2iabgnvda7967mmrdzykjydi8py67ycr9k1gx6pm5"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/pm859vbhapa3wh6chdj1k49qr6vlyag9-poxml-16.12.3.tar.xz
    (inputs
     `(("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(GettextPO REQUIRED)
;; find_package(Gettext)
    (home-page "http://www.kde.org/")
    (synopsis1 "Xml2po and vice versa converters ")
    (synopsis2 "tools for translating DocBook XML files with Gettext")
    (description1 "Software Development Kit for the K Desktop Environment.")
    (description2 "This is a collection of tools that facilitate translating DocBook XML
files using Gettext message files (PO files).

Also included are several command-line utilities for manipulating DocBook XML
files, PO files and PO template files.

This package is part of the KDE Software Development Kit module.")
    (license fdl-1.2+) ;; TODO: check this
))
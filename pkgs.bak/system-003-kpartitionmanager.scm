(define-public kpartitionmanager
  (package
    (name "kpartitionmanager")
    (version "2.2.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/partitionmanager/" version "/src/partitionmanager-" version ".tar.xz"))
      (sha256
       (base32 "0rxc4x4lmrxh796bvxsninghlfda6jb74haib4va85in78g57gdw"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "System")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/h1x7yiz80ayvn6sr4m04dhh8265f5xbk-partitionmanager-2.2.1.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kjobwidgets" ,kjobwidgets)
      ("kservice" ,kservice)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; #"
;; find_package(PkgConfig REQUIRED)
;; find_package(KPMcore 2.2.0 REQUIRED)
;; pkg_check_modules(BLKID REQUIRED blkid)
    (home-page "http://www.partitionmanager.org")
    (synopsis1 "KDE Partition Manager ")
    (synopsis2 "file, disk and partion management for KDE")
    (description1 "Easily manage disks, partitions and file systems on your KDE Desktop:
Create, resize, move, copy, back up, restore or delete partitions.")
    (description2 "Partition Manager is a utility program to help you manage the disk devices,
partitions and file systems on your computer. It allows you to easily create,
copy, move, delete, resize without losing data, backup and restore partitions.

Partition Manager supports a large number of file systems, including ext2/3/4,
reiserfs, NTFS, FAT16/32, jfs, xfs and more. Note that to gain support for a
specific file system other than ext2/3/4, you should install the corresponding
suggested package.

Partition Manager is based on libparted (like gparted) and makes use of the
KDE libraries for its user interface.")
    (license unknown) ;; TODO: check this
))
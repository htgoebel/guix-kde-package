(define-public libkeduvocdocument
  (package
    (name "libkeduvocdocument")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkeduvocdocument-" version ".tar.xz"))
      (sha256
       (base32 "05s79q269m5s78zjwxljxvprrqvpalf6h38n90m589vks82ahxx0"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/7khsbj2as6cwg1qyxc1b7v70lc9zq8wm-libkeduvocdocument-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 5.2)
;; find_package(KF5 5.3.0)
    (home-page "")
    (synopsis1 "Runtime library for KDE Education Application ")
    (synopsis2 "library for reading and writing vocabulary files")
    (description1 "Runtime library for KDE Education Application")
    (description2 "")
    (license ) ;; TODO: check this
))
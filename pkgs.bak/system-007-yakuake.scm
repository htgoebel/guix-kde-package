(define-public yakuake
  (package
    (name "yakuake")
    (version "3.0.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/yakuake/" version "/src/yakuake-" version ".tar.xz"))
      (sha256
       (base32 "0vcdji1k8d3pz7k6lkw8ighkj94zff2l2cf9v1avf83f4hjyfhg5"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "System")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/qk4figm2dwxdwdw4h7ixywfmzhyqv25v-yakuake-3.0.2.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kglobalaccel" ,kglobalaccel)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kparts" ,kparts)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("qtbase" ,qtbase)
      ("qtx11extras" ,qtx11extras)))
;; left-overs from CMakeLists.txt:
;; find_package(X11)
    (home-page "")
    (synopsis1 "Very powerful Quake style Konsole ")
    (synopsis2 "Quake-style terminal emulator based on KDE Konsole technology")
    (description1 "Yakuake is a Quake-style terminal emulator based on KDE Konsole technology.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public libkdepim
  (package
    (name "libkdepim")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkdepim-" version ".tar.xz"))
      (sha256
       (base32 "07afpxhvxpf50h10vg6vla543n4rvy1grldjj4aygwh1fgl5snm0"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
;; /gnu/store/x0n29h0grd9l8dsp1bgn036d26jmg81a-libkdepim-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdesignerplugin" ,kdesignerplugin)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("kjobwidgets" ,kjobwidgets)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Akonadi)
;; find_package(KF5 AkonadiContact ${AKONADICONTACT_LIB_VERSION})
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;; find_package(KF5 Ldap ${KLDAP_LIB_VERSION})
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiSearch ${AKONADISEARCH_LIB_VERSION})
    (home-page "")
    (synopsis1 "KDEPIM 4 library ")
    (synopsis2 "KDE PIM library")
    (description1 "KDE PIM runtime libraries.")
    (description2 "")
    (license ) ;; TODO: check this
))
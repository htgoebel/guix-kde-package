(define-public okular
  (package
    (name "okular")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/okular-" version ".tar.xz"))
      (sha256
       (base32 "1gilr9nviv51pcnmqdfw7834knvyfd11ggdjvinxvbpz61832niv"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/jmc710x04snfsk2x6ipn5dy71r1rl53n-okular-16.12.3.tar.xz
    (inputs
     `(("kactivities" ,kactivities)
      ("karchive" ,karchive)
      ("kbookmarks" ,kbookmarks)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("khtml" ,khtml)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kparts" ,kparts)
      ("kwallet" ,kwallet)
      ("kwindowsystem" ,kwindowsystem)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)
      ("threadweaver" ,threadweaver)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 TextToSpeech)
;; find_package(KF5 ${KF5_REQUIRED_VERSION} JS)
;; find_package(ZLIB REQUIRED)
;; set_target_properties(okularcore PROPERTIES VERSION 7.0.0 SOVERSION 7 OUTPUT_NAME Okular5Core EXPORT_NAME Core)
;; set_target_properties(okularpart PROPERTIES PREFIX "")
    (home-page "http://www.kde.org/")
    (synopsis1 "A universal document viewer ")
    (synopsis2 "universal document viewer")
    (description1 "Okular is a universal document viewer based on KPDF for KDE 4.

Okular combines the excellent functionalities of KPDF with the versatility
of supporting different kind of documents, like PDF, Postscript, DjVu, CHM,
and others.

The document format handlers page has a chart describing in more detail
the supported formats and the features supported in each of them.")
    (description2 "This package contains libraries used by the Okular document viewer.

This package is part of the KDE graphics module.")
    (license unknown) ;; TODO: check this
))
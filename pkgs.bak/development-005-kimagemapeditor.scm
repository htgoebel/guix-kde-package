(define-public kimagemapeditor
  (package
    (name "kimagemapeditor")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kimagemapeditor-" version ".tar.xz"))
      (sha256
       (base32 "0362zcj6by3kydr5v3sr7l6k9kkyfcy16879f93d1qqkjfi11cic"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Development")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/rg49mbb0ghswaaz1k8kyg62d09gbb0vp-kimagemapeditor-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; set_target_properties(kimagemapeditor_bin PROPERTIES OUTPUT_NAME kimagemapeditor)
    (home-page "")
    (synopsis1 "HTML imagemap editor for KDE ")
    (synopsis2 "HTML image map editor")
    (description1 "KImageMapEditor is an HTML imagemap editor for KDE. It allows you to create and
modify HTML imagemaps.")
    (description2 "")
    (license ) ;; TODO: check this
))
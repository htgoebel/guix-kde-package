(define-public skanlite
  (package
    (name "skanlite")
    (version "2.0.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/skanlite/2.0/skanlite-" version ".tar.xz"))
      (sha256
       (base32 "0dh2v8029gkhcf3pndcxz1zk2jgpihgd30lmplgirilxdq9l2i9v"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/c4nqqzsgfsm9k0w4kdy3hr6pbpqxc41k-skanlite-2.0.1.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; # yiels ecm_setup_version
;;  # yields ${XDG_APPS_INSTALL_DIR}
;; find_package(PNG REQUIRED)
;; find_package(KF5 # alias to find_package(KFGuiAddons)
;;         CoreAddons # KAboutData
;;         I18n
;;         XmlGui # KAboutApplicationDialog
;;         KIO # contains the KIOWidgets which we use in target_link_libraries
;;         DocTools # yields kdoctools_create_handbook
;;         Sane # will find KF5Sane
;;         TextWidgets
;; )
    (home-page "http://www.kde.org/applications/graphics/skanlite/")
    (synopsis1 "An image scanning application ")
    (synopsis2 "image scanner based on the KSane backend")
    (description1 "Skanlite is an image scanning application that does nothing more than
scan and save images. It is based on libksane, a KDE interface for SANE
library to control flat scanners.")
    (description2 "Skanlite is a small and simple scanner application for KDE 4 which allows easy
scanning of images with an attached scanner. Through the KSane backend, it can
access a wide variety of different scanner models.

Skanlite can be considered to be the replacement of Kooka.")
    (license unknown) ;; TODO: check this
))
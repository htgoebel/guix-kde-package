;; ?
(define-public kmailtransport
  (package
    (name "kmailtransport")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kmailtransport-" version ".tar.xz"))
      (sha256
       (base32 "1dkw7niymhnf0jncflvqyldw28c9j0q6j598flb5ksds0n30iasy"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/3yff2p75yn53vwb23zaprlpypn7kbg7a-kmailtransport-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfigwidgets" ,kconfigwidgets)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwallet" ,kwallet)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Mime)
;; find_package(KF5 Akonadi ${AKONADI_LIB_VERSION})
;; find_package(KF5 AkonadiMime ${AKONADIMIME_LIB_VERSION})
;; find_package(Sasl2)
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "mail transport service library")
    (description1 "This library provides an API and support code for managing mail transport.")
    (description2 "")
    (license ) ;; TODO: check this
))
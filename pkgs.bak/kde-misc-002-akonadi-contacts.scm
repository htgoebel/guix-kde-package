(define-public akonadi-contacts
  (package
    (name "akonadi-contacts")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/akonadi-contacts-" version ".tar.xz"))
      (sha256
       (base32 "1205g4z5rz02j8swrmhncm06d8m727z63d526djygz5svz15sd2l"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/3734v06cdf9smdjpf9zg1sij731gvwyj-akonadi-contacts-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("ktextwidgets" ,ktextwidgets)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(Qt5 WebEngine WebEngineWidgets)
;; find_package(Grantlee5 "5.1" CONFIG REQUIRED)
;; find_package(KF5 Contacts ${KCONTACTS_VERSION})
;; find_package(KF5 Mime ${KMIMELIB_VERSION})
;; find_package(KF5 AkonadiMime ${AKONADI_MIME_VERSION})
;; find_package(KF5 Akonadi)
;; find_package(KF5 Prison ${Prison_MIN_VERSION})
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "Akonadi contacts access library")
    (description1 "A client access library for using the Akonadi PIM data server.")
    (description2 "")
    (license ) ;; TODO: check this
))
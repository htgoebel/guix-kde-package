(define-public kgeography
  (package
    (name "kgeography")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kgeography-" version ".tar.xz"))
      (sha256
       (base32 "1rnk00nj29zimpw36vhm0yrwlmpmxwv9wzxnhr7n2jq5qhbqsp5g"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/4x7pmjb7vfy27qr4qzz998d9ar34s81r-kgeography-16.12.3.tar.xz
    (inputs
     `(("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kitemviews" ,kitemviews)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "http://edu.kde.org/")
    (synopsis1 "A geography learning program ")
    (synopsis2 "geography learning aid for KDE")
    (description1 "KGeography is a geography learning program.")
    (description2 "KGeography is an aid for learning about world geography.  You can use it to
explore a map, show information about regions and features, and play quiz
games to test your geography knowledge.

This package is part of the KDE education module.")
    (license fdl-1.2+) ;; TODO: check this
))
(define-public kdepim-apps-libs
  (package
    (name "kdepim-apps-libs")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdepim-apps-libs-" version ".tar.xz"))
      (sha256
       (base32 "1q4ksp377piqxbs843nxafzssz80ayjii90iz86r2z2rd3lyrjzw"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/kxp5fkhii194q7fv6qbhhsyilzqjv2za-kdepim-apps-libs-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kservice" ,kservice)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 Akonadi)
;; find_package(KF5 AkonadiContact ${AKONADICONTACT_LIB_VERSION})
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;; find_package(KF5 GrantleeTheme ${GRANTLEETHEME_LIB_VERSION})
;; find_package(KF5 Libkleo ${LIBKLEO_LIB_VERSION})
;; find_package(KF5 PimCommon ${PIMCOMMON_LIB_VERSION})
;; find_package(Grantlee5 "5.1" CONFIG REQUIRED)
;; find_package(KF5 Prison)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "KDE PIM mail related libraries, data files")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
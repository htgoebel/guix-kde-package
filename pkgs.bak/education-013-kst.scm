(define-public kst
  (package
    (name "kst")
    (version "2.0.8")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "https://sourceforge.net/projects/kst/files/Kst%20" version "/Kst-" version ".tar.gz"))
      (sha256
       (base32 "1ihqmwqw0h2p7hn5shd8iwb0gd4axybs60lgw22ijxqh6wzgvyyf"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/8c8hyg88gdjak0c1525x62a931cqrsvh-Kst-2.0.8.tar.gz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; kst_option_init()
;; kst_option(string ""  all version_string    "Version string")
;; kst_option(bool   OFF all release           "Build release version: optimize for speed, don't embedded debug symbols")
;; kst_option(bool   OFF gcc deploy            "Deploy into install dir")
;; kst_option(bool   OFF all merge_files       "Merge files to speedup build about factor 5")
;; kst_option(bool   OFF all merge_rebuild     "Rebuild generated files from merged files build")
;; kst_option(bool   OFF all verbose           "Make verbose CMake run and Makefiles")
;; kst_option(string ""  all install_prefix    "Install path for Kst, using a default if not set")
;; kst_option(string ""  all install_libdir    "Install directory name for libraries")
;; kst_option(bool   ON  all 3rdparty          "Build plugins depending on 3rd party libraries")
;; kst_option(bool   ON  all dataobjects       "Build dataobject plugins")
;; kst_option(bool   OFF all test              "Build unit tests")
;; kst_option(bool   ON  all pch               "Use precompiled headers")
;; kst_option(bool   ON  all svnversion        "Use svnversion's output for Kst's version information")
;; kst_option(bool   OFF gcc rpath             "Use rpath")
;; kst_option(bool   OFF all 3rdparty_build    "Download and build 3rd party libraries")
;; kst_option(bool   OFF win 3rdparty_download "Download precompiled 3rd party libraries")
;; kst_option(bool   OFF win console           "Open console on Windows")
;; kst_option(bool   OFF win edit_cont         "Enable \"Edit and Continue\" for Visual Studio")
;; kst_option(bool   OFF all python            "Install Python support")
;; kst_option(bool   OFF all python_build      "Download and build NumPy/SciPy then install Python support")
;; kst_option(string ""  all python_prefix     "Path for installing python bindings")
;; kst_option(string OFF all cross             "Cross-compile on Linux for Windows")
;; kst_option(string ""  all qt4               "Use cross compiled Qt4 at given path")
;; kst_option(string ""  all qt5               "Use Qt5")
;; kst_option(bool   ON  all qt5base_only      "Only use Qt5 features available in qtbase")
;; kst_option(bool   OFF all clang             "Use Clang compiler")
;; kst_option(bool   OFF all sanitize          "Use Clang's sanitizers")
;; kst_option(bool   OFF all dbgsym            "Build with debug symbols enabled")
;; kst_option(bool   OFF win noinstaller       "Don't build installer")
;; elseif(kst_3rdparty_download)
;;         elseif(GCC_VERSION VERSION_EQUAL 4.7 OR GCC_VERSION VERSION_GREATER 4.7)
;; kst_revision_project_name(Revision)
;; elseif(kst_svnversion)
;;     KstRevisionHeader(${kst_dir} ${kst_revision_project} ${CMAKE_BINARY_DIR}/kstrevision.h _modified)
;;   find_package(Getdata)
;;   find_package(Gsl)
;;   find_package(Netcdf)
;;   find_package(Matio)
;;   find_package(CFITSIO)
;;         find_package(${qt5lib} REQUIRED)
;;     use_qt5lib(Qt5Core)
;;     use_qt5lib(Qt5Concurrent)
;;     use_qt5lib(Qt5Widgets)
;;     use_qt5lib(Qt5Network)
;;     use_qt5lib(Qt5Xml)
;;     use_qt5lib(Qt5PrintSupport)
;;  # Qt5 bug?
;;     find_package(Qt4 REQUIRED)
;;             foreach(_it ${ARGN})
;;             endforeach()
;;         fix_it(QTCORE QTGUI QTXML QTSVG QTNETWORK)
;; find_package(Qt5 LinguistTools)
;;         find_package(Qt5 LinguistForKst)
;;     find_package(LinguistForKst)
;;  # Windows XP
;;         large_address(CMAKE_EXE_LINKER_FLAGS    ${flag})
;;         large_address(CMAKE_SHARED_LINKER_FLAGS ${flag})
;;         large_address(CMAKE_MODULE_LINKER_FLAGS ${flag})
;;         link_large_address(/LARGEADDRESSAWARE)
;;     elseif(NOT x64)
;;         link_large_address(-Wl,--large-address-aware)
;; kst_option_list_all()
;;         qt5_wrap_po(kst_QM ${kst_PO})
;;         qt4_wrap_po(kst_QM ${kst_PO})
;;     foreach(po ${kst_PO})
;; /kst_common_(.*).po" "\\2" lang ${po})
;;     endforeach()
;;  not found, disabling localization support")
;;  # http://www.mail-archive.com/cmake@cmake.org/msg33720.html
;; ")
;;         fixup_bundle(\"${app}\" \"\${libs}\" \"${dir}\") "
;;         COMPONENT RUNTIME)
    (home-page "http://kst.kde.org")
    (synopsis1 "Program for looking at data streams ")
    (synopsis2 "scientific data plotting tool")
    (description1 "kst is a program for looking at data streams. It can plot:
 - x-y plots
 - power spectra
 - histograms
 - equations (including equations of data streams).
 - data in files which are being updated as data is being logged, in
    which case in can act as a plotter for a chart recorder.
 - much more
Kst supports realtime viewing and manipulation of streaming data.")
    (description2 "Kst is a fast real-time large-dataset viewing and plotting tool.
It has basic data analysis functionality, contains many powerful
built-in features and is expandable with plugins and extensions.")
    (license public-domain) ;; TODO: check this
))
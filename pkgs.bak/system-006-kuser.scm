(define-public kuser
  (package
    (name "kuser")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kuser-" version ".tar.xz"))
      (sha256
       (base32 "0yhwlcq24hyn00mfjc8qqr05gy3p2h8j1ls24jdka7h5l6kwvcpq"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "System")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/lpm98ri9vpdqsz43mdzb0lxwnws4zic4-kuser-16.08.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; find_package(KdepimLibs REQUIRED)
;; test_big_endian(WORDS_BIGENDIAN)
    (home-page "http://www.kde.org/")
    (synopsis1 "Users and groups manager ")
    (synopsis2 "user and group administration tool")
    (description1 "Kuser is a tool to create, remove and modify user accounts and groups.")
    (description2 "KUser is an application for managing users and groups on your system.

This package is part of the KDE administration module.")
    (license fdl-1.2+) ;; TODO: check this
))
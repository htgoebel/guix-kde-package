(define-public kleopatra
  (package
    (name "kleopatra")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kleopatra-" version ".tar.xz"))
      (sha256
       (base32 "1ja26gzdj8h5f8w1061scl40p6ahba3ci4hp91n2vp3rrz9m96wa"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/x7rpmxq2k2wj7y60km3sdzva339v14f6-kleopatra-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kitemmodels" ,kitemmodels)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package( REQUIRED)
;; find_package(Gpgmepp "1.7.1" CONFIG REQUIRED)
;; find_package(KF5 Libkleo ${LIBKLEO_VERSION})
;; find_package(KF5 Mime ${KMIME_VERSION})
;; find_package(Assuan2 REQUIRED)
;; find_package(Boost 1.34.0 REQUIRED)
;; find_path(Boost_TOPOLOGICAL_SORT_DIR NAMES boost/graph/topological_sort.hpp PATHS ${Boost_INCLUDE_DIRS})
;; ")
    (home-page "")
    (synopsis1 "KDE Certificate Manager ")
    (synopsis2 "Certificate Manager and Unified Crypto GUI")
    (description1 "Kleopatra is a certificate manager and a universal crypto GUI. It supports
managing X.509 and OpenPGP certificates in the GpgSM keybox and retrieving
certificates from LDAP servers.")
    (description2 "")
    (license ) ;; TODO: check this
))
;; ?
(define-public kdepim-runtime
  (package
    (name "kdepim-runtime")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdepim-runtime-" version ".tar.xz"))
      (sha256
       (base32 "0j5c3y8bqnffcrx4g7ilq7id46h11d1hiw81l7x4mg1p0zw07bg1"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/xhsd6zx2lkr1xk2dr4gn9dria6lzdznk-kdepim-runtime-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kdelibs4support" ,kdelibs4support)
      ("kio" ,kio)
      ("kitemmodels" ,kitemmodels)
      ("knotifyconfig" ,knotifyconfig)
      ("kross" ,kross)
      ("ktextwidgets" ,ktextwidgets)
      ("kwindowsystem" ,kwindowsystem)
      ("qtbase" ,qtbase)
      ("qtxmlpatterns" ,qtxmlpatterns)))
;; left-overs from CMakeLists.txt:
;; find_package( SharedMimeInfo REQUIRED )
;; find_package(Qt5 WebEngineWidgets)
;; find_package(Qt5 TextToSpeech)
;; find_packagE() # for KPluralHandlingSpinBox
;; find_package(KF5 Akonadi)
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiMime ${AKONADIMIME_LIB_VERSION})
;; find_package(KF5 MailTransport ${KMAILTRANSPORT_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
;; find_package(KF5 AkonadiContact ${AKONADICONTACT_LIB_VERSION})
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;; find_package(KF5 AlarmCalendar ${AKONADIKALARM_LIB_VERSION})
;; find_package(KF5 CalendarCore ${KCALENDARCORE_LIB_VERSION})
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 Mbox ${KMBOX_LIB_VERSION})
;; find_package(KF5 PimTextEdit ${KPIMTEXTEDIT_LIB_VERSION})
;; find_package(KF5 IMAP ${KIMAP_LIB_VERSION})
;; find_package(KF5 AkonadiNotes ${AKONADINOTE_LIB_VERSION})
;; find_package(Sasl2)
;; update_xdg_mimetypes(${KDE_INSTALL_MIMEDIR})
    (home-page "")
    (synopsis1 "Core files for kdepim ")
    (synopsis2 "")
    (description1 "KDE PIM runtime libraries.
---
Core files of kdepim.")
    (description2 "")
    (license ) ;; TODO: check this
))
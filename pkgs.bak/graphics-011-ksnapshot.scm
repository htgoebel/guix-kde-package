;; no newer version?
(define-public ksnapshot
  (package
    (name "ksnapshot")
    (version "4.14.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/" version "/src/ksnapshot-" version ".tar.xz"))
      (sha256
       (base32 "10grzlp7sq367g91858d16sadzipzmgwczhnb5xvy0437lqhhz7c"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/rajfyky6vb89f2n7i9gjw1w7dl9925hj-ksnapshot-4.14.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; macro_optional_find_package(Kipi)
;; " "Support for capturing the cursor" "http://www.x.org/" FALSE "" "")
;; qt4_add_dbus_adaptor(ksnapshot_SRCS org.kde.ksnapshot.xml ksnapshot.h KSnapshot)
    (home-page "http://www.kde.org/")
    (synopsis1 "KDE Screenshot Utility ")
    (synopsis2 "transitional package for kde-spectacle")
    (description1 "KSnapshot is a KDE snapshot tool with many features.
Features:
    - Save in multiple formats
    - Take new snapshot
    - Open with... possibility to open snapshot in external editor.
    - Copy to clipboard
    - Several capture modes, including selected region or single window
    - Snapshot delay")
    (description2 "KSnapshot captures images of the screen.  It can capture the whole screen,
a specified region, an individual window, or only part of a window.

This package is part of the KDE graphics module.")
    (license gpl2+) ;; TODO: check this
))
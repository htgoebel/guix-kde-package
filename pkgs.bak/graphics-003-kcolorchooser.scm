(define-public kcolorchooser
  (package
    (name "kcolorchooser")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kcolorchooser-" version ".tar.xz"))
      (sha256
       (base32 "13nkvxl3z2l3m6zs057ipxgqfgsw7gma1rs66maqhzl30k3hrcyb"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/rr6rkk1cqyjb2pfn92p1v90v0kpnvx9v-kcolorchooser-16.12.3.tar.xz
    (inputs
     `(("ki18n" ,ki18n)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "https://projects.kde.org/projects/kde/kdegraphics/kcolorchooser")
    (synopsis1 "KDE Color Chooser ")
    (synopsis2 "color chooser and palette editor")
    (description1 "KColorChooser is a simple application to select the color from the screen or
from a pallete.
Features :
   - Select colors from any location on the screen.
   - Select colors from a range of standard palletes available.
   - Color values shown in Hue-Saturation-Value (HSV), Red-Green-Blue (RGB) and
     HTML formats.")
    (description2 "KColorChooser is a color palette tool, used to mix colors and create custom
color palettes.  Using the dropper, it can obtain the color of any pixel on
the screen.

A number of common color palettes are included, such as the standard Web colors
and the Oxygen color scheme.

This package is part of the KDE graphics module.")
    (license gpl2+) ;; TODO: check this
))
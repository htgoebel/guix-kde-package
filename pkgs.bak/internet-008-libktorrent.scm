(define-public libktorrent
  (package
    (name "libktorrent")
    (version "2.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/ktorrent/5.0/libktorrent-" version ".tar.xz"))
      (sha256
       (base32 "171bx41c3iyic71fjw03hissij4kz0pyv1mn0mbch00djanir2s7"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Internet")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/k9rk74q1j2ih9kwcfqrw17kgdxzl13vi-libktorrent-2.0.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("solid" ,solid)))
;; left-overs from CMakeLists.txt:
;; #XFS
;; find_package(KF5 "${KF5_MIN_VERSION}")
;; find_package(Boost "${Boost_MIN_VERSION}" REQUIRED)
;; find_package(LibGMP "${LibGMP_MIN_VERSION}" REQUIRED)
;; find_package(LibGcrypt "${LibGcrypt_MIN_VERSION}" REQUIRED)
;; find_package(Qca-qt5 CONFIG REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
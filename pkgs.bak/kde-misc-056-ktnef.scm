;; Sources way, this si part of kdepim, so why there is a separate package?
(define-public ktnef
  (package
    (name "ktnef")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/ktnef-" version ".tar.xz"))
      (sha256
       (base32 "0v38h7bwjwab1656w3c2h1by5925f616idnvflgp123v1cs6jy1n"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/c7p9h1ish8328bqwsgnrwrgkh845wk46-ktnef-16.12.3.tar.xz
    (inputs
     `(("kdelibs4support" ,kdelibs4support)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
    (home-page "")
    (synopsis1 "TNEF Viewer/Extractor ")
    (synopsis2 "Viewer for mail attachments using TNEF format")
    (description1 "A TNEF Viewer/Extractor.")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public incidenceeditor
  (package
    (name "incidenceeditor")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/incidenceeditor-" version ".tar.xz"))
      (sha256
       (base32 "11jplw3fngnyvpjkhqwv1gzzwxxcm4v93h09f68hs015apncjvpp"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/qa39g7bs7kmmczllai20a33awls6p4gz-incidenceeditor-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KGantt ${KDIAGRAM_LIB_VERSION} CONFIG REQUIRED)
;; find_package(KF5 Akonadi)
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiMime ${AKONADI_MIMELIB_VERSION})
;; find_package(KF5 Ldap ${KLDAP_LIB_VERSION})
;; find_package(KF5 CalendarSupport ${CALENDARSUPPORT_LIB_VERSION})
;; find_package(KF5 EventViews ${EVENTVIEW_LIB_VERSION})
;; find_package(KF5 Libkdepim ${LIBKDEPIM_LIB_VERSION})
;; find_package(KF5 KdepimDBusInterfaces ${KDEPIM_LIB_VERSION})
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 CalendarCore ${KCALENDARCORE_LIB_VERSION})
;; find_package(KF5 MailTransport ${KMAILTRANSPORT_LIB_VERSION})
    (home-page "")
    (synopsis1 "kincidenceeditor ")
    (synopsis2 "KDE PIM incidence editor")
    (description1 "New incidence editors")
    (description2 "")
    (license ) ;; TODO: check this
))
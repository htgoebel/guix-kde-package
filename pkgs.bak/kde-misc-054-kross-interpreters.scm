(define-public kross-interpreters
  (package
    (name "kross-interpreters")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kross-interpreters-" version ".tar.xz"))
      (sha256
       (base32 "0z1n42imbhsx0qmx479sklnihwvbd5l5pigbpbpm80rgwda2ib57"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/iza1fydxh46nigsim7ckrnr8ymw0gyqs-kross-interpreters-16.12.3.tar.xz
    (inputs
     `(("kross" ,kross)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 5.11.0)
;; unset(PYTHON_INCLUDE_DIR CACHE)
;; unset(PYTHON_LIBRARY CACHE)
;; unset(PYTHON_LIBRARY_DEBUG CACHE)
;; unset(PYTHON_VERSION_MAJOR)
;; unset(PYTHON_VERSION_MINOR)
;; find_package(PythonLibs 2 EXACT REQUIRED)
;; ")
    (home-page "")
    (synopsis1 "Java kross interpreter ")
    (synopsis2 "")
    (description1 "Java kross interpreter
---
Python kross interpreter
---
Ruby kross interpreter")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public kmix
  (package
    (name "kmix")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kmix-" version ".tar.xz"))
      (sha256
       (base32 "1mq4kna3z62269m43qy42knq4byrvirk0mk5yp56n51im1bmdyj4"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Multimedia")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/0lacvmpi9arzlnq71vnp1s74nara46zm-kmix-16.12.3.tar.xz
    (inputs
     `(("kconfigwidgets" ,kconfigwidgets)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("kglobalaccel" ,kglobalaccel)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kxmlgui" ,kxmlgui)
      ("plasma-framework" ,plasma-framework)))
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; 	find_package(PulseAudio "${PA_VER}")
;; 	macro_optional_find_package(PulseAudio "${PA_VER}")
;; find_package(GLIB2)
;; 	find_package(Canberra)
;; 	macro_optional_find_package(Canberra)
;; find_package(Alsa)
;; 	alsa_configure_file(${CMAKE_BINARY_DIR}/config-alsa.h)
;;  { std::shared_ptr<int> p; return 0; }
;; " HAVE_STD_SHARED_PTR)
;;  { std::tr1::shared_ptr<int> p; return 0; }
;; " HAVE_STD_TR1_SHARED_PTR)
;;
;;
;;
;;
;; qt4_add_dbus_adaptor( kmix_adaptor_SRCS dbus/org.kde.kmix.control.xml
;; 	dbus/dbuscontrolwrapper.h DBusControlWrapper )
;; qt4_add_dbus_adaptor( kmix_adaptor_SRCS dbus/org.kde.kmix.mixer.xml
;; 	dbus/dbusmixerwrapper.h DBusMixerWrapper )
;; qt4_add_dbus_adaptor( kmix_adaptor_SRCS dbus/org.kde.kmix.mixset.xml
;; 	dbus/dbusmixsetwrapper.h DBusMixSetWrapper )
;;   kde4_add_kdeinit_executable( kmix ${kmix_KDEINIT_SRCS})
;;   KF5_ADD_KDEINIT_EXECUTABLE( kmixctrl ${kmixctrl_KDEINIT_SRCS})
;;   kde4_add_kdeinit_executable( kmixctrl ${kmixctrl_KDEINIT_SRCS})
    (home-page "http://www.kde.org/")
    (synopsis1 "KDE Digital Mixer ")
    (synopsis2 "volume control and mixer")
    (description1 "KMix is an application to allow you to change the volume of your sound
card. Though small, it is full-featured, and it supports several
platforms and sound drivers.")
    (description2 "KMix is an audio device mixer, used to adjust volume, select recording inputs,
and set other hardware options.

This package is part of the KDE multimedia module.")
    (license license:lgpl2.0+) ;; TODO: check this
))
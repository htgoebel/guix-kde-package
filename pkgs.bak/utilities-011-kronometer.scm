(define-public kronometer
  (package
    (name "kronometer")
    (version "2.1.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kronometer/" version "/src/kronometer-" version ".tar.xz"))
      (sha256
       (base32 "02rjlncvxva34f4m3m8nklnx6xx3j7yx5cm17jq9c3kxvy8w0n83"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/hmgg6higxck25s40m7hcsdkqwxmmd8j1-kronometer-2.1.2.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; kdoctools_install(po)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "simple stopwatch application")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public blinken
  (package
    (name "blinken")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/blinken-" version ".tar.xz"))
      (sha256
       (base32 "1z50ack1iqh194vn487nhdkrbn1camswy4a3g2ayxv3qfgmxdcga"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Education")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/fazayr7nqnzcq9nysdlxbv0qzga4ics1-blinken-16.12.3.tar.xz
    (inputs
     `(("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kxmlgui" ,kxmlgui)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "http://edu.kde.org/applications/all/blinken")
    (synopsis1 "Simon Says Game ")
    (synopsis2 "KDE version of the Simon electronic memory game")
    (description1 "Blinken is the KDE version of the well-known game Simon Says.
Follow the pattern of sounds and lights as long as you can! Press the
start game button to begin. Watch the computer and copy the pattern it
makes. Complete the sequence in the right order to win.")
    (description2 "Blinken is based on an electronic game released in 1978, which
challenges players to remember sequences of increasing length.  On
the face of the device, there are 4 different color buttons, each
with its own distinctive sound.  These buttons light up randomly,
creating the sequence that the player must then recall.  If the
player is successful in remembering the sequence of lights in the
correct order, they advance to the next stage, where an identical
sequence with one extra step is presented.

This package is part of the KDE education module.")
    (license gpl2+) ;; TODO: check this
))

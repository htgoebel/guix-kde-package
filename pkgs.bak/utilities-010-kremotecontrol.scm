(define-public kremotecontrol
  (package
    (name "kremotecontrol")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kremotecontrol-" version ".tar.xz"))
      (sha256
       (base32 "0xcs8gvb7ack0xqdp99x04lyv6hbqgxa5nq44pxl7czzc0la5nbk"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/zlcq24n9d76hvwgf01g5jyk7dqdc9swf-kremotecontrol-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
    (home-page "http://www.kde.org")
    (synopsis1 "KDE Frontend for the LIRC Suite ")
    (synopsis2 "frontend for using remote controls")
    (description1 "KRemoteControl is a KDE frontend for the Linux Infrared Remote Control system
(LIRC).
It consist of two parts: a systemtray applet and a configuration module.")
    (description2 "KRemoteControl is a KDE frontend for your remote controls. It allows you
to configure actions for button presses on remotes. All types of remotes
supported by Solid are also supported by KRemoteControl.

KRemoteControl consists of the following parts:
 * a control module that allows one to create and assign actions.
 * a kded module responsible for executing the actions.
 * a system tray applet for visual feedback.
 * a Plasma data engine for interacting with remotes from Plasma widgets.

This package is part of the KDE utilities module.")
    (license gpl2+) ;; TODO: check this
))
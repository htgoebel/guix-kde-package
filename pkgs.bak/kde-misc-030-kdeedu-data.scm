;; ?
(define-public kdeedu-data
  (package
    (name "kdeedu-data")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdeedu-data-" version ".tar.xz"))
      (sha256
       (base32 "0vqzamp5fc2d0i9qn6986f3a1s1fdbrlzrsnimpdfcas5xngbv5m"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/0cjrcnjmndh4iq7xjzihl8a7kbg1w43v-kdeedu-data-16.12.3.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "Common files needed by kdeedu related applications ")
    (synopsis2 "transitional package for kdeedu-data")
    (description1 "This packages provides icons and contains KEduVocDocument and its related class
for reading from/writing to the KVTML format (and others too).
It's used by kdeedu related applications ( kanagram, khangman,kwordquiz)
---
This packages provides icons and contains KEduVocDocument and its related class
for reading from/writing to the KVTML format (and others too).
It's used by kdeedu related applications ( kanagram, khangman,kwordquiz)")
    (description2 "")
    (license ) ;; TODO: check this
))
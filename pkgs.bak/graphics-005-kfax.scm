;; Dated 2010
(define-public kfax
  (package
    (name "kfax")
    (version "3.3.6")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/extragear/kfax-" version "-kde4.4.0.tar.bz2"))
      (sha256
       (base32 "07bddjhvhwyzgpyj6svlx1v6sg02sp73zw4vz709p754f9gv55v8"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/455n99sy96h767aayvyryvfb6g4ljjd0-kfax-3.3.6-kde4.4.0.tar.bz2
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package (KDE4 REQUIRED)
;;
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
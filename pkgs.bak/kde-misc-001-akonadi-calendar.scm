(define-public akonadi-calendar
  (package
    (name "akonadi-calendar")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/akonadi-calendar-" version ".tar.xz"))
      (sha256
       (base32 "0kv636a8x75fcagw8hjnrwnxzvqnnm42hfw68ywfics0pn0pl50g"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/32dqfwsisq3wm5gng1g4zaygk6va873h-akonadi-calendar-16.12.3.tar.xz
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kio" ,kio)
      ("kwallet" ,kwallet)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 MailTransport ${MAILTRANSPORT_LIB_VERSION})
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 Akonadi ${AKONADI_LIB_VERSION})
;; find_package(KF5 AkonadiContact ${AKONADICONTACT_LIB_VERSION})
    (home-page "")
    (synopsis1 "KDE 4 core library ")
    (synopsis2 "library providing calendar helpers for Akonadi items")
    (description1 "This library manages calendar specific actions for collection and item views.")
    (description2 "")
    (license ) ;; TODO: check this
))
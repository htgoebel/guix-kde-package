(define-public kapptemplate
  (package
    (name "kapptemplate")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kapptemplate-" version ".tar.xz"))
      (sha256
       (base32 "036npdxyh9rx0aaiwvdjqrb39f8bqglqbl3iddb1vh7w9p1h158n"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Development")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/ysqcbjj7xr8klpi0a40vlxpv9anj3yk7-kapptemplate-16.12.3.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "http://www.kde.org/")
    (synopsis1 "Template for KDE Application Development ")
    (synopsis2 "application template generator")
    (description1 "KAppTemplate is a set of modular shell scripts that will create a
framework for any number of KDE application types. At its base
level, it handles creation of things like the automake/autoconf
framework, lsm files, RPM spec files, and po files. Then, there
are individual modules that allow you to create a skeleton KDE
application, a KPart application, a KPart plugin, or even convert
existing source code to the KDE framework.")
    (description2 "KAppTemplate is a shell script that will create the necessary framework to
develop several types of applications, including applications based on the
KDE development platform.

It generates the build-system configuration and provides example code
for a simple application.

This package is part of the KDE Software Development Kit module.")
    (license fdl-1.2+) ;; TODO: check this
))
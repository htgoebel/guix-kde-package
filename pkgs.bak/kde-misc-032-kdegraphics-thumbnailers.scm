(define-public kdegraphics-thumbnailers
  (package
    (name "kdegraphics-thumbnailers")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kdegraphics-thumbnailers-" version ".tar.xz"))
      (sha256
       (base32 "01q2czzqq240cbp9yg7mji2q15kmiwn1aqs6iii00i56yy2mwaxq"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/777hj3lbr8imn6yk9aa0hlhxbzp2791x-kdegraphics-thumbnailers-16.12.3.tar.xz
    (inputs
     `(("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 KExiv2)
;; find_package(KF5 KDcraw)
    (home-page "")
    (synopsis1 "Postscript and PDF ThumbCreator ")
    (synopsis2 "")
    (description1 "PostScript and PDF files ThumbCreator.")
    (description2 "")
    (license ) ;; TODO: check this
))
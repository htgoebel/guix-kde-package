;; Is this obsolete? Is this part of kactivities or plasma-workspaces?
(define-public kactivities-workspace
  (package
    (name "kactivities-workspace")
    (version "5.5.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kactivities/kactivities-workspace-" version ".tar.xz"))
      (sha256
       (base32 "031wvkn9hm41yg1a18kyb5501h8czjs7kp2kfib5mjn6zal497rp"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unlisted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/3fdacxixsb6nrm67f8gwz8jimsvb1iyb-kactivities-workspace-5.5.0.tar.xz
    (inputs
     `(("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; # handled by release scripts
;;  # handled by release scripts
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
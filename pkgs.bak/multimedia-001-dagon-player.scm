(define-public dagon-player
  (package
    (name "dagon-player")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append ""))
      (sha256
       (base32 "0s94vnr9hz5gzgs555phwl9y9m9jw0ysimv56mh8n56f6ar9za5m"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Multimedia")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/nwzfszppy0nvymk47zzwips0sjdlqm2p-dragon-16.08.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kjobwidgets" ,kjobwidgets)
      ("kparts" ,kparts)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("solid" ,solid)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "http://www.kde.org/")
    (synopsis1 "A simple video player for KDE 4 ")
    (synopsis2 "simple video player")
    (description1 "Dragon Player is a multimedia player where the focus is on simplicity,
instead of features. Dragon Player does one thing, and only one thing,
which is playing multimedia files. It's simple interface is designed not
to get in your way and instead empower you to simply play multimedia
files.")
    (description2 "A video player with a different philosophy: Simple, uncluttered interface.

Features:
 - Plays DVDs, VCDs, all video formats supported by the Phonon backend in use.
 - Bundled with a simple web-page KPart.
 - Starts quickly.

This is the KDE 4 version of the Codeine video player.

This package is part of the KDE multimedia module.")
    (license license:gpl-2+3+kdeev) ;; TODO: check this
))
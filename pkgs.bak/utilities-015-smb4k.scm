(define-public smb4k
  (package
    (name "smb4k")
    (version "1.2.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "https://sourceforge.net/projects/smb4k/files/" version "/smb4k-" version ".tar.xz/download"))
      (sha256
       (base32 "1xmm7izvb9xq1y1kx303w0sd3xvgclqm654z69iq43afpvll72rk"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/pvyczc8nlm2wr8b0ri0lb8bv03ap2yh6-download
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package( KDE4 REQUIRED )
;;   set_property(GLOBAL PROPERTY ALLOW_DUPLICATE_CUSTOM_TARGETS ON)
;; " )
;; elseif ( NOT INSTALL_PLASMOID )
;; " )
;; " )
;; " )
;; elseif ( NOT INSTALL_HEADER_FILES )
;; " )
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "http://smb4k.sourceforge.net/")
    (synopsis1 "A KDE SMB share browser ")
    (synopsis2 "Samba (SMB) share advanced browser")
    (description1 "An SMB network and share browser for KDE 4 or later.")
    (description2 "Smb4K is an advanced network neighborhood browser for the KDE Software
Compilation and a frontend to the programs of the Samba software suite.

Features:
 Scanning for (active) workgroups, hosts, and shares
 Support of the CIFS (Linux) and SMBFS (FreeBSD) file system
 Mounting and unmounting of shares (using the KAuth framework)
 Access to the files of a mounted share using a file manager or terminal
 Auto-detection of external mounts and unmounts
 Remounting of previously used shares on program start
 Miscellaneous infos about remote network items and mounted shares
 Network search
 WINS server support
 Preview of the contents of a share
 Several methods to look up the initial list of workgroups and domains
 Default login
 Special handling of homes shares
 Ability to bookmark favorite shares and organize them in groups
 System tray widget
 Support of advanced Samba options
 Support of printer shares
 KWallet support
 Synchronization of a remote share with a local copy and vice versa
 Ability to define custom options for individual servers and shares
 Laptop support through the Solid hardware device framework")
    (license unknown) ;; TODO: check this
))
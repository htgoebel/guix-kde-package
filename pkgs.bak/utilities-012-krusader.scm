(define-public krusader
  (package
    (name "krusader")
    (version "2.5.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/krusader/" version "/krusader-" version ".tar.xz"))
      (sha256
       (base32 "1mw8a05yd7bgdy9fgacvakz8sgagf6fjn860sn35vzbqfcilp7zv"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Utilities")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
;; /gnu/store/1hwyl69vbvmg7ibb9dhdd8qhmgb3dfqg-krusader-2.5.0.tar.xz
    (inputs
     `(("karchive" ,karchive)
      ("kbookmarks" ,kbookmarks)
      ("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("kparts" ,kparts)
      ("ktextwidgets" ,ktextwidgets)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("solid" ,solid)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "http://www.krusader.org")
    (synopsis1 "Advanced KDE twin-panel file-manager ")
    (synopsis2 "twin-panel (commander-style) file manager")
    (description1 "Krusader is an advanced twin panel (commander style) file manager
for KDE and other desktops in the *nix world, similar to Midnight or
Total Commander. It provides all the file management features you
could possibly want.

Plus: extensive archive handling, mounted filesystem support, FTP,
advanced search module, an internal viewer/editor, directory
synchronisation, file content comparisons, powerful batch renaming
and much much more. It supports a wide variety of archive formats
and can handle other KIO slaves such as smb or fish.")
    (description2 "Krusader is a simple, easy, powerful, twin-panel (commander-style) file
manager, similar to Midnight Commander (C) or Total Commander (C).

It provides all the file management features you could possibly want.

Plus: extensive archive handling, mounted filesystem support, FTP,
advanced search module, viewer/editor, directory synchronisation,
file content comparisons, powerful batch renaming and much more.

It supports archive formats: ace, arj, bzip2, deb, iso, lha, rar, rpm, tar,
zip and 7-zip.

It handles KIOSlaves such as smb:// or fish://.

Almost completely customizable, Krusader is very user friendly, fast and looks
great on your desktop.")
    (license unknown) ;; TODO: check this
))
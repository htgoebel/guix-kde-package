(define-public kontactinterface
  (package
    (name "kontactinterface")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kontactinterface-" version ".tar.xz"))
      (sha256
       (base32 "0n5hvx3xp01krwwd2szgh1s6nn5spfns1ivc36i1gdbhkf871k98"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/xc78x3rkqp3ivdaw0rj2ck0gm1knx1yr-kontactinterface-16.12.3.tar.xz
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kparts" ,kparts)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)))
;; left-overs from CMakeLists.txt:
;; find_package(X11)
    (home-page "")
    (synopsis1 "Kontact Interface library ")
    (synopsis2 "Kontact interface library")
    (description1 "Kontact Interface library.")
    (description2 "")
    (license ) ;; TODO: check this
))
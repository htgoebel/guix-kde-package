;; Dated 2010
(define-public kiconedit
  (package
    (name "kiconedit")
    (version "4.4.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/extragear/kiconedit-" version ".tar.bz2"))
      (sha256
       (base32 "03759r32zr8r7swmfxn9nxxr0zn9276fvkvck57x3gc97kx2xi1b"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/ajsas1lbqvx6h639raxmlpdd74w95n8x-kiconedit-4.4.0.tar.bz2
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package (KDE4 REQUIRED)
;;
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
(define-public kaccounts-integration
  (package
    (name "kaccounts-integration")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/kaccounts-integration-" version ".tar.xz"))
      (sha256
       (base32 "0w8h33sysf590xyg4bvf2a2z39kzc0wn6mxv31mrf68b6ks7b9h2"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/ijmsgk1j1xd7icbl03538gir4x1lq2x0-kaccounts-integration-16.12.3.tar.xz
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(AccountsQt5 ${ACCOUNTSQT_DEP_VERSION} CONFIG)
;; find_package(SignOnQt5 ${SIGNONQT_DEP_VERSION} CONFIG)
;; find_package(LibAccountsGlib ${ACCOUNTSGLIB_DEP_VERSION})
    (home-page "")
    (synopsis1 "")
    (synopsis2 "System to administer web accounts")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
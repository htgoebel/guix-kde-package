(define-public kgraphviewer
  (package
    (name "kgraphviewer")
    (version "2.2.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kgraphviewer/" version "/src/kgraphviewer-" version ".tar.xz"))
      (sha256
       (base32 "1vs5x539mx26xqdljwzkh2bj7s3ydw4cb1wm9nlhgs18siw4gjl5"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Graphics")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/hg8qnf90sdvs2p1yps896bc6vki1m9ja-kgraphviewer-2.2.0.tar.xz
    (inputs
     `())
;; left-overs from CMakeLists.txt:
;; find_package(KDE4 REQUIRED)
;; find_package(Boost 1.36 REQUIRED)
;; find_package(GraphViz REQUIRED)
    (home-page "")
    (synopsis1 "")
    (synopsis2 "")
    (description1 "")
    (description2 "")
    (license ) ;; TODO: check this
))
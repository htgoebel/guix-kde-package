(define-public baloo-widgets
  (package
    (name "baloo-widgets")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/baloo-widgets-" version ".tar.xz"))
      (sha256
       (base32 "0h75zhdiylyjifdk9ikw9gpwla3p87knndc2svkci90ga7ynggvl"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/lw6zv6prl31xbdclq67msn75rarkkg21-baloo-widgets-16.12.3.tar.xz
    (inputs
     `(("baloo" ,baloo)
      ("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; find_package(KF5 5.19 FileMetaData)
    (home-page "http://projects.kde.org/baloo-widgets")
    (synopsis1 "Widgets for Baloo ")
    (synopsis2 "Wigets for use with Baloo")
    (description1 "Widgets for Baloo")
    (description2 "Baloo is a framework for searching and managing metadata.

This package contains GUI widgets for baloo.

Baloo is part of KDE SC.")
    (license lgpl2.1+) ;; TODO: check this
))
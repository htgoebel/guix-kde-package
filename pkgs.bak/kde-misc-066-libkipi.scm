(define-public libkipi
  (package
    (name "libkipi")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version "/src/libkipi-" version ".tar.xz"))
      (sha256
       (base32 "0f1m0v0cm11dqwm3n9w1mwz25sj3bggx19fi0jj4ij7zqicpykz6"))))
        "0y7rl603vmwlxm6ilkhc51rx2mfj14ckcz40xxgs0ljnvlhp30yp"))
    (properties `((tags . ("Desktop" "KDE" "Unsorted")))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
;; /gnu/store/48ai7g6ls5l0020kcxjx1fkpb0r18syd-libkipi-16.12.3.tar.xz
    (inputs
     `(("kconfig" ,kconfig)
      ("kservice" ,kservice)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; left-overs from CMakeLists.txt:
;; none - hopefully everything has been detected
    (home-page "")
    (synopsis1 "Libkipi runtime library ")
    (synopsis2 "KDE Image Plugin Interface library")
    (description1 "Libkipi is an interface to use kipi-plugins from a KDE image management
program like digiKam (http://www.digikam.org).

This package provides the runtime library.")
    (description2 "")
    (license ) ;; TODO: check this
))
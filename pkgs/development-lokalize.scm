(define-public lokalize
  (package
    (name "lokalize")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/lokalize-" version ".tar.xz"))
      (sha256
       (base32 "17ikk89680jjzv95gnrzah4bi3xnyp5mi64prhblhw7kz6vlf8x0"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("kross" ,kross)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtscript" ,qtscript)
      ("sonnet" ,sonnet)))
;; find_package(HUNSPELL)
    (home-page "http://www.kde.org/")
    (synopsis "Computer-Aided Translation Tool")
    (description "Lokalize is a computer-aided translation system that focuses
on productivity and quality assurance.  It has components usual for CAT tools:
translation memory, glossary, and also a unique translation
merging (synchronization) capability.  It is primarily targeted at software
translation and also integrates external conversion tools for office document
translation.  Lokalize is a replacement of KBabel.

This package is part of the KDE Software Development Kit module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public libkdcraw
  (package
    (name "libkdcraw")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkdcraw-" version ".tar.xz"))
      (sha256
       (base32 "03ag6vzdj5n7zbb8yb9k84ckm1zwp2i9qqrsfn2mmmhypwknpg4w"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("qtbase" ,qtbase)))
;; find_package(LibRaw ${LIBRAW_MIN_VERSION} REQUIRED)
    (home-page "")
    (synopsis "
C++ interface around LibRaw library
RAW picture decoding library")
    (description "
Libkdcraw is a C++ interface around LibRaw library used to decode RAW
picture files. More information about LibRaw can be found at
http://www.libraw.org.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

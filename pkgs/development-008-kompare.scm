(define-public kompare
  (package
    (name "kompare")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kompare-" version ".tar.xz"))
      (sha256
       (base32 "18z6424xidpx5kmbjiasvhagh2b1a9pxizc0dsvba47v9xcavsaw"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kiconthemes" ,kiconthemes)
      ("kjobwidgets" ,kjobwidgets)
      ("kparts" ,kparts)
      ("ktexteditor" ,ktexteditor)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; find_package(LibKompareDiff2 REQUIRED)
    (home-page "http://www.kde.org/")
    (synopsis "
A diff graphic tool
file difference viewer")
    (description "
Kompare is a GUI front-end program that enables differences between source files
to be viewed and merged. It can be used to compare differences on files or the
contents of folders, and it supports a variety of diff formats and provide many
options to customize the information level displayed.

Features :
   -Comparing directories
   -Reading diff files
   -Creating and applying patches

Kompare displays the differences between files.  It can compare the
contents of files or directories, as well as create, display, and merge patch
files.

This package is part of the KDE Software Development Kit module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

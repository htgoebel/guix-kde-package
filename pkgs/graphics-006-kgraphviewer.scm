(define-public kgraphviewer
  (package
    (name "kgraphviewer")
    (version "2.2.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kgraphviewer/" version
                          "/src/kgraphviewer-" version ".tar.xz"))
      (sha256
       (base32 "1vs5x539mx26xqdljwzkh2bj7s3ydw4cb1wm9nlhgs18siw4gjl5"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; find_package(Boost 1.36 REQUIRED)
;; find_package(GraphViz REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

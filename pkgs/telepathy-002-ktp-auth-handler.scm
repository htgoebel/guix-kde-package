(define-public ktp-auth-handler
  (package
    (name "ktp-auth-handler")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-auth-handler-" version ".tar.xz"))
      (sha256
       (base32 "08ryqkba9zngjabsp1b9w13psp0n97qhjd31id007hc6r6s1nxxx"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwallet" ,kwallet)
      ("kwebkit" ,kwebkit)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; #Network for QSsl
;; find_package (TelepathyQt5 0.8.9 REQUIRED)
;; find_package (KTp REQUIRED)
;; find_package (Qca-qt5 REQUIRED)
;; find_package(AccountsQt5 1.10 REQUIRED CONFIG)
;; find_package(SignOnQt5 8.55 REQUIRED CONFIG)
;; find_package(KAccounts REQUIRED)
;; find_package(Qca-qt5-ossl QUIET)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

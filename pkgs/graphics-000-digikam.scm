(define-public digikam
  (package
    (name "digikam")
    (version "5.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/digikam/digikam-" version
                           ".tar.xz"))
       (sha256
        (base32 "0lk2rjndfbxlkwxzviq5m72rh56b5z07fzn9xdf27fdzildvz76z"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;;("perl" ,perl)
       ;;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `())
;;     find_package(Ruby)
;;     find_package(Subversion)
;;     find_package(Gettext REQUIRED)
    (home-page "http://www.digikam.org")
    (synopsis "Digital photo management application for KDE")
    (description "DigiKam is an advanced digital photo management application
for KDE.  Photos can be collected into albums which can be sorted
chronologically, by directory layout or by custom collections.

DigiKam also provides tagging functionality.  Images can be tagged despite of
their position and digiKam provides fast and intuitive ways to browse them.
User comments and customized meta-information added to images, are stored into
a database and retrieved to make them available into the user interface.  As
soon as the camera is plugged in digikam allows you to preview, download,
upload and delete images.

Digikam also includes tools like Image Editor, to modify photos using plugins
such as red eye correction or Gamma correction, exif management,...
Light Table to make artistic photos and an external image editor such
as Showfoto.

Digikam also uses KIPI plugins (KDE Image Plugin Interface) to increase
its functionalities.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

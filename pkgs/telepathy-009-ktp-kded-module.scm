(define-public ktp-kded-module
  (package
    (name "ktp-kded-module")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-kded-module-" version ".tar.xz"))
      (sha256
       (base32 "06zw8padspbn8ydcbs69v3ggmfpqrb3axxd2v1sgg4p4kdp0jyml"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kidletime" ,kidletime)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

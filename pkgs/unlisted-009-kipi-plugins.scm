;; Found on download-server, maybe already included in another package
(define-public kipi-plugins
  (package
    (name "kipi-plugins")
    (version "1.0.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kipi-plugins/kipi-plugins-1.9.0.tar.bz2"))
      (sha256
       (base32 "0k4k9v1rj7129n0s0i5pvv4rabx0prxqs6sca642fj95cxc6c96m"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; STRING(LENGTH ${${VAR}} NAME_LENGTH)
;;     FOREACH(COUNT RANGE ${DOT_LENGTH})
;;     ENDFOREACH(COUNT)
;; ENDMACRO(FILL_WITH_DOTS)
;;")
;;")
;; ENDMACRO(PRINT_PLUGIN_COMPILE_STATUS)
;;")
;;")
;; ENDMACRO(PRINT_OPTIONAL_LIBRARY_STATUS)
;; ENDMACRO(PRINT_LIBRARY_STATUS)
;;")
;;")
;; ENDMACRO(PRINT_OPTIONAL_QTMODULE_STATUS)
;; FIND_PACKAGE(KDE4 REQUIRED)
;; FIND_PACKAGE(Kexiv2 REQUIRED)
;; FIND_PACKAGE(Kdcraw REQUIRED)
;; FIND_PACKAGE(Kipi REQUIRED)
;; FIND_PACKAGE(JPEG REQUIRED)
;; FIND_PACKAGE(PNG REQUIRED)
;; FIND_PACKAGE(TIFF REQUIRED)
;; MACRO_OPTIONAL_FIND_PACKAGE(EXPAT)      # For DNGConverter: XMP SDK need Expat library to compile.
;; MACRO_OPTIONAL_FIND_PACKAGE(Threads)    # For DNGConverter: DNG SDK need native threads support.
;; MACRO_OPTIONAL_FIND_PACKAGE(LibXml2)    # For Htmlexport.
;; MACRO_OPTIONAL_FIND_PACKAGE(LibXslt)    # For Htmlexport.
;; MACRO_OPTIONAL_FIND_PACKAGE(OpenGL)     # For AdvancedSlideshow and ImageViewer.
;; MACRO_OPTIONAL_FIND_PACKAGE(Gpod)       # For ipodexport.
;; MACRO_OPTIONAL_FIND_PACKAGE(Gdk)        # For ipodexport.
;; MACRO_OPTIONAL_FIND_PACKAGE(GLIB2)      # For ipodexport.
;; MACRO_OPTIONAL_FIND_PACKAGE(GObject)    # For ipodexport.
;; MACRO_OPTIONAL_FIND_PACKAGE(KdepimLibs) # For Calendar (libkcal).
;; MACRO_OPTIONAL_FIND_PACKAGE(QCA2)       # For Shwup.
;; MACRO_OPTIONAL_FIND_PACKAGE(KSane)      # For AcquireImages.
;; MACRO_OPTIONAL_FIND_PACKAGE(OpenCV)     # For RemoveRedEyes.
;; MACRO_OPTIONAL_FIND_PACKAGE(QJSON)      # For Debian Screenshots.
;;     MACRO_OPTIONAL_FIND_PACKAGE(X11)   # For AdvancedSlideshow and ImageViewer.
;;     FIND_PACKAGE(PkgConfig)
;;     PKG_CHECK_MODULES(Kdcraw libkdcraw>=1.1.0)
;;     FIND_PACKAGE(PkgConfig)
;;     PKG_CHECK_MODULES(Kexiv2 libkexiv2>=1.1.0)
;; EXECUTE_PROCESS(COMMAND ${CMAKE_COMMAND} --version OUTPUT_VARIABLE VERSION_CMAKE_INFO)
;; STRING(REPLACE "-patch " "." VERSION_CMAKE_INFO "${VERSION_CMAKE_INFO}")
;; : ${VERSION_CMAKE_INFO}")
;;     MACRO_OPTIONAL_FIND_PACKAGE(OpenMP)
;; PRINT_LIBRARY_STATUS("libjpeg" "http://www.ijg.org" "" JPEG_FOUND)
;; PRINT_LIBRARY_STATUS("libtiff" "http://www.remotesensing.org/libtiff" "" TIFF_FOUND)
;; PRINT_LIBRARY_STATUS("libpng" "http://www.libpng.org/pub/png/libpng.html" " (version >= 1.2.7)" PNG_FOUND)
;; PRINT_LIBRARY_STATUS("libkipi" "http://www.digikam.org/sharedlibs" " (version >= 0.2.0)" KIPI_FOUND)
;; PRINT_LIBRARY_STATUS("libkexiv2" "http://www.digikam.org/sharedlibs" " (version >= 0.2.0)" KEXIV2_FOUND)
;; PRINT_LIBRARY_STATUS("libkdcraw" "http://www.digikam.org/sharedlibs" " (version >= 1.1.0)" KDCRAW_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libxml2" LIBXML2_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libxslt" LIBXSLT_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libexpat" EXPAT_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("native threads support" CMAKE_USE_PTHREADS_INIT OR CMAKE_USE_WIN32_THREADS_INIT)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libopengl" OPENGL_FOUND AND OPENGL_GLU_FOUND)
;; PRINT_OPTIONAL_QTMODULE_STATUS("Qt4 OpenGL" QT_QTOPENGL_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libopencv" OPENCV_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("QJson" QJSON_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libgpod" GPOD_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("Gdk" GDK_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libkdepim" KDEPIMLIBS_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("qca2" QCA2_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libkdcraw >= 1.1.0" KDCRAW_FOR_KIPIPLUGINS)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libkexiv2 >= 1.1.0" KEXIV2_FOR_KIPIPLUGINS)
;;     PRINT_OPTIONAL_LIBRARY_STATUS("OpenMP" OPENMP_FOUND)
;;     PRINT_OPTIONAL_LIBRARY_STATUS("libX11" X11_FOUND AND X11_Xrandr_FOUND)
;; PRINT_OPTIONAL_LIBRARY_STATUS("libksane" KSANE_FOUND)
;; PRINT_PLUGIN_COMPILE_STATUS("Shwup" QCA2_FOUND)
;; PRINT_PLUGIN_COMPILE_STATUS("HtmlExport" LIBXML2_FOUND AND LIBXSLT_FOUND)
;;     ELSEIF (OPENGL_FOUND AND OPENGL_GLU_FOUND AND QT_QTOPENGL_FOUND AND QT_QTOPENGL_FOUND)
;; PRINT_PLUGIN_COMPILE_STATUS("AdvancedSlideshow" BUILD_VIEWERS)
;; PRINT_PLUGIN_COMPILE_STATUS("ImageViewer" BUILD_VIEWERS)
;; PRINT_PLUGIN_COMPILE_STATUS("AcquireImages" KSANE_FOUND)
;; PRINT_PLUGIN_COMPILE_STATUS("DNGConverter" EXPAT_FOUND)
;; PRINT_PLUGIN_COMPILE_STATUS("RemoveRedEyes" OPENCV_FOUND)
;; PRINT_PLUGIN_COMPILE_STATUS("Debian Screenshots" QJSON_FOUND)
;; PRINT_PLUGIN_COMPILE_STATUS("IpodExport" GPOD_FOUND AND GLIB2_FOUND AND GOBJECT_FOUND AND GDK_FOUND)
;; PRINT_PLUGIN_COMPILE_STATUS("Calendar" KDEPIMLIBS_FOUND)
;;   # Dummy file
;;     INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/common/libkipiplugins
;;                         ${CMAKE_CURRENT_BINARY_DIR}/common/libkipiplugins
;;                         ${CMAKE_CURRENT_SOURCE_DIR}/common/libkipiplugins/dialogs
;;                         ${CMAKE_CURRENT_BINARY_DIR}/common/libkipiplugins/dialogs
;;                         ${CMAKE_CURRENT_SOURCE_DIR}/common/libkipiplugins/widgets
;;                         ${CMAKE_CURRENT_BINARY_DIR}/common/libkipiplugins/widgets
;;                         ${CMAKE_CURRENT_SOURCE_DIR}/common/libkipiplugins/tools
;;                         ${CMAKE_CURRENT_BINARY_DIR}/common/libkipiplugins/tools
;;                         ${KEXIV2_INCLUDE_DIR}
;;                         ${KDCRAW_INCLUDE_DIR}
;;                         ${KIPI_INCLUDE_DIR}
;;                         ${KDE4_INCLUDES}
;;                         ${QT4_INCLUDES}
;;                        )
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis "
KDE image Interface Plugins
image manipulation/handling plugins for KIPI aware programs")
    (description "
The library of the KDE Image Plugin Interface.

Libkipi allows image applications to use a plugin architecture
for additional functionality such as: RawConverter, SlideShow,
ImagesGallery, HTMLExport, PrintAssistant...
---
The library of the KDE Image Plugin Interface.

Libkipi allows image applications to use a plugin architecture
for additional functionality such as: RawConverter, SlideShow,
ImagesGallery, HTMLExport, PrintAssistant...

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

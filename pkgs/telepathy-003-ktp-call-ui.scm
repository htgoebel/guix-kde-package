(define-public ktp-call-ui
  (package
    (name "ktp-call-ui")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-call-ui-" version ".tar.xz"))
      (sha256
       (base32 "1aq0s4v972kskjnq720364y971iyr0m6pj42fw5rpkl7j8vfg1rd"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("knotifications" ,knotifications)
      ("kxmlgui" ,kxmlgui)
      ("phonon" ,phonon)
      ("qtdeclarative" ,qtdeclarative)))
;; find_package(KTp REQUIRED)
;; find_package(TelepathyQt5 REQUIRED)
;; find_package(TelepathyQt5Farstream REQUIRED)
;; find_package(Qt5 GStreamer)
;; find_package(PkgConfig REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kbibtex
  (package
    (name "kbibtex")
    (version "0.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/KBibTeX/" version
                          "/"
                           name "-" version ".tar.xz"))
       (sha256
        (base32 "127c236s264ckaqjibr2fhg6ynnd7qmkpvi1q000nrxqwg63zqzn"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
    (home-page "https://kde.org/applications/office/org.kde.kbibtex")
    (synopsis "BibTeX editor and reference manager")
    (description "KBibTeX is a reference management application which can be
used to collect TeX/LaTeX bibliographies and export them in various formats.

KBibTeX can do the following things:

@end itemize

@item Preview bibliography entries in various formats (Source (BibTeX),
Source (RIS), Wikipedia, standard (XML/XSLT), fancy (XML/XSLT), and
abstract-only (XML/XSLT)). Additional preview styles become available when
bibtex2html is installed.

@item Import data in various bibliography file formats such as BibTeX, RIS and
ISI (requires bibutils) and export data to PDF (requires pdflatex),
PostScript (requires latex), RTF (requires latex2rtf), and HTML.

@item Search for the bibliography entries data in online
databases (e.g. Google Scholar, ACM, IEEE, arXiv, etc.)

@item Preview local or remote (online) resources, e.g. PDF files, linked in
the BibTEX entry.

@item Find and merge duplicate entries in bibliography.

@item Integrate your bibliographies with LaTeX editors such as Kile and LyX.

@item Import your Zotero library.

@end itemize")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))


(define-public ktp-contact-runner
  (package
    (name "ktp-contact-runner")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-contact-runner-" version ".tar.xz"))
      (sha256
       (base32 "1lcx9smfv2dqrwsbdd4srcq7dqap8bclz788p6jjn04xi6wcbbiq"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("ki18n" ,ki18n)
      ("krunner" ,krunner)
      ("kservice" ,kservice)
      ("qtbase" ,qtbase)))
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))


(define-public kdgantt2
  (package
    (name "kdgantt2")
    (version "16.08.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/kdgantt2-" version ".tar.xz"))
       (sha256
        (base32 "01p3cqqhghvx42mrmxkrsm7cp23972gi86sagjw070scw71fpv0c"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "Gantt chart library for KDE")
    (description "This libraries provide gantt support.")
    (license (list license:gpl2+ license:lgpl2.0+))))


Braucht noch KDE4!

#:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages gpodder)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages kde)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages messaging)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xml))

(define-public amarok
  (package
    (name "amarok")
    (version "2.9.0")
    (source
     (origin
       (method url-fetch)
       ;; TODO: Why is it no longer in stable?
       (uri (string-append "mirror://kde/Attic/amarok/" version
                           "/src/amarok-" version ".tar.xz"))
       (sha256
        (base32 "1z8n66q1qpr6xbm8kpvziqcbkqdc4rbr1fc8hny5csdkkpkqsrz3"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("python" ,python)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(
       ("karchive" ,karchive)
       ("attica" ,attica)
       ("kcodecs" ,kcodecs)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("kdeclarative" ,kdeclarative)
       ("kdnssd" ,kdnssd)
       ("kglobalaccel" ,kglobalaccel)
       ("kguiaddons" ,kguiaddons)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kcmutils" ,kcmutils)
       ("kio" ,kio)
       ("knewstuff" ,knewstuff)
       ("knotifications" ,knotifications)
       ("knotifyconfig" ,knotifyconfig)
       ("kpackage" ,kpackage)
       ("solid" ,solid)
       ("ktexteditor" ,ktexteditor)
       ("threadweaver" ,threadweaver)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("kirigami" ,kirigami)
       ("phonon" ,phonon)

       ("curl" ,curl)
       ("ffmpeg" ,ffmpeg)
       ("libgcrypt" ,libgcrypt)
       ("gdk-pixbuf" ,gdk-pixbuf)
       ("glib" ,glib)
       ;("gobject" ,gobject)
       ("libmtp" ,libmtp)
       ("libxml2" ,libxml2)
       ("loudmouth" ,loudmouth)
       ("openssl" ,openssl)
       ("qca" ,qca)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtsvg" ,qtsvg)
       ("qtquickcontrols2" ,qtquickcontrols2)
       ;; TODO: ("qtwebengine" ,qtwebengine)
       ("taglib" ,taglib) ;; mp3
       ("zlib" ,zlib) ; comression
       ;; webkit
       ("gpodder" ,gpodder) ;;  Mygpo-qt
       ;; optional: liblastfm, qjson, libOFA
       ;; optional: ipod
       ))
;; find_package(Taglib-Extras)
;;     find_package(MySQLAmarok REQUIRED)
;;     find_program( CLAMZ_FOUND clamz PATH )
    (home-page "http://amarok.kde.org")
    (synopsis "Easy to use media player based on the KDE Platform")
    (description "Amarok is a powerful music player with an intuitive
interface.  It nicely integrates with KDE desktop.

Feature Overview:

@itemize
@item Music Collection:
You have a huge music library and want to locate tracks quickly? Let amaroK's
powerful Collection take care of that! It's a database powered music store,
which keeps track of your complete music library, allowing you to find any
title in a matter of seconds.

@item Intuitive User Interface:
You will be amazed to see how easy amaroK is to use! Simply drag-and-drop files
into the playlist. No hassle with complicated  buttons or tangled menus.
Listening to music has never been easier!

@item Streaming Radio:
Web streams take radio to the next level: Listen to thousands of great radio
stations on the internet, for free! amaroK provides excellent streaming
support, with advanced features, such as displaying titles of the currently
playing songs.

@item Context Browser:
This tool provides useful information on the music you are currently listening
to, and can make listening suggestions, based on your personal music taste. An
innovate and unique feature.

@item Visualizations:
amaroK is compatible with XMMS visualization plugins. Allows you to use the
great number of stunning visualizations available on the net. 3d visualizations
with OpenGL are a great way to enhance your music experience.
@end itemize")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

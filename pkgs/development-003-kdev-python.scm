;; Found on download-server, maybe already included in main package
(define-public kdev-python
  (package
    (name "kdev-python")
    (version "5.0.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kdevelop/" version
                          "/src/kdev-python-" version ".tar.xz"))
      (sha256
       (base32 "1zqi9rg4w4m3nr5hwl24f9b84fn9w0j9vqa3li16fbryjd8hj5jx"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("ki18n" ,ki18n)
      ("kitemmodels" ,kitemmodels)
      ("knewstuff" ,knewstuff)
      ("ktexteditor" ,ktexteditor)
      ("qtbase" ,qtbase)
      ("threadweaver" ,threadweaver)))
;; find_package(Python 3.5 REQUIRED)
;;  OR "${PYTHON_VERSION_MINOR}" GREATER 5 )
;; find_package(KDevPlatform ${KDEVPLATFORM_VERSION} REQUIRED)
;; find_package(KDevelop ${KDEVPLATFORM_VERSION} REQUIRED)
    (home-page "")
    (synopsis "
Python plugin for KDevelop
")
    (description "
Python plugin for KDevelop.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

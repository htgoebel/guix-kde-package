(define-public tellico
  (package
    (name "tellico")
    (version "3.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://tellico-project.org/files/tellico-" version
                           ".tar.xz"))
       (sha256
        (base32 "1jzj5h6acjcmf93psyk9jqjry05xllk805z88idymwz0x44yjy5d"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
       ("kcodecs" ,kcodecs)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kguiaddons" ,kguiaddons)
       ("khtml" ,khtml)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kitemmodels" ,kitemmodels)
       ("kjobwidgets" ,kjobwidgets)
       ("knewstuff" ,knewstuff)
       ("kwallet" ,kwallet)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("kxmlgui" ,kxmlgui)
       ("qtbase" ,qtbase)
       ("solid" ,solid)))
;; find_package(Gettext REQUIRED)
;; find_package(LibXml2 REQUIRED)
;; find_package(LibXslt REQUIRED)
;; find_package(KF5 FileMetaData)
;; find_package(KF5 Sane)
;; pkg_check_modules (QImageBlitz qimageblitz>=5.0)
;;     find_package(QImageBlitz 5.0)
;; find_package(KF5 Cddb)
;;   find_package(Libkcddb 5.0)
;; find_package(Taglib)
;; find_package(Yaz 2.0)
;; find_package(PopplerQt5)
;; find_package(Exempi 2.0)
;; find_package(Btparse)
;; find_package(DiscID)
;; find_package(Csv 3.0)
;;         pkg_check_modules(LIBV4L libv4l1>=0.6)
    (home-page "")
    (synopsis "Collection manager for books, videos, music, etc")
    (description "Tellico is a collection manager for KDE. It includes default
collections for books, bibliographies, comic books, videos, music, coins,
stamps, trading cards, and wines, and also allows custom
collections. Unlimited user-defined fields are allowed. Filters are available
to limit the visible entries by definable criteria. Full customization for
printing is possible through editing the default XSLT file. It can import CSV,
Bibtex, and Bibtexml and export CSV, HTML, Bibtex, Bibtexml, and
PilotDB. Entries may be imported directly from different web services such as
amazon.com.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

;; Is this obsolete? Is this part of kactivities or plasma-workspaces?
(define-public kactivities-workspace
  (package
    (name "kactivities-workspace")
    (version "5.5.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kactivities/kactivities-workspace-" version
                          ".tar.xz"))
      (sha256
       (base32 "031wvkn9hm41yg1a18kyb5501h8czjs7kp2kfib5mjn6zal497rp"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; # handled by release scripts
;;  # handled by release scripts
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

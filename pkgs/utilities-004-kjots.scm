;; part of Kontact, but separate package?
(define-public kjots
  (package
    (name "kjots")
    (version "5.0.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kjots/" version
                          "/src/kjots-" version ".tar.xz"))
      (sha256
       (base32 "01kz96aiyj7szakbzgfswg81dh3sf42fcl56r3m8qgd3l76lv150"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kbookmarks" ,kbookmarks)
      ("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kparts" ,kparts)
      ("kxmlgui" ,kxmlgui)))
;; find_packagE()
;; find_package(KF5 Akonadi ${KDEPIMLIBS_LIB_VERSION})
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiNotes ${KDEPIMLIBS_LIB_VERSION})
;; find_package(KF5 PimTextEdit ${PIMTEXTEDIT_LIB_VERSION})
;; find_package(KF5 KontactInterface ${KONTACTINTERFACE_LIB_VERSION})
;; find_package(Grantlee5 "5.0" CONFIG REQUIRED)
;; find_package(Xsltproc)
    (home-page "")
    (synopsis "
Note Taker for KDE
note-taking utility")
    (description "
KJots organises all of your notes into separate books
Features :
   - Multiple books handled
   - Each book has many named pages
   - Books and pages can be rearranged by drag-and-drop
   - Keyboard shortcuts are available for many functions
   - Automatic saving means your notes are safe from loss.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

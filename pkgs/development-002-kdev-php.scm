;; Found on download-server, maybe already included in main package
(define-public kdev-php
  (package
    (name "kdev-php")
    (version "5.0.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kdevelop/" version
                          "/src/kdev-php-" version ".tar.xz"))
      (sha256
       (base32 "1ixf03d03yc0dk29n0i6df912gw5xc58p3hsw6680m68xns8kv14"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("karchive" ,karchive)
      ("kcmutils" ,kcmutils)
      ("ki18n" ,ki18n)
      ("kitemmodels" ,kitemmodels)
      ("ktexteditor" ,ktexteditor)
      ("qtbase" ,qtbase)
      ("qtwebkit" ,qtwebkit)
      ("threadweaver" ,threadweaver)))
;; find_package(KDevPlatform ${KDEVPLATFORM_VERSION} REQUIRED)
;; find_package(KDevelop-PG-Qt REQUIRED)
    (home-page "")
    (synopsis "
PHP plugin for kdevelop
")
    (description "
This plugin adds PHP language support (including classview and code-completion)
to KDevelop.
---
This plugin adds PHP language support (including classview and code-completion)
to KDevelop.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

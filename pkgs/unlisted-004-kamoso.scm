;; Web-cam program
(define-public kamoso
  (package
    (name "kamoso")
    (version "3.2.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kamoso/3.2/src/kamoso-" version
                          ".tar.xz"))
      (sha256
       (base32 "1rvka31d786gik1sdlz72gmbvm216zr478czk45c93d6m6vmqsm7"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; find_package(Qt5 GStreamer 1.1.90)
;; find_package(KDEExperimentalPurpose REQUIRED)
;; find_package(UDev REQUIRED)
    (home-page "http://www.kde-apps.org/content/show.php/Kamoso?content=111750")
    (synopsis "
Application to take pictures and videos out of your webcam
tool to take pictures and videos from your webcam")
    (description "
Kamoso is an application to take pictures and videos out of your webcam.

Kamoso is a utility that does the very simple actions a webcam offers,
like taking pictures or recording videos and adds some extra features that will
make the webcam usage both funnier and more useful.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kdegraphics-thumbnailers
  (package
    (name "kdegraphics-thumbnailers")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kdegraphics-thumbnailers-" version ".tar.xz"))
      (sha256
       (base32 "01q2czzqq240cbp9yg7mji2q15kmiwn1aqs6iii00i56yy2mwaxq"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kio" ,kio)
      ("qtbase" ,qtbase)))
;; find_package(KF5 KExiv2)
;; find_package(KF5 KDcraw)
    (home-page "")
    (synopsis "
Postscript and PDF ThumbCreator
")
    (description "
PostScript and PDF files ThumbCreator.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

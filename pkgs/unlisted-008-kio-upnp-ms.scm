;; Found on download-server, maybe already included in another package
(define-public kio-upnp-ms
  (package
    (name "kio-upnp-ms")
    (version "0.8.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kio-upnp-ms/" version
                          "/src/kio-upnp-ms-" version ".tar.gz"))
      (sha256
       (base32 "171dbqn8mlsk3fxi0gxrfrd0br4vrvzm9l9kifkw5bl3x9345fkg"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; FIND_PACKAGE(KDE4 REQUIRED)
;; find_package(HUpnp REQUIRED)
;; INCLUDE_DIRECTORIES( ${KDE4_INCLUDES}
;;     ${CMAKE_SOURCE_DIR} build . )
;;     TARGET_LINK_LIBRARIES(upnpmstest ${KDE4_KDEUI_LIBS}
;;         ${KDE4_KPARTS_LIBS} )
;;     TARGET_LINK_LIBRARIES(stattest ${KDE4_KDEUI_LIBS}
;;         ${KDE4_KPARTS_LIBS} )
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

;; also as separate package in http://download.kde.org/stable/umbrello/latest/src/
(define-public umbrello
  (package
    (name "umbrello")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/umbrello-" version ".tar.xz"))
      (sha256
       (base32 "1a4jhfmh2p1vsx8702ham550blkjj42ibwigcink6s9cadwk90cl"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("ktexteditor" ,ktexteditor)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)
      ("qtwebkit" ,qtwebkit)))
;; " OFF)
;;     find_package(Qt4 4.4.3 REQUIRED QtCore QtGui QtXml QtTest QtWebKit)
;;   # for unit tests
;;     find_package(KDE4 REQUIRED)
;;         foreach(a ${ARGN})
;;             elseif(a STREQUAL "NAME_PREFIX")
;;             elseif(a STREQUAL "GUI")
;;             elseif(a STREQUAL "TEST_NAME")
;;             elseif (mode EQUAL 0)
;;             elseif(mode EQUAL 1)
;;             elseif(mode EQUAL 2)
;;             elseif(mode EQUAL 4)
;;         endforeach(a)
;;         kde4_add_unit_test(${targetname} TESTNAME ${testname} ${guimode} ${sources})
;;         foreach(_target ${ARGN})
;;             set_target_properties(${_target}
;;                                   PROPERTIES
;;                                   WIN32_EXECUTABLE FALSE
;;                                   MACOSX_BUNDLE FALSE
;;                                  )
;;         endforeach()
;;     KDE4_NO_ENABLE_FINAL(umbrello)
;;
;;
;;
;; find_package(LibXslt)
;; find_package(LibXml2)
;; add_unstable_feature(WIDGET_SHOW_DOC) # show documentation in classes ticket xxx
;; add_unstable_feature(NEW_CODE_GENERATORS) # new c++ code generator
;; add_unstable_feature(UML_OBJECTS_WINDOW) # objects dock window
;; add_unstable_feature(XMIRESOLUTION) # see https://bugs.kde.org/show_bug.cgi?id=90103
    (home-page "http://www.kde.org/")
    (synopsis "
UML Modeller
UML modelling tool and code generator")
    (description "
Umbrello UML Modeller is a UML diagramming tool for KDE.

Umbrello UML Modeller is a Unified Modelling Language diagram editor for KDE.
It can create diagrams of software and other systems in the industry-standard
UML format, and can also generate code from UML diagrams in a variety of
programming languages.

This package is part of the KDE Software Development Kit module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public libkexiv2
  (package
    (name "libkexiv2")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkexiv2-" version ".tar.xz"))
      (sha256
       (base32 "02fr10prqmpaqqlcj8hf5h4b3vhhiwkfsbpzlag64n5764x1hl3f"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("qtbase" ,qtbase)))
;; find_package(Exiv2 ${EXIV2_MIN_VERSION} REQUIRED)
    (home-page "")
    (synopsis "
Wrapper around exiv2 library
Qt like interface for the libexiv2 library")
    (description "
Libkexiv2 is a wrapper around Exiv2 library to manipulate pictures
metadata as EXIF/IPTC and XMP.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

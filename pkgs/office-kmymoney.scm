(define-public kmymoney
  (package
    (name "kmymoney")
    (version "4.8.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kmymoney/" version
                          "/src/kmymoney-" version ".tar.xz"))
      (sha256
       (base32 "1hlayhcmdfayma4hchv2bfyg82ry0h74hg4095d959mg19qkb9n2"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; " BRANCH "${BRANCH}")
;; find_package(KDE4 4.6.0 REQUIRED)
;;   set_target_properties(Qt4::moc PROPERTIES
;;     IMPORTED_LOCATION "${QT_MOC_EXECUTABLE}"
;;   )
;;   find_package(Boost "1.33.1")
;;   find_package(Boost "1.33.1" COMPONENTS graph)
;; find_package(QGpgme REQUIRED)
;; find_package(KdepimLibs REQUIRED)
;; find_package(SharedMimeInfo REQUIRED)
;; find_package(Doxygen)
;; find_package(LibAlkimia 4.3.2 REQUIRED)
;;   find_package(GMP REQUIRED)
;; find_package(PkgConfig)
;;   find_package(LibOfx)
;;   find_package(GWENHYWFAR CONFIG "4.13.0")
;;   find_package(gwengui-qt4 CONFIG "4.13.0")
;;   find_package(gwengui-cpp CONFIG "4.13.0")
;;")
;;   find_package(AQBANKING CONFIG "5.5.1")
;;")
;;   find_package(Libical)
;; find_package(KActivities)
;; ?$|^profile$")
;; " OFF)
;; " OFF)
;; " OFF)
;;   "~$"         # all files ending in '~'
;;   "\\\\.m4$"   # since this is a cmake dist, we remove automake files
;;   "\\\\.m4\\\\.in$" "\\\\.am$" "Makefile\\\\.dist" "configure\\\\.in"
;;   )
;;   rpmtools_add_rpm_targets(kmymoney          kmymoney.spec.in)
;;   rpmtools_add_rpm_targets(kmymoney-unstable kmymoney.spec.in)
;; nice_yesno("LIBOFX_FOUND")
;; nice_yesno("KBANKING_FOUND")
;; nice_yesno("WEBOOB_FOUND")
;; nice_yesno("LIBICAL_FOUND")
;; nice_yesno("ENABLE_SQLCIPHER")
;; nice_yesno("USE_QT_DESIGNER")
;; nice_yesno("USE_DEVELOPER_DOC")
;; nice_yesno("KDE4_BUILD_TESTS")
;; nice_yesno("DOXYGEN_FOUND")
;; nice_yesno("USE_HTML_HANDBOOK")
;; :
;; --------------------------------------------
;; OFX plugin:                              ${nice_LIBOFX_FOUND}
;; KBanking plugin:                         ${nice_KBANKING_FOUND}
;; weboob plugin:                           ${nice_WEBOOB_FOUND}
;; iCalendar export plugin:                 ${nice_LIBICAL_FOUND}
;; SQLCipher plugin:                        ${nice_ENABLE_SQLCIPHER}
;; --------------------------------------------
;; Configure results (developer options):
;; --------------------------------------------
;; Qt-Designer library support:             ${nice_USE_QT_DESIGNER}
;; Generate developer documentation:        ${nice_USE_DEVELOPER_DOC}
;; Build unit tests:                        ${nice_KDE4_BUILD_TESTS}
;; Generate API documentation with Doxygen: ${nice_DOXYGEN_FOUND}")
    (home-page "http://kmymoney2.sourceforge.net")
    (synopsis "
The Personal Finances Manager
personal finance manager for KDE")
    (description "
KMyMoney Personal Finance Manager.
---
KMyMoney Personal Finance Manager.

KMyMoney is the Personal Finance Manager for KDE. It operates similar to
MS-Money and Quicken, supports different account types, categorisation of
expenses, QIF import/export, multiple currencies and initial online banking
support.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

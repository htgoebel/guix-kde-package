(define-public kdiff3
  (package
    (name "kdiff3")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "https://sourceforge.net/projects/kdiff3/files/kdiff3/0.9.98/kdiff3-0.9.98.tar.gz"))
      (sha256
       (base32 "0s6n1whkf5ck2r8782a9l8b736cj2p05and1vjjh7d02pax1lb40"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;;  will be built.")
;;  will not be built.)")
;;  ...")
;;   MACRO_OPTIONAL_FIND_PACKAGE(LibKonq)
;;  will be built.")
;;  will not be built.)")
    (home-page "http://kdiff3.sourceforge.net")
    (synopsis "
Utility for comparing/merging up to three text files or directories
compares and merges 2 or 3 files or directories")
    (description "
A file and directory diff and merge tool which:
   * compares and merges two or three text input files or directories
   * shows the differences line by line and character by character(!)
   * provides an automatic merge-facility
   * has an editor for comfortable solving of merge-conflicts
   * provides network transparency via KIO
   * has options to highlight or hide changes in white-space or comments
---
A file and directory diff and merge tool which:
   * compares and merges two or three text input files or directories
   * shows the differences line by line and character by character(!)
   * provides an automatic merge-facility
   * has an editor for comfortable solving of merge-conflicts
   * provides network transparency via KIO
   * has options to highlight or hide changes in white-space or comments

KDiff3 compares two or three input files and shows the differences
line by line and character by character. It provides an automatic
merge facility and an integrated editor for comfortable solving of
merge conflicts. KDiff3 allows recursive directory comparison and
merging as well.

This is the standard version of KDiff3, highly integrated into KDE.
It has got KIO support (allowing for remote access of files and
direct access to files in compressed archives) and integration into
konqueror's context menu. There's also a stripped-down version called
kdiff3-qt not depending on the KDE libraries.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

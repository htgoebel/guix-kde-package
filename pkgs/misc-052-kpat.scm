(define-public kpat
  (package
    (name "kpat")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kpat-" version ".tar.xz"))
      (sha256
       (base32 "04qvv7q7rlsiyff2isy18h2fbz2lnljal5spq5qg9zl6v8hx6qzm"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("knewstuff" ,knewstuff)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; find_package(KF5 KDEGames)
    (home-page "http://games.kde.org/")
    (synopsis "
Several patience card games
solitaire card games")
    (description "
KPatience is a relaxing card sorting game.
To win the game a player has to arrange a single deck of cards in certain order
amongst each other.

KPatience is a collection of fourteen solitaire card games, including Klondike,
Spider, and FreeCell.

This package is part of the KDE games module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

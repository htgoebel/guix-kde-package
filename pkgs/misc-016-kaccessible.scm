(define-public kaccessible
  (package
    (name "kaccessible")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kaccessible-" version ".tar.xz"))
      (sha256
       (base32 "162a4pw61b3rhq5mf7zdhgyyhbrxhr9fjw7bidicw7aljiy2cwb9"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;;     foreach (_i ${_sources})
;;     endforeach (_i ${ARGN})
;; dbus_add_activation_service(org.kde.kaccessible.service.in)
;; macro_optional_find_package(Speechd)
;; qt4_wrap_cpp(kaccessibleapp_SRCS kaccessibleapp.h)
    (home-page "http://www.kde.org")
    (synopsis "
Accessibility services like focus reading and a screenreader for kde
accessibility services for Qt applications")
    (description "
Kaccessible implements a QAccessibleBridgePlugin to provide accessibility
services like focus tracking and a screenreader.

kaccessible implements a QAccessibleBridgePlugin to provide accessibility
services like focus tracking and a screen reader.

Components:
 * kaccessibleapp: a D-Bus activation service that acts as proxy.
 * kaccessiblebridge: a Qt plugin which will be loaded by the QAccessible
   framework in each Qt and KDE application.

This package is part of the KDE accessibility module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

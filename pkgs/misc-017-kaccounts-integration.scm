(define-public kaccounts-integration
  (package
    (name "kaccounts-integration")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kaccounts-integration-" version ".tar.xz"))
      (sha256
       (base32 "0w8h33sysf590xyg4bvf2a2z39kzc0wn6mxv31mrf68b6ks7b9h2"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; find_package(AccountsQt5 ${ACCOUNTSQT_DEP_VERSION} CONFIG)
;; find_package(SignOnQt5 ${SIGNONQT_DEP_VERSION} CONFIG)
;; find_package(LibAccountsGlib ${ACCOUNTSGLIB_DEP_VERSION})
    (home-page "")
    (synopsis "

System to administer web accounts")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

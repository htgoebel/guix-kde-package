(define-public ktp-text-ui
  (package
    (name "ktp-text-ui")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-text-ui-" version ".tar.xz"))
      (sha256
       (base32 "0ssxr35vcqjppnppyjxwzrkzvb5sx45fpnvbzsv9md5rnlf2xh1k"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("karchive" ,karchive)
      ("kcmutils" ,kcmutils)
      ("kdbusaddons" ,kdbusaddons)
      ("kemoticons" ,kemoticons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kpeople" ,kpeople)
      ("kservice" ,kservice)
      ("ktextwidgets" ,ktextwidgets)
      ("kwebkit" ,kwebkit)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtwebkit" ,qtwebkit)
      ("sonnet" ,sonnet)))
;; find_package(Qt5 TextToSpeech)
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

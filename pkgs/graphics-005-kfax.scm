;; Dated 2010
(define-public kfax
  (package
    (name "kfax")
    (version "3.3.6")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/extragear/kfax-" version
                           "-kde4.4.0.tar.bz2"))
       (sha256
        (base32 "07bddjhvhwyzgpyj6svlx1v6sg02sp73zw4vz709p754f9gv55v8"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package (KDE4 REQUIRED)
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis "")
    (description "")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

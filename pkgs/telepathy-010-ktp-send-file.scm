(define-public ktp-send-file
  (package
    (name "ktp-send-file")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-send-file-" version ".tar.xz"))
      (sha256
       (base32 "1m7cj3q4lzj8qj2cla6wm1crpjid77b3f3yywri167f1zd4p51z6"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; find_package(Qt5 5.0)
;; find_package(KTp REQUIRED)
;; qt5_wrap_ui(KTP_SEND_FILE_SRCS mainwindow.ui)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

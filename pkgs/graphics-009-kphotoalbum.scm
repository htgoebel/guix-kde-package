(define-public kphotoalbum
  (package
    (name "kphotoalbum")
    (version "5.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kphotoalbum/" version
                          "/kphotoalbum-" version ".tar.xz"))
      (sha256
       (base32 "128b7c6l95vlkiz979ywi84j4250wadv8jqc9n1db1qpna021l4s"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kjobwidgets" ,kjobwidgets)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)))
;; set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS ";Debug;Release;RelWithDebInfo;MinSizeRel")
;; foreach(part in
;;         AnnotationDialog
;;         EXIF
;;         HTMLGENERATOR
;;         JOBMANAGER
;;         KIM_IMPORT
;;         KIPI
;;         MAP
;;         ResizeSlider
;;         )
;; endforeach()
;; find_package(JPEG REQUIRED)
;; find_package(Exiv2 REQUIRED)
;; find_package(KF5 Kipi)
;; "
;;     )
;; find_package(KF5 KDcraw)
;;     find_package(KF5 KFace)
;; find_package(KF5 KGeoMap)
    (home-page "http://kphotoalbum.org")
    (synopsis "
Photo album manager
tool for indexing, searching and viewing images by keywords for KDE")
    (description "
KPhotoAlbum supports all the normal image formats including
raw formats produced by most digital cameras and scanners
(using dcraw to do the decoding).
It is also possible to use the thumbnails embedded in raw
images (that are usually of decent size and quality) for
fast viewing so there is no need to decode the whole raw image.

KPhotoAlbum lets you index, search, group and view images by keywords, date,
locations and persons. It provides a quick and elegant way to lookup groups of
images when you have thousands of pictures on your hard disk.

The information associated with each photo is stored in an XML file. Together
with its keywords, KPhotoAlbum stores each picture's MD5 sum, so it will
recognize them even if you move them to another directory. KPhotoAlbum can
also create HTML galleries with the images you select.

KPhotoAlbum can also make use of the KIPI image handling plugins to extend its
capabilities. The kipi-plugins package contains many useful extensions. Among
others, it contains extensions for photo manipulation, importing, exporting
and batch processing.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

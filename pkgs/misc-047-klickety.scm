(define-public klickety
  (package
    (name "klickety")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/klickety-" version ".tar.xz"))
      (sha256
       (base32 "0mim86wxcljs192q9y6a6i326sic350jd89m1vx3p78dwpj35q42"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; find_package(KF5 KDEGames)
    (home-page "http://games.kde.org/")
    (synopsis "
An adaptation of the Clickomania game
SameGame puzzle game")
    (description "
Klickety is an adaptation of the Clickomania game.
The rules are similar to those of the Same game: your goal is to clear
the board by clicking on groups to destroy them.

Klickety is a puzzle game where the player removes groups of colored marbles to
clear the board.

This package is part of the KDE games module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

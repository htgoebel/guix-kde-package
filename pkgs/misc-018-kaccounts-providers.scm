(define-public kaccounts-providers
  (package
    (name "kaccounts-providers")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kaccounts-providers-" version ".tar.xz"))
      (sha256
       (base32 "0iqqwfjadsf7nhrqvpzypazipris4ljhf6daprxwxqa2bfigr5j2"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kpackage" ,kpackage)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; find_package(Intltool REQUIRED)
;; find_package(KAccounts REQUIRED)
    (home-page "")
    (synopsis "

KDE providers for accounts sign-on")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

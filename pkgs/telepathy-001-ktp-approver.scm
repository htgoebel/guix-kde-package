(define-public ktp-approver
  (package
    (name "ktp-approver")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-approver-" version ".tar.xz"))
      (sha256
       (base32 "08aj2j9bjdyigl4cx65v5fjsdayid07mx0mq72iy6l17d8z4b39a"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kconfig" ,kconfig)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("knotifications" ,knotifications)
      ("kservice" ,kservice)
      ("qtbase" ,qtbase)))
;; find_package(TelepathyQt5 REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

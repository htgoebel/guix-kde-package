(define-public yakuake
  (package
    (name "yakuake")
    (version "3.0.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/yakuake/" version
                          "/src/yakuake-" version ".tar.xz"))
      (sha256
       (base32 "0vcdji1k8d3pz7k6lkw8ighkj94zff2l2cf9v1avf83f4hjyfhg5"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("karchive" ,karchive)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kglobalaccel" ,kglobalaccel)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kparts" ,kparts)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("qtbase" ,qtbase)
      ("qtx11extras" ,qtx11extras)))
;; find_package(X11)
    (home-page "")
    (synopsis "
Very powerful Quake style Konsole
Quake-style terminal emulator based on KDE Konsole technology")
    (description "
Yakuake is a Quake-style terminal emulator based on KDE Konsole technology.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

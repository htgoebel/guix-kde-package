(define-public kde-gtk-config
  (package
    (name "kde-gtk-config")
    (version "2.2.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kde-gtk-config/" version
                          "/src/kde-gtk-config-" version ".tar.xz"))
      (sha256
       (base32 "11aw86jcjcg3ywnzrxy9x8dvd73my18k0if52fnvyvmb75z0v2cw"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis "
GTK2 and GTK3 configurator for KDE
")
    (description "
Configuration dialog to adapt GTK applications appearance to your taste
under KDE. Among its many features, it lets you:
- Choose which theme is used for GTK2 and GTK3 applications.
- Tweak some GTK applications behaviour.
- Select what icon theme to use in GTK applications.
- Select GTK applications default fonts.
- Easily browse and install new GTK2 and GTK3 themes.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

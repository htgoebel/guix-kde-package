(define-public kuser
  (package
    (name "kuser")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kuser-" version ".tar.xz"))
      (sha256
       (base32 "0yhwlcq24hyn00mfjc8qqr05gy3p2h8j1ls24jdka7h5l6kwvcpq"))))
    (properties `((tags . ("Desktop" "KDE" "System"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; find_package(KdepimLibs REQUIRED)
;; test_big_endian(WORDS_BIGENDIAN)
    (home-page "http://www.kde.org/")
    (synopsis "
Users and groups manager
user and group administration tool")
    (description "
Kuser is a tool to create, remove and modify user accounts and groups.

KUser is an application for managing users and groups on your system.

This package is part of the KDE administration module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

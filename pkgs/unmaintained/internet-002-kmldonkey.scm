;; Comment from Debian # The upstream version looks like 2.0.2+kde4.2.4, but the program actually
# reports itself as 2.0.5.  Also, the version number does not appear to change
# between KDE releases, but the code does change.
opts=uversionmangle=s/2.0.2/2.0.5/,dversionmangle=s/\+/-/ \
(define-public kmldonkey
  (package
    (name "kmldonkey")
    (version "2.0.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/extragear/kmldonkey-2.0.2-kde4.4.0.tar.bz2"))
      (sha256
       (base32 "0gv01c8xwzvhp7m9y83a94wv6ypijxl65mc1wh97glinj0j4x8yz"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;;     macro_optional_find_package(Plasma)
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "http://extragear.kde.org/apps/kmldonkey/")
    (synopsis "
A frontend for MLDonkey
KDE GUI for MLDonkey")
    (description "
KMLDonkey is a frontend for MLDonkey, a powerful P2P file sharing tool,
designed for the KDE desktop.

Authors:
Sebastian Sauer
Petter E. Stokke

KMLDonkey is a frontend for MLDonkey, a powerful P2P file sharing tool,
designed for the KDE desktop.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

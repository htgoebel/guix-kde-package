(define-public klinkstatus
  (package
    (name "klinkstatus")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/klinkstatus-" version ".tar.xz"))
      (sha256
       (base32 "0kmjh0398k6fpz6lgz6d5rb79xl6wpgd4j56zacpha9046cfnmsk"))))
    (properties `((tags . ("Desktop" "KDE" "Development"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; find_package(KdepimLibs REQUIRED)
;; macro_optional_find_package(LibTidy)
    (home-page "")
    (synopsis "
KlinkStatus is a link checker
web link validity checker")
    (description "
KLinkStatus is a link checker. It allows you to search internal and external
links in either your entire web site, a single page or a specified depth. For
performance, links can be checked simultaneously.

* Support several protocols (allowing fast checking of
local documents): http, ftp, ssh (fish or sftp) and file.
* Proxy support
* Allows authentication when checking restricted documents
* Supports the latest Web standards-- HTML 4.0, HTTP 1.1
* Server-Side Includes (SSI, aka SHTML) are supported and checked
* Regular expressions to restrict which URLs are searched
* Show link results as they are checked
* Tree like view (that reflects the file structure of the documents) or
  flat view
* Limit the search depth
* Fragment identifiers ("#" anchor links that point to a specific
 section in a document) are supported and checked
* Pause/Resume of checking session
* History of checked URLs
* Tabbed checking (allow multiple sessions at the same time)
* Filter checked links (good, broken, malformed and undetermined)
* Configurable number of simultaneous connections (performance tunning)
* Other configurable options like "check external links",
"check parent folders", "timeout"
* Good integration with Quanta+

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

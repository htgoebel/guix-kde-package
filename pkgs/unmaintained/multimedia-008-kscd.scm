(define-public kscd
  (package
    (name "kscd")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kscd-" version ".tar.xz"))
      (sha256
       (base32 "1mpba78m4hs8541n4ydz7vswq1chi0fmmlfw2kqnrzarcandyga0"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(Qt4 REQUIRED)
;; find_package(KDE4 REQUIRED)
;; find_package(MusicBrainz3 REQUIRED)
;; qt4_add_dbus_adaptor(kscd_SRCS org.kde.kscd.cdplayer.xml kscd.h KSCD)
;; qt4_add_dbus_adaptor( kscd_SRCS dbus/org.freedesktop.MediaPlayer.root.xml dbus/RootDBusHandler.h KsCD::RootDBusHandler RootAdaptor RootAdaptor)
;; qt4_add_dbus_adaptor( kscd_SRCS dbus/org.freedesktop.MediaPlayer.player.xml dbus/PlayerDBusHandler.h KsCD::PlayerDBusHandler PlayerAdaptor PlayerAdaptor)
;; qt4_add_dbus_adaptor( kscd_SRCS dbus/org.freedesktop.MediaPlayer.tracklist.xml dbus/TracklistDBusHandler.h KsCD::TracklistDBusHandler TracklistAdaptor TracklistAdaptor)
    (home-page "http://www.kde.org/")
    (synopsis "
KDE Audio CD Player
audio CD player")
    (description "
KsCD is a small, fast, CDDB enabled audio CD player which supports
multiple platforms.

KsCD is an audio CD player.  It uses the Compact Disc DataBase to
fetch album information automatically.

This package is part of the KDE multimedia module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

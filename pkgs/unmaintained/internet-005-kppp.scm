(define-public kppp
  (package
    (name "kppp")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kppp-" version ".tar.xz"))
      (sha256
       (base32 "0lqv95zfzcik8k95a39s6whjsnq6g15amnxlzy898liyxkr499bc"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;;
;;
;;
;;
;;
;; qt4_add_dbus_interfaces(kppp_SRCS org.kde.kppp.xml)
;; qt4_add_dbus_adaptor(kppp_SRCS org.kde.kppp.xml kpppwidget.h KPPPWidget)
;;     macro_add_compile_flags(kppp ${KDE4_CXX_FPIE_FLAGS})
;;     macro_add_link_flags(kppp ${KDE4_PIE_LDFLAGS})
    (home-page "http://www.kde.org/")
    (synopsis "
Internet Dial-Up Tool
modem dialer for KDE")
    (description "
KPPP is used to setup PPP (Point-to-Point Protocol) connections. This is most
useful for connecting with a cell phone "modem" card these days. It is also use
to configure real modem connections.

KPPP is a modem dialer for connecting to a dial-up Internet Service Provider.
It displays statistics and accounting information to help users keep track of
connection costs.

This package is part of the KDE 4 networking module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

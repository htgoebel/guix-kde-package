;; Dated 2010
(define-public kmid
  (package
    (name "kmid")
    (version "2.4.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "https://sourceforge.net/projects/kmid2/files/" version
                          "/kmid-" version ".tar.bz2/download"))
      (sha256
       (base32 "0fz4gp8lc7qai5pr2cvcqyvwzlbyw41qwkcgiqkqxlqis1jwzi24"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package (KDE4 REQUIRED)
;;   find_package (PkgConfig)
;;     pkg_check_modules (ALSA alsa>=1.0)
;;       pkg_check_modules(DRUMSTICK-FILE drumstick-file>=0.4)
;;       pkg_check_modules(DRUMSTICK-ALSA drumstick-alsa>=0.4)
;;   find_package (PkgConfig)
;;     pkg_check_modules(DRUMSTICK drumstick-file>=0.4)
    (home-page "")
    (synopsis "
A MIDI/karaoke player for KDE
")
    (description "
KMid2 is a MIDI/karaoke file player, with configurable midi mapper, real
Session Management, drag & drop, customizable fonts, etc. It has a very
nice interface which let you easily follow the tune while changing the
color of the lyrics.
It supports output through external synthesizers, AWE, FM and GUS cards.
It also has a keyboard view to see the notes played by each instrument.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

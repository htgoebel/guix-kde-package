(define-public kfilereplace
  (package
    (name "kfilereplace")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kfilereplace-" version ".tar.xz"))
      (sha256
       (base32 "0gym9bmkyjwg97khy6xxiaidjp6wi98fzmk7wa97wbdqc0qvswja"))))
    (properties `((tags . ("Desktop" "KDE" "Utilities"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; qt4_add_dbus_adaptor( kfilereplacepart_PART_SRCS org.kde.kfilereplace.xml kfilereplacepart.h KFileReplacePart )
;; kde4_add_ui3_files(kfilereplacepart_PART_SRCS
;;    kfilereplaceviewwdg.ui
;;    kaddstringdlgs.ui
;;    knewprojectdlgs.ui
;;    koptionsdlgs.ui )
    (home-page "")
    (synopsis "
KDE utility that replace some strings in many of files in one operation
batch search-and-replace component")
    (description "
KFileReplace is a very capable multi-line, mutli-file, multi-directory find and
replace that can create backups, use wildcards and regexps.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

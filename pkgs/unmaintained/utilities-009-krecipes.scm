(define-public krecipes
  (package
    (name "krecipes")
    (version "2.1.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/krecipes/" version
                          "/src/krecipes-" version ".tar.xz"))
      (sha256
       (base32 "0x38xkabgq7dkzq9rk6ayjlaap1lpsklmqxkzgn1kx4cl0cd7i7x"))))
    (properties `((tags . ("Desktop" "KDE" "Utilities"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
    (home-page "http://krecipes.sourceforge.net/")
    (synopsis "
Krecipes - Your Way to Cook with Tux
recipes manager for KDE")
    (description "
A highly configurable recipe manager, designed to make organizing your
personal recipes collection fast and easy. Features include: shopping
lists, nutrient analysis, advanced search, recipe ratings, import/export
various formats, and more.

Krecipes is a KDE application designed to manage recipes. It can help you to
do your shopping list, search through your recipes to find what you can do
with available ingredients and a diet helper. It can also import or export
recipes from files in various format (eg RecipeML or Meal-Master) or from
databases.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

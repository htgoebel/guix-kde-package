(define-public jovie
  (package
    (name "jovie")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/jovie-" version ".tar.xz"))
      (sha256
       (base32 "190c4g587x4wxbfksf0mszq5qv1pzny6bkd3w2pwwsj222bl0fxa"))))
    (properties `((tags . ("Desktop" "KDE" "Utilities"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; find_package( Qt4 REQUIRED QT_USE_QT* )
;;   find_package(Speechd)
    (home-page "http://accessibility.kde.org/developer/kttsd")
    (synopsis "
Subsystem within the KDE desktop for conversion of text to audible speech
text-to-speech system")
    (description "
Jovie is a subsystem within the KDE desktop for
conversion of text to audible speech. Jovie is currently under development
and aims to become the standard subsystem for all KDE applications
to provide speech output.
User Features:
 * Speak any text from the KDE clipboard.
 * Speak any plain text file.
 * Speak all or any portion of a text file from Kate.
 * Speak all or any portion of an HTML page from Konqueror.
 * Use as the speech backend for KMouth and KSayIt.
 * Speak KDE notifications (KNotify).
 * Long text is parsed into sentences. User may backup by sentence or
    paragraph, replay, pause, and stop playing.
 * Audio output via GStreamer (version 0.8.7 or later)

The Jovie text-to-speech system is a plugin based service that allows any KDE
(or non-KDE) application to speak using the D-Bus interface.

It uses the speech-dispatcher daemon for the actual speech job; kmouth is an
useful front-end for it.

This package is part of the KDE accessibility module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public cervisia
  (package
    (name "cervisia")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/cervisia-" version ".tar.xz"))
      (sha256
       (base32 "14r55ngvz4rci1h3iqdwbfcyxmm2b04c5smkv9x0bqxq4zz2yfvk"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kdesu" ,kdesu)
      ("kiconthemes" ,kiconthemes)
      ("kinit" ,kinit)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("kparts" ,kparts)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
    (home-page "http://www.kde.org/")
    (synopsis "
K Desktop Environment - Software Development Kit
graphical CVS client")
    (description "
Software Development Kit for the K Desktop Environment.

Cervisia is a front-end for the CVS version control system client.

In addition to basic and advanced CVS operations, it provides a convenient
graphical interface for viewing, editing, and manipulating files in a CVS
repository or working directory.  It includes tools designed to ease the use
of CVS, such as a log browser, conflict resolver, and changelog editor that
checks for incorrect formatting.

This package is part of the KDE Software Development Kit module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

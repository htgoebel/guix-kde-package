;; ?
(define-public kdeedu-data
  (package
    (name "kdeedu-data")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kdeedu-data-" version ".tar.xz"))
      (sha256
       (base32 "0vqzamp5fc2d0i9qn6986f3a1s1fdbrlzrsnimpdfcas5xngbv5m"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
    (home-page "")
    (synopsis "
Common files needed by kdeedu related applications
transitional package for kdeedu-data")
    (description "
This packages provides icons and contains KEduVocDocument and its related class
for reading from/writing to the KVTML format (and others too).
It's used by kdeedu related applications ( kanagram, khangman,kwordquiz)
---
This packages provides icons and contains KEduVocDocument and its related class
for reading from/writing to the KVTML format (and others too).
It's used by kdeedu related applications ( kanagram, khangman,kwordquiz)

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

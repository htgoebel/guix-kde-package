(define-public kdegraphics-mobipocket
  (package
    (name "kdegraphics-mobipocket")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kdegraphics-mobipocket-" version ".tar.xz"))
      (sha256
       (base32 "06zqny8idaw7s85h7iprbwdp7y1qspfp7yh5klwav12p72nn54w2"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "
A collection of plugins to handle mobipocket files
")
    (description "
A collection of plugins to handle mobipocket files.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

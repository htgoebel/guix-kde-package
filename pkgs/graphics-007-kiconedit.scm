;; Dated 2010
(define-public kiconedit
  (package
    (name "kiconedit")
    (version "4.4.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/extragear/kiconedit-" version
                          ".tar.bz2"))
      (sha256
       (base32 "03759r32zr8r7swmfxn9nxxr0zn9276fvkvck57x3gc97kx2xi1b"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package (KDE4 REQUIRED)
;;
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

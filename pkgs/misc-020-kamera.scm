(define-public kamera
  (package
    (name "kamera")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kamera-" version ".tar.xz"))
      (sha256
       (base32 "04p19qv5ssf5wlpfqzhqsi8281pzcdjkd0jy177f9r7qgqq4lkgr"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; find_package(Gphoto2 REQUIRED)
    (home-page "https://projects.kde.org/projects/kde/kdegraphics/kamera")
    (synopsis "
Kamera ioslave
digital camera support for KDE applications")
    (description "
KDE integration for gphoto2 cameras.

This package allows any KDE application to access and manipulate pictures on
a digital camera.

This package is part of the KDE graphics module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

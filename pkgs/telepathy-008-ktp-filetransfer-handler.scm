(define-public ktp-filetransfer-handler
  (package
    (name "ktp-filetransfer-handler")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-filetransfer-handler-" version ".tar.xz"))
      (sha256
       (base32 "0ddmrpnh6myl7m3drh3cxx9pp06al98g8mppnysmgswgkwafg6cq"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; find_package (KTp REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

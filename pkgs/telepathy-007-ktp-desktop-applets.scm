(define-public ktp-desktop-applets
  (package
    (name "ktp-desktop-applets")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-desktop-applets-" version ".tar.xz"))
      (sha256
       (base32 "1pd8vzwmxcsw6pq80r9casi07wwisr70l5ffm231v9d73k4fw7kv"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kwindowsystem" ,kwindowsystem)
      ("plasma-framework" ,plasma-framework)
      ("qtdeclarative" ,qtdeclarative)))
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

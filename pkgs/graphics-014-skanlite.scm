(define-public skanlite
  (package
    (name "skanlite")
    (version "2.0.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/skanlite/2.0/skanlite-" version
                          ".tar.xz"))
      (sha256
       (base32 "0dh2v8029gkhcf3pndcxz1zk2jgpihgd30lmplgirilxdq9l2i9v"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; # yiels ecm_setup_version
;;  # yields ${XDG_APPS_INSTALL_DIR}
;; find_package(PNG REQUIRED)
;; find_package(KF5 # alias to find_package(KFGuiAddons)
;;         CoreAddons # KAboutData
;;         I18n
;;         XmlGui # KAboutApplicationDialog
;;         KIO # contains the KIOWidgets which we use in target_link_libraries
;;         DocTools # yields kdoctools_create_handbook
;;         Sane # will find KF5Sane
;;         TextWidgets
;; )
    (home-page "http://www.kde.org/applications/graphics/skanlite/")
    (synopsis "
An image scanning application
image scanner based on the KSane backend")
    (description "
Skanlite is an image scanning application that does nothing more than
scan and save images. It is based on libksane, a KDE interface for SANE
library to control flat scanners.

Skanlite is a small and simple scanner application for KDE 4 which allows easy
scanning of images with an attached scanner. Through the KSane backend, it can
access a wide variety of different scanner models.

Skanlite can be considered to be the replacement of Kooka.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

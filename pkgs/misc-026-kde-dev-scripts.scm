(define-public kde-dev-scripts
  (package
    (name "kde-dev-scripts")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kde-dev-scripts-" version ".tar.xz"))
      (sha256
       (base32 "0kiddn0wg90p98zgnpq3x2hcfw8xczn98nyd21zbkzz357v14pya"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `())
;; function(create_manpage)
;;     kdoctools_create_manpage(${ARGN})
;;   endfunction()
;;   find_package(KDE4)
;;   function(create_manpage)
;;     kde4_create_manpage(${ARGN})
;;   endfunction()
    (home-page "")
    (synopsis "
K Desktop Environment - Software Development Kit
")
    (description "
This package contains the scripts for KDE development which are contained in the
kdesdk module.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kdesdk-kioslaves
  (package
    (name "kdesdk-kioslaves")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kdesdk-kioslaves-" version ".tar.xz"))
      (sha256
       (base32 "17zqp43a1266616h3g6yccjmfgwni6lr8lz4rrvfda275vvwbshj"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; macro_optional_find_package(SVN)
;; macro_optional_find_package(Perl 5.10)
    (home-page "")
    (synopsis "
K Desktop Environment - Software Development Kit
")
    (description "
Software Development Kit for the K Desktop Environment.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

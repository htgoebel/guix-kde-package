(define-public ktp-accounts-kcm
  (package
    (name "ktp-accounts-kcm")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktp-accounts-kcm-" version ".tar.xz"))
      (sha256
       (base32 "0pr191i9fmz66wlv8krmy1pba7igd1qwa0akb4kdzkm4bx3z8wq3"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kcodecs" ,kcodecs)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; find_package (TelepathyQt5 REQUIRED)
;; find_package (KAccounts REQUIRED)
;; find_package (Intltool REQUIRED) #needed to generate service and provider files
;;  #for KAccountsMacros
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

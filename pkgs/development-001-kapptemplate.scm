(define-public kapptemplate
  (package
    (name "kapptemplate")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kapptemplate-" version ".tar.xz"))
      (sha256
       (base32 "036npdxyh9rx0aaiwvdjqrb39f8bqglqbl3iddb1vh7w9p1h158n"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
    (home-page "http://www.kde.org/")
    (synopsis "
Template for KDE Application Development
application template generator")
    (description "
KAppTemplate is a set of modular shell scripts that will create a
framework for any number of KDE application types. At its base
level, it handles creation of things like the automake/autoconf
framework, lsm files, RPM spec files, and po files. Then, there
are individual modules that allow you to create a skeleton KDE
application, a KPart application, a KPart plugin, or even convert
existing source code to the KDE framework.

KAppTemplate is a shell script that will create the necessary framework to
develop several types of applications, including applications based on the
KDE development platform.

It generates the build-system configuration and provides example code
for a simple application.

This package is part of the KDE Software Development Kit module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kde-runtime
  (package
    (name "kde-runtime")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kde-runtime-" version ".tar.xz"))
      (sha256
       (base32 "1sqp827l30adiqrp12djx3xk6mlz2lb46hmxnbnzv52mv2whcr3y"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 4.9.95 REQUIRED)
;; find_package(KDeclarative QUIET CONFIG)
;;  support from kdelibs"
;;                        URL "http://www.kde.org"
;;                        TYPE REQUIRED
;;                        PURPOSE "Required by corebindingsplugin (org.kde.plasma.core)"
;;                       )
;; macro_optional_find_package(NepomukCore)
;; macro_optional_find_package(SLP)
;;  implementation"
;;                        URL "http://www.openslp.org/"
;;                        TYPE OPTIONAL
;;                        PURPOSE "Provides SLP support in the network:/ kioslave."
;;                       )
;; find_package(LibAttica 0.1.4)
;; macro_optional_find_package(QCA2 2.0.0)
;; find_package(LibGcrypt 1.5.0 REQUIRED QUIET)
    (home-page "")
    (synopsis "
K Desktop Environment - Base Runtime
")
    (description "
KDE 4 application runtime components.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

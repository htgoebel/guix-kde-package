(define-public print-manager
  (package
    (name "print-manager")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/print-manager-" version ".tar.xz"))
      (sha256
       (base32 "1mjzqq7yhm1s4jbr6nhy1i1lm27134j6g7b04mmzk3hbgd8lkr99"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("plasma-framework" ,plasma-framework)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; find_package(CUPS "1.5" REQUIRED)
    (home-page "http://www.kde.org/")
    (synopsis "
Printing management for KDE
printer configuration and monitoring tools")
    (description "
Printer Applet is a system tray utility that shows current print jobs,
shows printer warnings and errors and shows when printers that have
been plugged in for the first time are being auto-configured by
hal-cups-utils.

This package provides a KDE configuration module and a Plasma widget for
installing and configuring printers, and monitoring printers, print jobs and
print queues.

This package is part of the KDE utilities module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

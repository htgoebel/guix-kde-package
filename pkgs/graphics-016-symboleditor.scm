(define-public symboleditor
  (package
    (name "symboleditor")
    (version "2.0.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/symboleditor/" version
                          "/src/SymbolEditor-" version ".tar.bz2"))
      (sha256
       (base32 "053f9n4pzv0k57icycwd50pplw6h34w8wvjjj49bxg79r8xfmwvw"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; find_package (Doxygen)
;; find_package (SharedMimeInfo)
;; qt5_wrap_ui (SymbolEditor_SRCS
;;     ui/EditorConfigPage.ui
;; )
;;     update_xdg_mimetypes (${XDG_MIME_INSTALL_DIR})
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public gwenview
  (package
    (name "gwenview")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/gwenview-" version ".tar.xz"))
       (sha256
        (base32 "069fblw9g9h6r9gy05nj2n93jpnsx610jncwl6403q01rwlbrgbr"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("baloo" ,baloo)
       ("kactivities" ,kactivities)
       ("kdelibs4support" ,kdelibs4support)
       ("kio" ,kio)
       ("phonon" ,phonon)
       ("qtbase" ,qtbase)
       ("qtsvg" ,qtsvg)
       ("qtx11extras" ,qtx11extras)))
;; find_package(JPEG)
;; find_package(PNG)
;; find_package(Exiv2)
;; find_package(KF5 Kipi)
;; find_package(LCMS2)
;; find_package(KF5 KDcraw)
;; find_package(X11)
    (home-page "http://www.kde.org/")
    (synopsis "Fast and easy to use image viewer for KDE")
    (description "Gwenview is an image viewer, ideal for browsing and
displaying a collection of images.  It is capable of showing images in a
full-screen slideshow view and making simple adjustments, such as rotating or
cropping images.

Additional features, such as image renaming, comparing, converting, and batch
processing, HTML gallery and others are provided by the KIPI image framework.

This package is part of the KDE graphics module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

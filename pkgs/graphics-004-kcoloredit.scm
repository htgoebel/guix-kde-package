;; Dated 2010
(define-public kcoloredit
  (package
    (name "kcoloredit")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/extragear/kcoloredit-" version
                           "-kde4.4.0.tar.bz2"))
       (sha256
        (base32 "0vlyyzy0l0qszbdgj3a50rm77p046x35pra37bmhv6f6ql020w9p"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package ( KDE4 REQUIRED )
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

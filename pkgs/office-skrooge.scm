(define-public skrooge
  (package
    (name "skrooge")
    (version "2.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/skrooge/skrooge-" version
                           ".tar.xz"))
       (sha256
        (base32 "03ayrrr7rrj1jl1qh3sgn56hbi44wn4ldgcj08b93mqw7wdvpglp"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)
      ("qttools" ,qttools)))
    (inputs
     `(("grantlee" ,grantlee)
       ("karchive" ,karchive)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kdesignerplugin" ,kdesignerplugin)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kitemviews" ,kitemviews)
       ("kjobwidgets" ,kjobwidgets)
       ("knewstuff" ,knewstuff)
       ("kparts" ,kparts)
       ("kwallet" ,kwallet)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("kxmlgui" ,kxmlgui)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtscript" ,qtscript)
       ("qtsvg" ,qtsvg)
       ("qtwebkit" ,qtwebkit)
       ("sqlite" ,sqlite)
       ("sqlcipher" ,sqlcipher)
       ))
    (home-page "http://skrooge.org/")
    (synopsis "Personal Finance Management Tool")
    (description " Skrooge allows you to manage your personal finances. It is
intended to be used by individuals who want to keep track of their incomes,
expenses and investments.  Its philosophy is to stay simple and intuitive.

Here is the list of Skrooge main features:
 * QIF, CSV, KMyMoney, Skrooge,  import/export
 * OFX, QFX, GnuCash, Grisbi, HomeBank import
 * Advanced Graphical Reports
 * Several tabs to help you organize your work
 * Infinite undo/redo
 * Instant filtering on operations and reports
 * Infinite categories levels
 * Mass update of operations
 * Scheduled operations
 * Track refund of your expenses
 * Automatically process operations based on search conditions
 * Multi currencies
 * Dashboard")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

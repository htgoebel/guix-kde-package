(define-public kdebugsettings
  (package
    (name "kdebugsettings")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kdebugsettings-" version ".tar.xz"))
      (sha256
       (base32 "0mkhklv4dynz23w8w9yh5qspdlfp3hi6fyiijyfwj358rqgbf3p5"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kitemviews" ,kitemviews)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

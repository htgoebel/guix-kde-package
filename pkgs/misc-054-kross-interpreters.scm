(define-public kross-interpreters
  (package
    (name "kross-interpreters")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kross-interpreters-" version ".tar.xz"))
      (sha256
       (base32 "0z1n42imbhsx0qmx479sklnihwvbd5l5pigbpbpm80rgwda2ib57"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kross" ,kross)
      ("qtbase" ,qtbase)))
;; find_package(KF5 5.11.0)
;; unset(PYTHON_INCLUDE_DIR CACHE)
;; unset(PYTHON_LIBRARY CACHE)
;; unset(PYTHON_LIBRARY_DEBUG CACHE)
;; unset(PYTHON_VERSION_MAJOR)
;; unset(PYTHON_VERSION_MINOR)
;; find_package(PythonLibs 2 EXACT REQUIRED)
;;")
    (home-page "")
    (synopsis "
Java kross interpreter
")
    (description "
Java kross interpreter
---
Python kross interpreter
---
Ruby kross interpreter

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

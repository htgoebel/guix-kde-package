(define-public kcolorchooser
  (package
    (name "kcolorchooser")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/kcolorchooser-" version ".tar.xz"))
       (sha256
        (base32 "13nkvxl3z2l3m6zs057ipxgqfgsw7gma1rs66maqhzl30k3hrcyb"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("ki18n" ,ki18n)
       ("kxmlgui" ,kxmlgui)
       ("qtbase" ,qtbase)))
    (home-page "https://projects.kde.org/projects/kde/kdegraphics/kcolorchooser")
    (synopsis "Color chooser and palette editor")
    (description "KColorChooser is a color palette tool, used to mix colors
and create custom color palettes.  Using the dropper, it can obtain the color
of any pixel on the screen.  A number of common color palettes are included,
such as the standard Web colors and the Oxygen color scheme.

This package is part of the KDE graphics module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

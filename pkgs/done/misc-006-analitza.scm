(define-public analitza
  (package
    (name "analitza")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/analitza-" version ".tar.xz"))
      (sha256
       (base32 "0ap3sf8bw9f58pzw3zy6w60apd4ccc47zs3v61x4kp1g81rn265w"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)))
;; #FindCurses seems to need that on the CI
;; find_package(OpenGL)
;;
;; find_package(Eigen3) # find and setup Eigen3 if available
    (home-page "http://edu.kde.org/")
    (synopsis "
Runtime library for analitza
common files for Analitza")
    (description "
The analitza library will let you add mathematical features to your program.
This pakage provide the runtime library for analitza.

Analitza is a library to parse and work with mathematical expressions. This
library is being used by KAlgebra and Cantor and may be used in other
programs.

This package contains a console interface for Analitza.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

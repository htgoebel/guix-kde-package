(define-public krdc
  (package
    (name "krdc")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/krdc-" version ".tar.xz"))
      (sha256
       (base32 "0visx3371ym78n9aqkln2akvvf0rphyxg5hxvc2981pgpdry20zq"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kbookmarks" ,kbookmarks)
      ("kcmutils" ,kcmutils)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kdnssd" ,kdnssd)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)))
;; find_package(LibVNCServer)
;; FIND_PROGRAM(FREERDP_EXECUTABLE xfreerdp)
;;  Implementation"
;;         URL "http://www.freerdp.com"
;;         PURPOSE "Needed for RDP support in KRDC (at runtime)"
;;                                      )
    (home-page "http://www.kde.org/")
    (synopsis "
KDE Remote Desktop Client
Remote Desktop Connection client")
    (description "
KDE Remote Desktop Client is a client application that allows you to view or
even control the desktop session on another machine that is running a compatible
server. VNC and RDP are supported.

The KDE Remote Desktop Connection client can view and control a desktop
session running on another system.  It can connect to Windows Terminal Servers
using RDP and many other platforms using VNC/RFB.

This package is part of the KDE networking module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

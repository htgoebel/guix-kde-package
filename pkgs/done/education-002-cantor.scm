(define-public cantor
  (package
    (name "cantor")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/cantor-" version ".tar.xz"))
      (sha256
       (base32 "1nzg9sfnv8afpa84x51whbw1qh8gfwqnkr5zyai7817kkc97g1l8"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("kparts" ,kparts)
      ("kpty" ,kpty)
      ("ktexteditor" ,ktexteditor)
      ("ktextwidgets" ,ktextwidgets)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)
      ("qtxmlpatterns" ,qtxmlpatterns)))
;; find_package(LibSpectre)
    (home-page "https://kde.org/applications/education/org.kde.cantor")
    (synopsis "
KDE Interface for doing Mathematics and Scientific Computing
interface for mathematical applications")
    (description "
Cantor is a KDE Application aimed to provide a nice interface
for doing Mathematics and Scientific Computing. It doesn't implement
its own Computation Logic, but instead is built around different
Backends.

Cantor is an application to allow you to you use your favorite mathematical
applications from within an elegant worksheet interface. It provides dialogs
to assist with common tasks and allows you to share your worksheets
with others.

Cantor supports various mathematical applications as backends (provided in
external packages):
 * KAlgebra (cantor-backend-kalgebra)
 * Maxima Computer Algebra System (cantor-backend-maxima)
 * R Project for Statistical Computing (cantor-backend-r)
 * Sage Mathematics Software (cantor-backend-sage)
 * Octave (cantor-backend-octave)
 * Python (cantor-backend-python2)
 * Scilab (cantor-backend-scilab)
 * Qalculate! (cantor-backend-qalculate)
 * Lua (cantor-backend-lua)

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

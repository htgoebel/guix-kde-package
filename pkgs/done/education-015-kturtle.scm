(define-public kturtle
  (package
    (name "kturtle")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kturtle-" version ".tar.xz"))
      (sha256
       (base32 "1mpdwb6999nar16mpha30cf4qzmpbsdha44aw77gn301v215rwj3"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("ktextwidgets" ,ktextwidgets)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
    (home-page "https://kde.org/applications/education/org.kde.kturtle")
    (synopsis "
An educational programming environment
educational programming environment")
    (description "
KTurtle is an educational programming environment for the KDE Desktop.
KTurtle aims to make programming as easy and touchable as possible, and
therefore can be used to teach kids the basics of math, geometry
and... programming.

KTurtle is an educational programming environment which uses the TurtleScript
programming language (inspired by Logo) to make programming as easy and
accessible as possible.

The user issues TurtleScript language commands to control the "turtle", which
draws on the canvas, making KTurtle suitable for teaching elementary
mathematics, geometry and programming.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

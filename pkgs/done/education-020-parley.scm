(define-public parley
  (package
    (name "parley")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/parley-" version ".tar.xz"))
      (sha256
       (base32 "0vmbn8188brp4bysyzmkgsa8nnn9zdqb7q6x3mi1xg7ralzfjw71"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("kross" ,kross)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtmultimedia" ,qtmultimedia)
      ("qtsvg" ,qtsvg)
      ("sonnet" ,sonnet)))
;; find_package(KF5 #to produce the docbook)
;; find_package(LibKEduVocDocument)
;; find_package(LibXslt)
;; find_package(LibXml2)
    (home-page "https://kde.org/applications/education/org.kde.parley")
    (synopsis "
KDE Vocabulary training application
vocabulary trainer")
    (description "
Parley is a program to help you memorize things.

Parley supports many language specific features but can be used for other
learning tasks just as well. It uses the spaced repetition learning method,
also known as flash cards.

Parley is a utility to help train vocabulary when learning a foreign language.
It is intended as a replacement for flash cards.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public libkmahjongg
  (package
    (name "libkmahjongg")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkmahjongg-" version ".tar.xz"))
      (sha256
       (base32 "0fhz7jp4wcvjcicyiwc4qq4vviwagfjdy6n5bhv7bmlccq4xfk5x"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
;; target_include_directories(KF5KMahjongglib INTERFACE "$<INSTALL_INTERFACE:${KF5_INCLUDE_INSTALL_DIR}/KF5KMahjongg>" INTERFACE "$<INSTALL_INTERFACE:${KF5_INCLUDE_INSTALL_DIR}>")
;; set_target_properties(KF5KMahjongglib PROPERTIES VERSION ${KMAHJONGGLIB_VERSION}
;;                                                  EXPORT_NAME KF5KMahjongglib)
;; export(TARGETS KF5KMahjongglib
;;     FILE "${PROJECT_BINARY_DIR}/KF5KMahjonggLibraryDepends.cmake")
    (home-page "")
    (synopsis "
Runtime library for kmahjongg
shared library for kmahjongg and kshisen")
    (description "
Runtime library for kmahjongg.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public khangman
  (package
    (name "khangman")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/khangman-" version ".tar.xz"))
      (sha256
       (base32 "03ffigr9a6n3aj1a7lxcw9wgf1pafdjwqjnwnny2ric5vn6lpq1z"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtsvg" ,qtsvg)))
;; find_package(LibKEduVocDocument REQUIRED)
    (home-page "https://kde.org/applications/education/org.kde.khangman")
    (synopsis "
Classical hangman game
Hangman word puzzle")
    (description "
KHangman is the classical hangman game. The child should guess a word
letter by letter. At each miss, the picture of a hangman appears. After
10 tries, if the word is not guessed, the game is over and the answer
is displayed.

KHangMan is the well-known Hangman game, aimed towards children aged 6 and
above.

It picks a random word which the player must reveal by guessing if it contains
certain letters.  As the player guesses letters, the word is gradually
revealed, but 10 wrong guesses will end the game.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

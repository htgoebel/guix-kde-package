(define-public kimap
  (package
    (name "kimap")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kimap-" version ".tar.xz"))
      (sha256
       (base32 "1jlva17hy500jpb5mg6m3vrcy6mqikcy8m1pgy68d2ip0m93rb5f"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)))
;; find_package(KF5 Mime ${KMIME_LIBS_VERSION})
;; find_package(Sasl2)
    (home-page "")
    (synopsis "
KDE 4 core library
library for handling IMAP data")
    (description "
This library provides an API for IMAP support.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

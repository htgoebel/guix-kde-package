(define-public kontactinterface
  (package
    (name "kontactinterface")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kontactinterface-" version ".tar.xz"))
      (sha256
       (base32 "0n5hvx3xp01krwwd2szgh1s6nn5spfns1ivc36i1gdbhkf871k98"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kparts" ,kparts)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)))
;; find_package(X11)
    (home-page "")
    (synopsis "
Kontact Interface library
Kontact interface library")
    (description "
Kontact Interface library.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

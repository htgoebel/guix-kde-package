(define-public kmailtransport
  (package
    (name "kmailtransport")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/kmailtransport-" version ".tar.xz"))
       (sha256
        (base32 "1dkw7niymhnf0jncflvqyldw28c9j0q6j598flb5ksds0n30iasy"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("akonadi" ,akonadi)
       ("akonadi-mime" ,akonad-mimei)
       ("kcmutils" ,kcmutils)
       ("kconfigwidgets" ,kconfigwidgets)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("kmime" ,kmime)
       ("kwallet" ,kwallet)
       ("sasl2" ,sasl2)))
    (home-page "")
    (synopsis "Mail transport service library")
    (description " This library provides an API and support code for managing
mail transport.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

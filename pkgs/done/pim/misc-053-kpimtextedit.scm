(define-public kpimtextedit
  (package
    (name "kpimtextedit")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kpimtextedit-" version ".tar.xz"))
      (sha256
       (base32 "06iz9l8z708kdivzibqkgdrbvw7kfd2lr2grf3v9l6gfl3qs1kbw"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdesignerplugin" ,kdesignerplugin)
      ("kemoticons" ,kemoticons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("ksyntaxhighlighting" ,ksyntaxhighlighting)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("sonnet" ,sonnet)))
;; find_package(Grantlee5 "5.1" CONFIG REQUIRED)
;;
;;     find_package(Qt5 TextToSpeech)
    (home-page "")
    (synopsis "
Kdepimlibs 4 core library
library that provides a textedit with PIM-specific features")
    (description "
A library for PIM-specific text editing utilities.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kdepim
  (package
    (name "kdepim")
    (version "16.08.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/kdepim-" version ".tar.xz"))
       (sha256
        (base32 "1l0rvi33i9hzr9c3nqzbr3cnz046ccf7s3v54mdmxfdk5x0ynkms"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)
       ("qttools" ,qttools)))
    (inputs
     `(("akonadi" ,akonadi)
       ("akonadi-calendar" ,akonadi-calendar)
       ("akonadi-contact" ,akonadi-contact)
       ("akonadi-mime" ,akonadi-mime)
       ("akonadi-notes" ,akonadi-notes)
       ("akonadi-search" ,akonadi-search)
       ("boost", boost)

       ("karchive" ,karchive)
       ("kauth" ,kauth)
       ("kcmutils" ,kcmutils)
       ("kcodecs" ,kcodecs)
       ("kconfig" ,kconfig)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ;;("kdelibs4support" ,kdelibs4support)
       ("kdnssd" ,kdnssd)
       ("kglobalaccel" ,kglobalaccel)
       ("knewstuff" ,knewstuff)
       ("knotifyconfig" ,knotifyconfig)
       ("kservice" ,kservice)
       ("ktexteditor" ,ktexteditor)
       ("kwallet" ,kwallet)
       ("kxmlrpcclient" ,kxmlrpcclient)
       ("phonon" ,phonon)
       ("qtbase" ,qtbase)
       ("qtx11extras" ,qtx11extras)
       ("sonnet" ,sonnet)
       ("grantlee" ,grantlee)

       ("kalarmcalendar" ,kalarmcalendar)
       ("kblog" ,kblog)
       ("kcalendarcore" ,kcalendarcore)
       ("kcalendarsupport" ,kcalendarsupport)
       ("kcalrutils" ,kcalutils)
       ("kcontacts" ,kcontacts)
       ("keventviews" ,keventviews)
       ("kfollowupreminder" ,kfollowupreminder)
       ("kgpgmepp" ,kgpgmepp)
       ("kgrantleetheme" ,kgrantleetheme)
       ("kgravatar" ,kgravatar)
       ("kholidays" ,kholidays)
       ("kidentitymanagement" ,kidentitymanagement)
       ("kimap" ,kimap)
       ("kincidenceeditor" ,kincidenceeditor)
       ("kkaddressbookgrantlee" ,kkaddressbookgrantlee)
       ("kkdepimdbusinterfaces" ,kkdepimdbusinterfaces)
       ("kkdgantt2" ,kkdgantt2)
       ("kkontactinterface" ,kkontactinterface)
       ("kldap" ,kldap)
       ("libkdepim" ,libkdepim)
       ("libkleo" ,libkleo)
       ("libksieve" ,libksieve)
       ("kmailcommon" ,kmailcommon)
       ("kmailimporter" ,kmailimporter)
       ("kmailtransport" ,kmailtransport)
       ;;("kmailtransportdbusservice" ,kmailtransportdbusservice)
       ("kmbox" ,kmbox)
       ("kmessagecomposer" ,kmessagecomposer)
       ("kmessagecore" ,kmessagecore)
       ("kmessagelist" ,kmessagelist)
       ("kmessageviewer" ,kmessageviewer)
       ("kmime" ,kmime)
       ("kpimcommon" ,kpimcommon)
       ("kpimtextedit" ,kpimtextedit)
       ("ksendlater" ,ksendlater)
       ("ksyndication" ,ksyndication)
       ("ktemplateparser" ,ktemplateparser)
       ("ktnef" ,ktnef)
       ("kwebengineviewer" ,kwebengineviewer)
;;         find_package(X11)
       ("kgapi" ,kgapi)
       ("qtwebengine" ,qtwebengine)
       ("qtwebenginewidgets" ,qtwebenginewidgets)
       ))
    (home-page "http://pim.kde.org/")
    (synopsis "KDE PIM library")
    (description "
Information Management applications for the K Desktop Environment.
	- kaddressbook: The KDE addressbook application.
	- korganizer: a calendar-of-events and todo-list manager
	- kalarm: gui for setting up personal alarm/reminder messages
	- kalarmd: personal alarm/reminder messages daemon, shared by korganizer
	  and kalarm.
	- kaplan: A shell for the PIM apps, (still experimental).
	- ktimetracker: Time tracker.
	- kfile-plugins: vCard KFIleItem plugin.
	- knotes: yellow notes application
	- konsolecalendar: Command line tool for accessing calendar files.
	- kmail: universal mail client
	- kmailcvt: a tool to convert addressbooks to kmail format

KDE (the K Desktop Environment) is a powerful Open Source graphical
desktop environment for Unix workstations. It combines ease of use,
contemporary functionality, and outstanding graphical design with the
technological superiority of the Unix operating system.

This metapackage includes a collection of Personal Information Management
(PIM) desktop applications provided with the official release of KDE.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

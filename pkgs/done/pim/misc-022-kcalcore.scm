(define-public kcalcore
  (package
    (name "kcalcore")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kcalcore-" version ".tar.xz"))
      (sha256
       (base32 "09i43vs6jpjmmk52df6pzclh0jn8shn3wfnaivw2wlirwa60d901"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kdelibs4support" ,kdelibs4support)))
;; find_package(LibIcal ${LibIcal_MIN_VERSION})
    (home-page "")
    (synopsis "
KDE 4 core library
library for handling calendar data")
    (description "
An API for the iCalendar and vCalendar formats.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

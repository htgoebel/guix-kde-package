(define-public kdepim-apps-libs
  (package
    (name "kdepim-apps-libs")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/kdepim-apps-libs-" version ".tar.xz"))
       (sha256
        (base32 "1q4ksp377piqxbs843nxafzssz80ayjii90iz86r2z2rd3lyrjzw"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("akonadi" ,akonadi)
       ("akonadi-contact" ,akonadi-contact)
       ("grantlee" ,grantlee)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcontacts" ,kcontacts)
       ("kcoreaddons" ,kcoreaddons)
       ("kgrantleetheme" ,kgrantleetheme)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("klibkleo" ,klibkleo)
       ("kpimcommon" ,kpimcommon)
       ("kservice" ,kservice)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("prison" ,prison)
       ("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "KDE PIM mail related libraries, data files")
    (description "")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

;; Sources way, this si part of kdepim, so why there is a separate package?
(define-public ktnef
  (package
    (name "ktnef")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ktnef-" version ".tar.xz"))
      (sha256
       (base32 "0v38h7bwjwab1656w3c2h1by5925f616idnvflgp123v1cs6jy1n"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kdelibs4support" ,kdelibs4support)))
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
    (home-page "")
    (synopsis "
TNEF Viewer/Extractor
Viewer for mail attachments using TNEF format")
    (description "
A TNEF Viewer/Extractor.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

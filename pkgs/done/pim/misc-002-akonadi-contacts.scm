(define-public akonadi-contacts
  (package
    (name "akonadi-contacts")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/akonadi-contacts-" version ".tar.xz"))
       (sha256
        (base32 "1205g4z5rz02j8swrmhncm06d8m727z63d526djygz5svz15sd2l"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcodecs" ,kcodecs)
       ("kcompletion" ,kcompletion)
       ("kdbusaddons" ,kdbusaddons)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("ktextwidgets" ,ktextwidgets)
       ("qtwebengine" ,qtwebengine)
       ("qtwebenginewidgets" ,qtwebenginewidgets)
       ("grantlee" ,grantlee)
       ("kcontacts" ,kcontacts)
       ("kmime" ,kmime)
       ("akonadi-mime" ,akonadi-mime)
       ("akonadi" ,akonadi)
       ("prison" ,prison)
       ("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "Akonadi contacts access library")
    (description "A client access library for using the Akonadi PIM data
server.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

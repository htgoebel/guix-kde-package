(define-public kcalutils
  (package
    (name "kcalutils")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kcalutils-" version ".tar.xz"))
      (sha256
       (base32 "0449029fa1w496fmb81csb5amk801n11ish450drqw34lp9qla4n"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)))
;; find_package(Grantlee5 "5.1" CONFIG REQUIRED)
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGER_LIB_VERSION})
    (home-page "")
    (synopsis "
KDE 4 core library
library with utility functions for the handling of calendar data")
    (description "
This library provides a utility and user interface functions for accessing
calendar data using the kcalcore API.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

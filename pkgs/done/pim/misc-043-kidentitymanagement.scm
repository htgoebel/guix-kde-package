(define-public kidentitymanagement
  (package
    (name "kidentitymanagement")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kidentitymanagement-" version ".tar.xz"))
      (sha256
       (base32 "1b23cwdhc7rv260kmn4akff3jfa21hk49p0kp8p0mf6nw6qahbvp"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kemoticons" ,kemoticons)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("ktextwidgets" ,ktextwidgets)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; find_package(KF5 PimTextEdit ${PIMTEXTEDIT_LIB_VERSION})
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

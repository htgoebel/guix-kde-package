(define-public akonadi-notes
  (package
    (name "akonadi-notes")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/akonadi-notes-" version ".tar.xz"))
      (sha256
       (base32 "0405nkz9ri9qlclgvwycvdx1gsms2pm1fn6xhvyrj2v4s8brb3r7"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; find_package(KF5 Mime ${KMIMELIB_VERSION})
;; find_package(KF5 Akonadi)
    (home-page "")
    (synopsis "
KDE 4 core library
Akonadi notes access library")
    (description "
A client access library for using the Akonadi PIM data server.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

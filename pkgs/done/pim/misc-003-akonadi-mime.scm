(define-public akonadi-mime
  (package
    (name "akonadi-mime")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/akonadi-mime-" version ".tar.xz"))
       (sha256
        (base32 "1xay3rlygdhf9lp356wjb92wnmxdpaxgm3prxnfxcdg5ql6xcg07"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("akonadi" ,akonadi)
       ("kconfig" ,kconfig)
       ("kdbusaddons" ,kdbusaddons)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("kitemmodels" ,kitemmodels)
       ("kmime" ,kmime)
       ("kxmlgui" ,kxmlgui)
       ("libxslt" ,libxslt)
       ("shared-mime-info" ,shared-mime-info)))
    (home-page "")
    (synopsis "Akonadi MIME handling library")
    (description "A client access library for using the Akonadi PIM data
server.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

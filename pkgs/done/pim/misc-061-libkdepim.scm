(define-public libkdepim
  (package
    (name "libkdepim")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkdepim-" version ".tar.xz"))
      (sha256
       (base32 "07afpxhvxpf50h10vg6vla543n4rvy1grldjj4aygwh1fgl5snm0"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
    (inputs
     `(("kcmutils" ,kcmutils)
       ("kcodecs" ,kcodecs)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kdesignerplugin" ,kdesignerplugin)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kitemviews" ,kitemviews)
       ("kjobwidgets" ,kjobwidgets)
       ("kwallet" ,kwallet)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("qtbase" ,qtbase)))
;; find_package(KF5 Akonadi)
;; find_package(KF5 AkonadiContact ${AKONADICONTACT_LIB_VERSION})
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;; find_package(KF5 Ldap ${KLDAP_LIB_VERSION})
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiSearch ${AKONADISEARCH_LIB_VERSION})
    (home-page "")
    (synopsis "
KDEPIM 4 library
KDE PIM library")
    (description "
KDE PIM runtime libraries.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

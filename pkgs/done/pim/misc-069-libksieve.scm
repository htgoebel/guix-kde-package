(define-public libksieve
  (package
    (name "libksieve")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/libksieve-" version ".tar.xz"))
       (sha256
        (base32 "1riqsfl3x4vpwqv7398gj86hnwfzqwfj7d69wsxk3r02jp3xd9i2"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("akonadi" ,akonadi)
       ("cyrus-sasl2" ,cyrus-sasl2)
       ("karchive" ,karchive)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kidentitymanagement" ,kidentitymanagement)
       ("kio" ,kio)
       ("kmailtransport" ,kmailtransport)
       ("kmime" ,kmime)
       ("knewstuff" ,knewstuff)
       ("kpimtextedit" ,kpimtextedit)
       ("kwindowsystem" ,kwindowsystem)
       ("libkdepim" ,libkdepim)
       ("pimcommon" ,pimcommon)
       ("qtbase" ,qtbase)
       ("qtwebengine" ,qtwebengine)))
    (home-page "")
    (synopsis "KDE Sieve library")
    (description "Sieve is a language that can be used filter emails.  KSieve
is a Sieve parser and interpreter library for KDE.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public mailimporter
  (package
    (name "mailimporter")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/mailimporter-" version ".tar.xz"))
      (sha256
       (base32 "08yj4i6jy08hk62mxw299sh2n5pknzxanmzr96n6lf8g1jcf2l21"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("karchive" ,karchive)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; find_package(KF5 Akonadi)
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiMime ${AKONADIMIME_LIB_VERSION})
;; find_package(KF5 Libkdepim ${KDEPIM_LIB_VERSION})
    (home-page "")
    (synopsis "
Mail Importer Library
mailimporter library")
    (description "
Mail Importer Library

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

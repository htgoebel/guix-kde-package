(define-public kalarmcal
  (package
    (name "kalarmcal")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kalarmcal-" version ".tar.xz"))
      (sha256
       (base32 "064sihcsbdi1w6viv5gq6sw2m8r7x3nn1hl2aji1109pf5913vbr"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kdelibs4support" ,kdelibs4support)))
;; find_package(KF5 Akonadi ${AKONADI_LIB_VERSION})
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGER_LIB_VERSION})
;; find_package(KF5 Holidays ${HOLIDAY_LIB_VERSION})
    (home-page "")
    (synopsis "
KDE 4 core library
library for handling kalarm calendar data")
    (description "
This library provides an API for KAlarm alarms.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public akonadi-calendar
  (package
    (name "akonadi-calendar")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/akonadi-calendar-" version ".tar.xz"))
      (sha256
       (base32 "0kv636a8x75fcagw8hjnrwnxzvqnnm42hfw68ywfics0pn0pl50g"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kio" ,kio)
      ("kwallet" ,kwallet)))
;; find_package(KF5 MailTransport ${MAILTRANSPORT_LIB_VERSION})
;; find_package(KF5 Contacts ${KCONTACTS_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
;; find_package(KF5 CalendarCore ${CALENDARCORE_LIB_VERSION})
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 Akonadi ${AKONADI_LIB_VERSION})
;; find_package(KF5 AkonadiContact ${AKONADICONTACT_LIB_VERSION})
    (home-page "")
    (synopsis "
KDE 4 core library
library providing calendar helpers for Akonadi items")
    (description "
This library manages calendar specific actions for collection and item views.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

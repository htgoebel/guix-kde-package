(define-public libkleo
  (package
    (name "libkleo")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkleo-" version ".tar.xz"))
      (sha256
       (base32 "0rzp2bxqij9fkdsghskd2f05vgcybdc9l7wdrjqhhcngi8qxl0nn"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("boost" ,boost)
       ("gpgmepp" ,gpgmepp)
       ("kcodecs" ,kcodecs)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kcoreaddons" ,kcoreaddons)
       ("ki18n" ,ki18n)
       ("kitemmodels" ,kitemmodels)
       ("kpimtextedit" ,kpimtextedit)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "KDE PIM cryptographic library")
    (description "KDE keymanagement library.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

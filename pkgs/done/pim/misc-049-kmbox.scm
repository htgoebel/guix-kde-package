(define-public kmbox
  (package
    (name "kmbox")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kmbox-" version ".tar.xz"))
      (sha256
       (base32 "0khvf4kqf9i425fjczl1miqsz0pxbyryxy32bf9knr3k5kmwbn24"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KF5 Mime)
    (home-page "")
    (synopsis "
KDE 4 core library
library for handling mbox mailboxes")
    (description "
A library for accessing mail storages in MBox format.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

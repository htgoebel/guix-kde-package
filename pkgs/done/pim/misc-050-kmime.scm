(define-public kmime
  (package
    (name "kmime")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/kmime-" version ".tar.xz"))
      (sha256
       (base32 "0gzbh0g075ml5x0qy4zd1cg1qygdsnssl5ahk9pcgc0fik4p9j20"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcodecs" ,kcodecs)
       ("ki18n" ,ki18n)))
    (home-page "")
    (synopsis "Library for handling MIME data")
    (description "A library for MIME handling.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public akonadi-search
  (package
    (name "akonadi-search")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/akonadi-search-" version ".tar.xz"))
       (sha256
        (base32 "0wf22rmfz471iw6zxl7yz279fkks2pj5jfw4bs5185kr3xw2q7zp"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("akonadi" ,akonadi)
       ;("akonadi-mime" ,akonadi-mime)
       ("kcontacts" ,kcontakts)
       ("kcalendarcore" ,kcalendarcore)
       ("kcmutils" ,kcmutils)
       ("kconfig" ,kconfig)
       ("kcrash" ,kcrash)
       ("ki18n" ,ki18n)
       ("kmime" ,kmime)
       ("krunner" ,krunner)
       ("qtbase" ,qtbase)
       ("xapian" ,xapian)))
   (home-page "")
    (synopsis "

Akonadi search library - runtime binaries")
    (description "")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kblog
  (package
    (name "kblog")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/kblog-" version ".tar.xz"))
       (sha256
        (base32 "0q1qswg7dgy0jvk9kaz6cps6sfddsmv0lp5r3nhk751l497bbl3x"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kcalendarcore" ,kcalendarcore)
       ("kio" ,kio)
       ("ksyndication" ,ksyndication)
       ("kxmlrpcclient" ,kxmlrpcclient)
    ))
    (home-page "")
    (synopsis "Client-side support library for web application remote blogging
APIs")
    (description "This library provides client-side support for web
application remote blogging APIs.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kldap
  (package
    (name "kldap")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kldap-" version ".tar.xz"))
      (sha256
       (base32 "0a7pm9dzcvyyznqs4apwdl6dpg87qhxcr3a06grjwxhqvfdl52na"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcompletion" ,kcompletion)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)))
;; find_package(KF5 Mbox ${MBOXLIB_VERSION})
;; find_package(Ldap)
;; find_package(Sasl2)
    (home-page "")
    (synopsis "
KDE 4 core library
library for accessing LDAP")
    (description "
This library provides an API for IMAP support.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

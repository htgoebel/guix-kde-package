(define-public konversation
  (package
    (name "konversation")
    (version "1.6.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/konversation/" version
                          "/src/konversation-" version ".tar.xz"))
      (sha256
       (base32 "1798sslwz7a3h1v524ra33p0j5iqvcg0v1insyvb5qp4kv11slmn"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kbookmarks" ,kbookmarks)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kemoticons" ,kemoticons)
      ("kglobalaccel" ,kglobalaccel)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kidletime" ,kidletime)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kparts" ,kparts)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("solid" ,solid)
      ("sonnet" ,sonnet)))
;; find_package(Qca-qt5 2.1.0)
    (home-page "http://konversation.kde.org/")
    (synopsis "
A user friendly IRC Client for KDE
user friendly Internet Relay Chat (IRC) client for KDE")
    (description "
Konversation is a graphical Internet Relay Chat client (IRC)
with KDE support.

Features:

* Standard IRC features
* SSL server support
* Bookmarking support
* Easy to use graphical user interface
* Multiple servers and channels in one single window
* DCC file transfer
* Multiple identities for different servers
* Text decorations and colors
* OnScreen Display for notifications
* Automatic UTF-8 detection
* Per channel encoding support
* Theme support for nick icons
* Highly configurable

Konversation is a client for the Internet Relay Chat (IRC) protocol.
It is easy to use and well-suited for novice IRC users, but novice
and experienced users alike will appreciate its many features:

      * Standard IRC features
      * Easy to use graphical interface
      * Multiple server and channel tabs in a single window
      * IRC color support
      * Pattern-based message highlighting and OnScreen Display
      * Multiple identities for different servers
      * Multi-language scripting support (with DCOP)
      * Customizable command aliases
      * NickServ-aware log-on (for registered nicknames)
      * Smart logging
      * Traditional or enhanced-shell-style nick completion
      * DCC file transfer with resume support")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kaffeine
  (package
    (name "kaffeine")
    (version "2.0.5")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/kaffeine/" version
                           "/src/kaffeine-" version "-1.tar.xz"))
       (sha256
        (base32 "132s0ppv4nijhl86mz65n3x25smy3zsk2ly7z638npj7x4qhjqg1"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kxmlgui" ,kxmlgui)
       ("qtbase" ,qtbase)
       ("qtx11extras" ,qtx11extras)
       ("libdvbpsi" ,libdvbpsi) ;;;? Libdvbv5
       ("solid" ,solid)
       ("vlc" ,vlc)))
    (home-page "http://kaffeine.kde.org")
    (synopsis "versatile media player for KDE")
    (description "Kaffeine is a media player for KDE.  While it supports
multiple Phonon backends, its default backend is Xine, giving Kaffeine a wide
variety of supported media types and letting Kaffeine access CDs, DVDs, and
network streams easily.

Kaffeine can keep track of multiple playlists simultaneously, and supports
autoloading of subtitle files for use while playing video.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public dolphin
  (package
    (name "dolphin")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/dolphin-" version ".tar.xz"))
       (sha256
        (base32 "0y35ljbjib4pvyngdgbq1yx3rfmy94crpa7v1zzwfav94lm3kwb2"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("baloo" ,baloo)
       ("kactivities" ,kactivities)
       ("kbookmarks" ,kbookmarks)
       ("kcmutils" ,kcmutils)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("kdelibs4support" ,kdelibs4support)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kinit" ,kinit)
       ("kio" ,kio)
       ("knewstuff" ,knewstuff)
       ("knotifications" ,knotifications)
       ("kparts" ,kparts)
       ("ktextwidgets" ,ktextwidgets)
       ("kwindowsystem" ,kwindowsystem)
       ("phonon" ,phonon)
       ("qtbase" ,qtbase)
       ("solid" ,solid)))
    (home-page "")
    (synopsis "File manager for KDE")
    (description "Dolphin is a file manager for KDE focusing on usability.
The main features of Dolphin are:
@itemize
@item Navigation bar for URLs, which allows to navigate quickly
      through the file hierarchy.
@itemize View properties are remembered for each folder.
@itemize Split of views is supported.
@itemize Network transparency.
@itemize Undo/redo functionality.
@itemize Renaming of a variable number of selected items in one step.
@end itemize")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kxstitch
  (package
    (name "kxstitch")
    (version "1.3.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kxstitch/" version
                          "/src/kxstitch-" version ".tar.bz2"))
      (sha256
       (base32 "05kzf17clih9wximdr05qw3sg0d960si8ml13jschwf1szqdqpv7"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package (KDE4 REQUIRED)
;; find_package (ImageMagick COMPONENTS MagickCore Magick++ REQUIRED)
;; find_package (X11)
;; find_package (Doxygen)
;; find_package (SharedMimeInfo)
;;     set_target_properties (kxstitch PROPERTIES LINK_FLAGS -pg)
;;     update_xdg_mimetypes (${XDG_MIME_INSTALL_DIR})
;; find_package(Msgfmt REQUIRED)
;; find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis "
Program to create cross stitch patterns
Cross stitch pattern editor")
    (description "
KXStitch is a program that lets you create cross stitch patterns and charts.
---
KXStitch is a program that lets you create cross stitch patterns and charts.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public akregator
  (package
    (name "akregator")
    (version "1.6")
    (source
     (origin
      (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/akregator-" version ".tar.xz"))
      (sha256
       (base32 "03ri4y1wzyqlixnhczsls5gmy7jzzm67bb5gz8bav51ngc32fxca"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kemoticons" ,kemoticons)
      ("kglobalaccel" ,kglobalaccel)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("ktextwidgets" ,ktextwidgets)
      ("kwallet" ,kwallet)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("sonnet" ,sonnet)))
;; find_package(Qca-qt5 REQUIRED)
;; find_package(QtOAuth 2.0.1 REQUIRED)
    (home-page "http://choqok.gnufolks.org")
    (synopsis "
KDE Micro-Blogging Client
KDE micro-blogging client")
    (description "
Choqok is a Free/Open Source micro-blogging client for K Desktop

Choqok is a fast, efficient and simple to use micro-blogging client for KDE.
It currently supports the twitter.com and identi.ca microblogging services.

Other notable features include:
   * Support for user + friends time-lines.
   * Support for @Reply time-lines.
   * Support for sending and receiving direct messages.
   * Twitpic.com integration.
   * The ability to use multiple accounts simultaneously.
   * Support for search APIs for all services.
   * KWallet integration.
   * Support for automatic shortening urls with more than 30 characters.
   * Support for configuring status lists appearance.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

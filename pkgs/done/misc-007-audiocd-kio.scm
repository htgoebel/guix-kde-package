(define-public audiocd-kio
  (package
    (name "audiocd-kio")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/audiocd-kio-" version ".tar.xz"))
      (sha256
       (base32 "0bmmi2rxx368nss8ciwj32dspc1ailc0shm06ynmhw3slrplnx1y"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("kio" ,kio)))
;; # handled by release scripts
;; find_package(KF5 Cddb)
;; find_package(KF5 CompactDisc)
;; find_package(Cdparanoia REQUIRED)
;; check_struct_has_member("struct cdrom_drive" "ioctl_device_name" "cdda_interface.h" CDDA_IOCTL_DEVICE_EXISTS)
    (home-page "http://www.kde.org/")
    (synopsis "

transparent audio CD access for applications using the KDE Platform")
    (description "


This package includes the audiocd KIO plugin, which allows applications using
the KDE Platform to read audio from CDs and automatically convert it into other
formats.

This package is part of the KDE multimedia module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public grantleetheme
  (package
    (name "grantleetheme")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/grantleetheme-" version ".tar.xz"))
       (sha256
        (base32 "19mka62p75qnv6r9ib70y25jjj3bpziz0xqihfkb6jvafrgy8p8k"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("grantlee" ,grantlee)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("knewstuff" ,knewstuff)
       ("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "Library providing grantlee theme support")
    (description "This library provides grantlee theme support.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

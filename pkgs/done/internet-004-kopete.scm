(define-public kopete
  (package
    (name "kopete")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kopete-" version ".tar.xz"))
      (sha256
       (base32 "1dkn4d13knf0lcj4241fmjcj51ypg9frzsf0pl02mr08rd2rxgk1"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; find_package(KdepimLibs REQUIRED)
;; find_package(QImageBlitz REQUIRED)
;; kde4_no_enable_final(kopete)
;; macro_optional_find_package(Boost)
;; macro_optional_find_package(Expat)
;; macro_optional_find_package(GIF)
;; macro_optional_find_package(GLIB2)
;; macro_optional_find_package(IDN)
;;  implementation" "http://www.gnu.org/software/libidn/" FALSE "" "Required for the Jabber protocol")
;; macro_optional_find_package(Jasper)
;; macro_optional_find_package(JsonCpp)
;; macro_optional_find_package(Kleopatra)
;; " "http://kde.org/" FALSE "" "Required for the Cryptography plugin")
;; macro_optional_find_package(Libgadu)
;; macro_optional_find_package(LibMeanwhile)
;; macro_optional_find_package(Libmsn)
;; macro_optional_find_package(LiboRTP)
;; macro_optional_find_package(LibOTR)
;; macro_optional_find_package(LibV4L2)
;; macro_optional_find_package(LibXml2)
;; macro_optional_find_package(LibXslt)
;; macro_optional_find_package(Mediastreamer)
;; macro_optional_find_package(OpenSSL)
;; macro_optional_find_package(QCA2)
;; macro_optional_find_package(QJSON)
;; macro_optional_find_package(QGpgme)
;; " "http://www.kde.org/" FALSE "" "Required for the Cryptography plugin")
;; macro_optional_find_package(Sqlite)
;; macro_optional_find_package(SRTP)
;; " "http://srtp.sourceforge.net/srtp.html" FALSE "" "Required for the Jabber protocol with libjingle support")
;; macro_optional_find_package(Xmms)
;; macro_optional_find_package(ZLIB)
    (home-page "http://www.kde.org/")
    (synopsis "
KDE Internet Messenger
instant messaging and chat application")
    (description "
Kopete is a flexible and extendable multiple protocol instant messaging
system designed as a plugin-based system.

All protocols are plugins and allow modular installment, configuration,
and usage without the main application knowing anything about the plugin
being loaded.

The goal of Kopete is to provide users with a standard and easy to use
interface for all of their instant messaging systems, but at the same
time also providing developers with the ease of writing plugins to support
new protocols.

The core Kopete development team provides a handful of plugins that most
users can use, in addition to templates for new developers to base a plugin
off of.

Kopete is an instant messaging and chat application with support for a wide
variety of services, such as AIM, Yahoo, ICQ, MSN, and Jabber.  Advanced
features and additional protocols are available as plugins.

This package contains main Kopete library which is used by Kopete and its
plugins.

This package is part of the KDE networking module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

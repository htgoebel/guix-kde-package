(define-public libkcddb
  (package
    (name "libkcddb")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkcddb-" version ".tar.xz"))
      (sha256
       (base32 "0iaascv9a5g8681mjc6b7f2fd8fdi9p3dx8l9lapkpzcygy1fwpc"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcodecs" ,kcodecs)
      ("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; # handled by release scripts
;; find_package(MusicBrainz5)
    (home-page "")
    (synopsis "
libkcddb library
CDDB library for KDE Platform (runtime)")
    (description "
A library for retrieving and sending cddb information.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

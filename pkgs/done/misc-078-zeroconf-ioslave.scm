(define-public zeroconf-ioslave
  (package
    (name "zeroconf-ioslave")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/zeroconf-ioslave-" version ".tar.xz"))
       (sha256
        (base32 "0p7kfx7bg3yvd44vg608s2znzfahkihan67zgyf3gmjllbzvp55b"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `())
    (home-page "")
    (synopsis "DNS-SD Service Discovery Monitor")
    (description "DNS-SD Service Discovery Monitor.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

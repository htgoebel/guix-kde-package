(define-public dolphin-plugins
  (package
    (name "dolphin-plugins")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/dolphin-plugins-" version ".tar.xz"))
      (sha256
       (base32 "18azlmzw33praz4z6lamamj79gyxbbdgz7lp1cimxyddhmacc2x9"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; find_package(DolphinVcs)
    (home-page "http://www.kde.org/")
    (synopsis "
Plugins for dolphin
dolphin VCS plugins")
    (description "
This package contains some scripts plugins for dolphin

This package contains plugins that offer integration in Dolphin with the
following version control systems:

 * Bzr
 * Git
 * Mercurial
 * Subversion

This package is part of the KDE Software Development Kit module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

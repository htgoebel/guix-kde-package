(define-public libgravatar
  (package
    (name "libgravatar")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/libgravatar-" version ".tar.xz"))
       (sha256
        (base32 "0m726ixss72rz3gwgn7q5s34xwbghi877y7gxa1ilcrk9rhyxv2f"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kconfig" ,kconfig)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("ktextwidgets" ,ktextwidgets)
       ;; TODO: ("kpimcommon" ,kpimcommon)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("qtbase" ,qtbase)))
    (home-page "https://cgit.kde.org/libgravatar.git")
    (synopsis "Online avatar lookup library")
    (description "This library allows to retrieve avatar images based on a
hash from a person's email address, as well as local caching to avoid
unnecessary network operations.")
    (license (list license:gpl2+ license:lgpl2.0+))))

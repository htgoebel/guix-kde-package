(define-public ffmpegthumbs
  (package
    (name "ffmpegthumbs")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ffmpegthumbs-" version ".tar.xz"))
      (sha256
       (base32 "1scyyd85rs21rv3ghcxv7pw3aa2nl703gi4dpikbsd7wjmxixzq9"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kio" ,kio)
      ("qtbase" ,qtbase)))
;; find_package(FFmpeg COMPONENTS AVCODEC AVFORMAT SWSCALE)
    (home-page "http://www.kde.org/")
    (synopsis "
Video thumbnail generator for KDE4 file managers
video thumbnail generator using ffmpeg")
    (description "
FFmpegThumbs is a video thumbnails implementation for KDE4 based on
FFmpegThumbnailer.

This thumbnailer uses FFmpeg to decode frames from the video files,
so supported video formats depend on the configuration flags of ffmpeg.

This thumbnailer was designed to be as fast and lightweight as possible.

FFMpegThumbs is a video thumbnail generator for KDE file managers
like Dolphin and Konqueror.  It enables them to show preview images
of video files using FFMpeg.

This package is part of the KDE multimedia module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

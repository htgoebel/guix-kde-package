(define-public artikulate
  (package
    (name "artikulate")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/artikulate-" version ".tar.xz"))
      (sha256
       (base32 "113k6c0yrir61j258gn9n6k7fifa6g9g8wxf3zq18jy3liwdl97s"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("knewstuff" ,knewstuff)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtxmlpatterns" ,qtxmlpatterns)))
    (home-page "https://kde.org/applications/education/")
    (synopsis "
Learning software to improve pronunciation skills
Language learning application")
    (description "
Artikulate is a learning software that helps improving pronunciation skills
by listening to native speakers.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

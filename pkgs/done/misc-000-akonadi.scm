(define-public akonadi
  (package
    (name "akonadi")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/akonadi-" version ".tar.xz"))
      (sha256
       (base32 "00sbchj3yjbqdjckrciv2c7j9i7kc5yqvyvbmjlxacbkq80a4j7w"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
    (inputs
     `(("boost" ,boost)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("kdesignerplugin" ,kdesignerplugin)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kitemmodels" ,kitemmodels)
       ("kitemviews" ,kitemviews)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("kxmlgui" ,kxmlgui)
       ("libxml2" ,libxml2)
       ("libxslt" ,libxslt)
       ("mysql" ,mysql) ;; only needed at runtime by the mysql backend
       ("postgres" ,postgres) ;; only needed at runtime by the PostgreSQL backend"
       ("sqlite" ,sqlite)
       ("qtbase" ,qtbase)
       ("shared-mime-info" ,shared-mime-info)))
    (home-page "http://pim.kde.org/akonadi")
    (synopsis "Extensible cross-desktop storage service for PIM")
    (description "Akonadi is an extensible cross-desktop Personal Information
Management (PIM) storage service.  It provides a common framework for
applications to store and access mail, calendars, addressbooks, and other PIM
data.

This package contains the Akonadi PIM storage server and associated
programs.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))


(define-public elisa
  (package
    (name "elisa")
    (version "0.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/elisa/0.12"
                           "/elisa-" version ".tar.bz2"))
       (sha256
        (base32 "0wzdxym4fc83wvqyhcwid65yv59a2wvp1lq303cn124mpnlwx62y"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
       ("kcoreaddons" ,kcoreaddons)
       ("kdelibs4support" ,kdelibs4support)
       ("ki18n" ,ki18n)
       ("kinit" ,kinit)
       ("kio" ,kio)
       ("kparts" ,kparts)
       ("kmediaplayer" ,kmediaplayer)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("phonon" ,phonon)
       ("qtbase" ,qtbase)
       ("cairo" ,cairo)
       ("qtsvg" ,qtsvg)
       ("qtx11extras" ,qtx11extras)))
    (home-page "https://kde.org/applications/multimedia/org.kde.elisa")
    (synopsis "")
    (description "")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

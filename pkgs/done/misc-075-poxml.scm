(define-public poxml
  (package
    (name "poxml")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/poxml-" version ".tar.xz"))
      (sha256
       (base32 "1cpc49hnslc2iabgnvda7967mmrdzykjydi8py67ycr9k1gx6pm5"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("qtbase" ,qtbase)))
;; find_package(GettextPO REQUIRED)
;; find_package(Gettext)
    (home-page "http://www.kde.org/")
    (synopsis "
Xml2po and vice versa converters
tools for translating DocBook XML files with Gettext")
    (description "
Software Development Kit for the K Desktop Environment.

This is a collection of tools that facilitate translating DocBook XML
files using Gettext message files (PO files).

Also included are several command-line utilities for manipulating DocBook XML
files, PO files and PO template files.

This package is part of the KDE Software Development Kit module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

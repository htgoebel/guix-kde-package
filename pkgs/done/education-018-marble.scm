(define-public marble
  (package
    (name "marble")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/marble-" version ".tar.xz"))
      (sha256
       (base32 "08dykrmiq1jk9yv83sjj6s3gps56bw0hxjbvb90bzd1g0kh0c82j"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtlocation" ,qtlocation)
      ("qtmultimedia" ,qtmultimedia)
      ("qtsvg" ,qtsvg)
      ("qtwebkit" ,qtwebkit)))
;; elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel" AND NOT WIN32)
;;
;;
;;
;; ENABLE_TESTING()
;; elseif(APPLE)
;; elseif(CMAKE_SYSTEM_NAME STREQUAL Android)
;;  # Linux / bsd etc...
;; ?")
;;         REMOVE_DEFINITIONS( -DQT_NO_DEBUG )
    (home-page "https://kde.org/applications/education/org.kde.marble")
    (synopsis "
A virtual globe and world atlas
globe and map widget")
    (description "
Marble is a Virtual Globe and World Atlas that you can use to learn more
about Earth: You can pan and zoom around and you can look up places and
roads. A mouse click on a place label will provide the respective
Wikipedia article.

Marble is a generic geographical map widget and framework for KDE
applications. The Marble widget shows the earth as a sphere but does not
require hardware acceleration.  A minimal set of geographical data is
included, so it can be used without an internet connection.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

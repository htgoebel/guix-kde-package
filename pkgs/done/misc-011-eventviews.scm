(define-public eventviews
  (package
    (name "eventviews")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/eventviews-" version ".tar.xz"))
      (sha256
       (base32 "0z8jznvw2nhszrlll7458gb4r0585ivkbq9dqplkw2mdrv7vxz5c"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
    (inputs
     `(("kcodecs" ,kcodecs)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; find_package(KF5 Akonadi ${AKONADI_LIB_VERSION})
;; find_package(KF5 Libkdepim ${LIBKDEPIM_LIB_VERSION})
;; find_package(KGantt ${KDIAGRAM_LIB_VERSION} CONFIG REQUIRED)
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 CalendarCore ${KCALENDARCORE_LIB_VERSION})
;; find_package(KF5 CalendarSupport ${CALENDARSUPPORT_LIB_VERSION})
;; find_package(KF5 AkonadiCalendar ${AKONADICALENDAR_LIB_VERSION})
;; find_package(KF5 Mime)
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
    (home-page "")
    (synopsis "
KDEPIM 4 library
event viewing library")
    (description "
KDEPIM 4 library.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public okular
  (package
    (name "okular")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/okular-" version ".tar.xz"))
       (sha256
        (base32 "1gilr9nviv51pcnmqdfw7834knvyfd11ggdjvinxvbpz61832niv"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kactivities" ,kactivities)
       ("karchive" ,karchive)
       ("kbookmarks" ,kbookmarks)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("khtml" ,khtml)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kjs" ,kjs)
       ("kparts" ,kparts)
       ("kwallet" ,kwallet)
       ("kwindowsystem" ,kwindowsystem)
       ("libz" ,libz)
       ("phonon" ,phonon)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtspeech" ,qtspeech)
       ("qtsvg" ,qtsvg)
       ("threadweaver" ,threadweaver)))
    (home-page "http://www.kde.org/")
    (synopsis "Universal document viewer")
    (description "Okular supports different kind of documents, like PDF,
Postscript, DjVu, CHM, and others.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

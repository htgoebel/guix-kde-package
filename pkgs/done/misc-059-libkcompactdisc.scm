(define-public libkcompactdisc
  (package
    (name "libkcompactdisc")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkcompactdisc-" version ".tar.xz"))
      (sha256
       (base32 "021yws9854w6fwwfw31b87rpz92ach5xyq427968m3mc3c430d4l"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("solid" ,solid)))
;; ﻿cmake_minimum_required(VERSION 2.8.12)
;;  # handled by release scripts
    (home-page "")
    (synopsis "
KDE library for playing & ripping CDs
CD drive library for KDE Platform (runtime)")
    (description "
KDE library for playing & ripping CDs.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

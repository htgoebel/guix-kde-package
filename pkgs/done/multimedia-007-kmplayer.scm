(define-public kmplayer
  (package
    (name "kmplayer")
    (version "0.12.0b")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/kmplayer/0.12/kmplayer-" version
                           ".tar.bz2"))
       (sha256
        (base32 "0wzdxym4fc83wvqyhcwid65yv59a2wvp1lq303cn124mpnlwx62y"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kconfig" ,kconfig)
       ("kcoreaddons" ,kcoreaddons)
       ("kdelibs4support" ,kdelibs4support)
       ("ki18n" ,ki18n)
       ("kinit" ,kinit)
       ("kio" ,kio)
       ("kparts" ,kparts)
       ("kmediaplayer" ,kmediaplayer)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("phonon" ,phonon)
       ("qtbase" ,qtbase)
       ("cairo" ,cairo)
                                        ;gmodule-2.0
                                        ;gtk+-x11
                                        ;gthread
                                        ;dbus-glib-1
       ("qtsvg" ,qtsvg)
       ("qtx11extras" ,qtx11extras)))
    (home-page "http://kmplayer.kde.org")
    (synopsis "Media player using mplayer/phonon as backend")
    (description "
Kmplayer can play all the audio/video supported by mplayer/phonon from a
local file or URL and be embedded in Konqueror and KHTML.
It also plays DVDs.

KMPlayer is a simple frontend for MPlayer/FFMpeg/Phonon.

Some features:
 * play DVD/VCD movies (from file or url and from a video device)
 * embed inside konqueror (movie is played inside konqueror)
 * embed inside khtml (movie playback inside a html page)
 * Movie recording using mencoder (part of the mplayer package)
 * No video during recording, but you can always open a new window and play it
 * Broadcasting, http streaming, using ffserver/ffmpeg
 * For TV sources, you need v4lctl (part of the xawtv package)")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

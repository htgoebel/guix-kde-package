(define-public kwordquiz
  (package
    (name "kwordquiz")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kwordquiz-" version ".tar.xz"))
      (sha256
       (base32 "1r8q2d6j7bq8jdr4cl9maapadzg7yp0zldjxkcqg08ldwsrrsnqj"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcrash" ,kcrash)
      ("kdelibs4support" ,kdelibs4support)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kitemviews" ,kitemviews)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kxmlgui" ,kxmlgui)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)))
;; find_package(LibKEduVocDocument REQUIRED)
    (home-page "https://kde.org/applications/education/org.kde.kwordquiz")
    (synopsis "
A general purpose flash card program
flashcard learning program")
    (description "
KWordQuiz is a general purpose flash card program. It can be used for
vocabulary learning and many other subjects. If you need more advanced
language learning features, please try KVocTrain.

KWordQuiz is a general purpose flashcard program, typically used for
vocabulary training.

KWordQuiz can open vocabulary data in various formats, including the kvtml
format used by KDE programs such as Parley, the WQL format used by
WordQuiz for Windows, the xml.gz format used by Pauker, and CSV text.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

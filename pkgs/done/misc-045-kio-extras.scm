(define-public kio-extras
  (package
    (name "kio-extras")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/kio-extras-" version ".tar.xz"))
       (sha256
        (base32 "1mfpllrmc88khlpg3yd4sghs8igg8dh0x568jw46vv90qgdb9xss"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kactivities" ,kactivities)
       ("karchive" ,karchive)
       ("kbookmarks" ,kbookmarks)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("kdelibs4support" ,kdelibs4support)
       ("kdnssd" ,kdnssd)
       ("kguiaddons" ,kguiaddons)
       ("khtml" ,khtml)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kpty" ,kpty)
       ("phonon" ,phonon)
       ("qtbase" ,qtbase)
       ("qtsvg" ,qtsvg)
       ("solid" ,solid)))
;; find_package(SLP)
;;     find_package(Samba)
;; find_package(LibSSH 0.6.0)
;; find_package(Mtp)
    (home-page "")
    (synopsis "Extra functionality for kioslaves.")
    (description "Extra kio plugins by KF5")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

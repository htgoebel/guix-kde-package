(define-public ktorrent
  (package
    (name "ktorrent")
    (version "5.0.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/ktorrent/5.0/ktorrent-" version
                          ".tar.xz"))
      (sha256
       (base32 "1rbr932djmn1la6vs2sy1zdf39fmla8vwzfn76h7csncbp5fw3yh"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kross" ,kross)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtscript" ,qtscript)
      ("solid" ,solid)
      ("sonnet" ,sonnet)))
;; find_package(LibKTorrent 1.9.50 CONFIG REQUIRED)
;; find_package(Boost REQUIRED)
;;  #XFS
    (home-page "http://ktorrent.org/")
    (synopsis "
BitTorrent program for KDE
BitTorrent client based on the KDE platform")
    (description "
KTorrent is a BitTorrent program for KDE. Its main features are:
 o Downloads torrent files
 o Upload speed capping, seeing that most people can't upload
   infinite amounts of data.
 o Internet searching using  The Bittorrent website's search engine
 o UDP Trackers

This package contains KTorrent, a BitTorrent peer-to-peer network client, that
is based on the KDE platform. Obviously, KTorrent supports such basic features
as downloading, uploading and seeding files on the BitTorrent network.
However, lots of other additional features and intuitive GUI should make
KTorrent a good choice for everyone. Some features are available as plugins
hence you should make sure you have the ones you need enabled.
  - Support for HTTP and UDP trackers, trackerless DHT (mainline) and webseeds.
  - Alternative UI support including Web interface and Plasma widget.
  - Torrent grouping, speed capping, various download prioritization
    capabilities on both torrent and file level as well as bandwidth
    scheduling.
  - Support for fetching torrent files from many sources including any local
    file or remote URL, RSS feeds (with filtering) or actively monitored
    directory etc.
  - Integrated and customizable torrent search on the Web.
  - Various security features like IP blocking and protocol encryption.
  - Disk space preallocation to reduce fragmentation.
  - uTorrent compatible peer exchange.
  - Autoconfiguration for LANs like Zeroconf extension and port forwarding via
    uPnP.
  - Scripting support via Kross and interprocess control via DBus interface.
  - SOCKSv4 and SOCKSv5 proxy, IPv6 support.
  - Lots of other useful built-in features and plugins.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kmplot
  (package
    (name "kmplot")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kmplot-" version ".tar.xz"))
      (sha256
       (base32 "02vh4diqs4688p2xlia437jywx89yhpaf8ipprdq92q034glybyz"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kparts" ,kparts)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
    (home-page "https://kde.org/applications/education/org.kde.kmplot")
    (synopsis "
A mathematical function plotter
mathematical function plotter for KDE")
    (description "
KmPlot is a mathematical function plotter for the KDE-Desktop.

It has a built in powerful parser. You can plot different functions
simultaneously and combine their function terms to build new functions.
KmPlot supports functions with parameters and functions in polar
coordinates. Several grid modes are possible. Plots may be printed with
high precision in correct scale.

KmPlot is a powerful mathematical plotter KDE, capable of plotting multiple
functions simultaneously and combining them into new functions.

Cartesian, parametric, and differential functions are supported, as well as
functions using polar coordinates.  Plots are printed with high precision at
the correct aspect ratio.

KmPlot also provides numerical and visual features such as filling and
calculating the area between the plot and the first axis, finding maxima and
minima, changing function parameters dynamically, and plotting derivatives
and integral functions.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

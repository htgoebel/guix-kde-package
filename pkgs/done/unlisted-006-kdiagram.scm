;; Powerful libraries (KChart, KGantt) for creating business diagrams
(define-public kdiagram
  (package
    (name "kdiagram")
    (version "2.6.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kdiagram/" version
                          "/src/kdiagram-" version ".tar.xz"))
      (sha256
       (base32 "10hqk12wwgbiq4q5145s8v7v96j621ckq1yil9s4pihmgsnqsy02"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

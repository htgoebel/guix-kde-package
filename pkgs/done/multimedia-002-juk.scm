(define-public juk
  (package
    (name "juk")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/juk-" version ".tar.xz"))
       (sha256
        (base32 "1ssgia5sknam2hnjflzglv0khxbwyyvzm4w1wmxw04rbjzs4gi4h"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(
       ("kdbusaddons" ,kdbusaddons)
       ("taglib" ,taglib)
       ("qtbase" ,qtbase)
       ))
    (home-page "http://www.kde.org/")
    (synopsis "Music jukebox / music player")
    (description "JuK is a powerful music player capable of managing a large
music collection.

Some of JuK's features include:
@itemize
@item Music collection, playlists, and smart playlists
@item Tag editing support, including the ability to edit multiple files at once
@item Tag-based music file organization and renaming
@item CD burning support using k3b
@item Album art using Google Image Search
@end itemize

This package is part of the KDE multimedia module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

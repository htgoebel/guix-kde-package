(define-public kalzium
  (package
    (name "kalzium")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kalzium-" version ".tar.xz"))
      (sha256
       (base32 "0rlfjqfb1vpr0cdcx429nvrbkr7kc1m4ps3z3pphkajq4vlrql8i"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdelibs4support" ,kdelibs4support)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("knewstuff" ,knewstuff)
      ("kparts" ,kparts)
      ("kplotting" ,kplotting)
      ("kunitconversion" ,kunitconversion)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtscript" ,qtscript)
      ("qtsvg" ,qtsvg)
      ("solid" ,solid)))
;; find_package(Gettext REQUIRED)
;; find_package(PythonInterp REQUIRED)
;; find_package(PkgConfig)
;; find_package(OpenBabel2)
;; find_package(AvogadroLibs)
;; find_package(Eigen3)
;; find_package(OCaml)
;; find_package(Libfacile)
;; pkg_check_modules(CHEMICAL_MIME_DATA chemical-mime-data)
;;   find_package(KDEWIN32 REQUIRED)
    (home-page "https://kde.org/applications/education/org.kde.kalzium")
    (synopsis "
Shows the periodic system of the elements
periodic table and chemistry tools")
    (description "
Kalzium is an application which will show you some information about the
periodic system of the elements. Therefore you could use it as an
information database.

Kalzium is a full-featured chemistry application, including a
Periodic Table of Elements, chemical reference, chemical equation solver, and
3D molecule viewer.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

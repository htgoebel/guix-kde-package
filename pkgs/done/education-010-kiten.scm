(define-public kiten
  (package
    (name "kiten")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kiten-" version ".tar.xz"))
      (sha256
       (base32 "0skwgv3v79p5z1livjbdsg7i18ky8vc49z53dmgsgbziqvs0s2y4"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
    (home-page "https://kde.org/applications/education/org.kde.kiten")
    (synopsis "
A Japanese reference/learning tool
Japanese reference and study aid for KDE")
    (description "
Kiten is a Japanese reference/learning tool.

Kiten features:
* Search with english keyword, Japanese reading, or a Kanji string on a
  list of EDICT files.
* Search with english keyword, Japanese reading, number of strokes, grade
  number, or a Kanji on a list of KANJIDIC files.
* Comes with all necessary files.
* Very fast.
* Limit searches to only common entries.
* Nested searches of results possible.
* Compact, small, fast interface.
* Global KDE keybindings for searching highlighted strings.
* Learning dialog. (One can even open up multiple ones and have them sync
  between each other.)
* Browse Kanji by grade.
* Add Kanji to a list for later learning.
* Browse list, and get quizzed on them.

Kiten is a collection of Japanese reference tools and study aids for KDE,
including a Japanese/English dictionary, Kanji dictionary, and Kanji quiz.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

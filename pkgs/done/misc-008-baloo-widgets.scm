(define-public baloo-widgets
  (package
    (name "baloo-widgets")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/baloo-widgets-" version ".tar.xz"))
      (sha256
       (base32 "0h75zhdiylyjifdk9ikw9gpwla3p87knndc2svkci90ga7ynggvl"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("baloo" ,baloo)
      ("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; find_package(KF5 5.19 FileMetaData)
    (home-page "http://projects.kde.org/baloo-widgets")
    (synopsis "
Widgets for Baloo
Wigets for use with Baloo")
    (description "
Widgets for Baloo

Baloo is a framework for searching and managing metadata.

This package contains GUI widgets for baloo.

Baloo is part of KDE SC.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public step
  (package
    (name "step")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/step-" version ".tar.xz"))
       (sha256
        (base32 "0pyvhlfrklc2xxylb0nlnpqx5xi0pp4zyb3xbzj87wmvcw7v5n6r"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
       ("kcrash" ,kcrash)
       ("kdelibs4support" ,kdelibs4support)
       ("khtml" ,khtml)
       ("knewstuff" ,knewstuff)
       ("kplotting" ,kplotting)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtsvg" ,qtsvg)))
    ;; find_package(Eigen3 3.2.2 REQUIRED)
;; find_package(GSL)
;; find_package(Qalculate)
;;  # CACHE BOOL "Enable QT-powered features for StepCore")
    (home-page "https://kde.org/applications/education/org.kde.step")
    (synopsis "Interactive physical simulator")
    (description "With Step you can not only learn but feel how physics
works.  You place some bodies on the scene, add some forces such as gravity or
springs, then click \"Simulate\" and Step shows you how your scene will evolve
according to the laws of physics.  You can change every property of
bodies/forces in your experiment (even during simulation) and see how this
will change evolution of the experiment.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

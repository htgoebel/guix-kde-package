(define-public calendarsupport
  (package
    (name "calendarsupport")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/calendarsupport-" version ".tar.xz"))
      (sha256
       (base32 "0r30z2wzyms7m7s8y0livsfy5pj82g988bs6amaj2571hz55d8y0"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
    (inputs
     `(("kcodecs" ,kcodecs)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)))
;; find_package(KF5 Akonadi)
;; find_package(KF5 Mime)
;; find_package(KF5 AkonadiMime ${AKONADI_MIMELIB_VERSION})
;; find_package(KF5 CalendarUtils ${CALENDARUTILS_LIB_VERSION})
;; find_package(KF5 CalendarCore ${KCALENDARCORE_LIB_VERSION})
;; find_package(KF5 IdentityManagement ${IDENTITYMANAGEMENT_LIB_VERSION})
;; find_package(KF5 Holidays ${KHOLIDAYS_LIB_VERSION})
;; find_package(KF5 AkonadiCalendar ${AKONADICALENDAR_LIB_VERSION})
;; find_package(KF5 PimCommon ${PIMCOMMON_LIB_VERSION})
;; find_package(KF5 KdepimDBusInterfaces ${KDEPIM_LIB_VERSION})
    (home-page "")
    (synopsis "
KDEPIM 4 library
KDE PIM Calendar support - library")
    (description "
KDEPIM 4 library.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

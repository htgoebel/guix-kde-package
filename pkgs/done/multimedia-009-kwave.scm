(define-public kwave
  (package
    (name "kwave")
    (version "0.9.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://sourceforge.net/projects/kwave/files/kwave/" version
                           "/kwave-" version "-1.tar.bz2/download"))
       (sha256
        (base32 "0mwn061lccfj02scx79lm17bh9d61fx22jq2hbc3sy3d5kniv875"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kservice" ,kservice)
       ("ktextwidgets" ,ktextwidgets)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kxmlgui" ,kxmlgui)
       ("qtbase" ,qtbase)
       ("qtmultimedia" ,qtmultimedia)))
    (home-page "http://kwave.sourceforge.net")
    (synopsis "
A sound editor for KDE
sound editor for KDE")
    (description "
Kwave is a sound editor designed for the KDE Desktop Environment.

With Kwave you can edit many sorts of wav-files including multi-channel
files. You are able to alter and play back each channel on its own.
Kwave also includes many plugins (most are still under development) to
transform the wave-file in several ways and presents a graphical view
with a complete zoom- and scroll capability.

Kwave is a sound editor designed for the KDE Desktop Environment.

With Kwave you can record, play back, import and edit many sorts of audio
files including multi-channel files.

Kwave includes some plugins to transform audio files in several ways and
presents a graphical view with a complete zoom- and scroll capability.

Its features include:
 * 24 Bit Support
 * Undo/Redo
 * Use of multicore CPUs (SMP, hyperthreading)
 * Simple Drag & Drop
 * Realtime Pre-Listen for some effects
 * Support for multi-track files
 * Playback and recording via native ALSA (or OSS, deprecated)
 * Playback via PulseAudio and Phonon
 * Load and edit-capability for large files (can use virtual memory)
 * Reading and auto-repair of damaged wav-files
 * Supports multiple windows
 * Extendable Plugin interface
 * a nice splashscreen
 * some label handling")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

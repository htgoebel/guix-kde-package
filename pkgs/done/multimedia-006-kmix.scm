(define-public kmix
  (package
    (name "kmix")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kmix-" version ".tar.xz"))
      (sha256
       (base32 "1mq4kna3z62269m43qy42knq4byrvirk0mk5yp56n51im1bmdyj4"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kconfigwidgets" ,kconfigwidgets)
       ("kdbusaddons" ,kdbusaddons)
       ("kdelibs4support" ,kdelibs4support)
       ("kglobalaccel" ,kglobalaccel)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kxmlgui" ,kxmlgui)
       ;;("pulseaudio" pulseaudio) ;; nicht phonon?
       ("glib" ,glib)
       ("alsa" ,alsa)
       ("libcanberra" ,libcanberra) ;; oder -gtk2?
       ("plasma-framework" ,plasma-framework)))
    (home-page "http://www.kde.org/")
    (synopsis "
KDE Digital Mixer
volume control and mixer")
    (description "
KMix is an application to allow you to change the volume of your sound
card. Though small, it is full-featured, and it supports several
platforms and sound drivers.

KMix is an audio device mixer, used to adjust volume, select recording inputs,
and set other hardware options.

This package is part of the KDE multimedia module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

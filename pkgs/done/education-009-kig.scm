(define-public kig
  (package
    (name "kig")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kig-" version ".tar.xz"))
      (sha256
       (base32 "0fnlgxwcnspaqzv4y40xm0kq3xwwd4r5abh7ssbd6iqsbzgm6ghw"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kparts" ,kparts)
      ("ktexteditor" ,ktexteditor)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)
      ("qtxmlpatterns" ,qtxmlpatterns)))
;; find_package(Qt5 5.5.1)
;; find_package(BoostPython)
;;   qt5_add_resources(kigpart_PART_SRCS ${CMAKE_SOURCE_DIR}/geogebra/geogebra.qrc)
;;   kde_source_files_enable_exceptions(scripting/python_scripter.cc)
    (home-page "https://kde.org/applications/education/org.kde.kig")
    (synopsis "
KDE Interactive Geometry
interactive geometry tool")
    (description "
Kig is a program for use in math classes in high school, to allow
students to interactively explore geometric concepts.

Kig is an application for interactive geometric construction, allowing
students to draw and explore mathematical figures and concepts using the
computer.

Kig supports macros and is scriptable using Python.  It can import and export
files in various formats, including SVG, Cabri, Dr. Geo, KGeo, KSeg, and XFig.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

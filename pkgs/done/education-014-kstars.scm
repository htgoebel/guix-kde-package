(define-public kstars
  (package
    (name "kstars")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kstars-" version ".tar.xz"))
      (sha256
       (base32 "0lcrn7r1nw85c0w6dg03mwf5lnsahmww60y6vwzfh2r53nbm9c1y"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("knotifications" ,knotifications)
      ("kplotting" ,kplotting)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtlocation" ,qtlocation)
      ("qtquickcontrols2" ,qtquickcontrols2)
      ("qtsvg" ,qtsvg)))
;; #QtQuickControls 2 is available only in the Qt 5.7.0
;;     find_package(Qt5 5.7)
;;
;;
;;     find_package(OpenMP REQUIRED) #Needed for LibRaw
;;
;; find_package(Eigen3 REQUIRED)
;;     find_package(CFitsio REQUIRED)
;;     find_package(CFitsio)
;;  data format in KStars.")
;; find_package(INDI 1.3.1)
;;
;; find_package(LibRaw)
;; find_package(WCSLIB)
;; find_package(Xplanet)
;; find_package(AstrometryNet)
;; find_package(OpenGL)
    (home-page "http://edu.kde.org/")
    (synopsis "
A Desktop Planetarium
desktop planetarium for KDE")
    (description "
KStars is a Desktop Planetarium for KDE. It provides an accurate graphical
simulation of the night sky, from any location on Earth, at any date and
time. The display includes 130,000 stars, 13,000 deep-sky objects,all 8
planets, the Sun and Moon, and thousands of comets and asteroids.

KStars is a desktop planetarium for KDE, depicting an accurate graphical
simulation of the night sky, from any location on Earth, at any date and time.
The display includes 130,000 stars, 13,000 deep-sky objects, all 8 planets,
the Sun and Moon, and thousands of comets and asteroids.  It includes tools
for astronomical calculations and can control telescopes and cameras.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

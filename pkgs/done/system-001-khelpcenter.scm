(define-public khelpcenter
  (package
    (name "khelpcenter")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/khelpcenter-" version ".tar.xz"))
      (sha256
       (base32 "100xcjjjbszhbwgydbylk9m9lrxikjyazmhaq2rvv2mfzsbijjm7"))))
    (properties `((tags . ("Desktop" "KDE" "System"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kbookmarks" ,kbookmarks)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("khtml" ,khtml)
      ("ki18n" ,ki18n)
      ("kinit" ,kinit)
      ("kservice" ,kservice)
      ("kwindowsystem" ,kwindowsystem)
      ("qtbase" ,qtbase)))
;; find_package(Grantlee5 REQUIRED)
;; find_package(Xapian REQUIRED)
;; find_package(LibXml2 REQUIRED)
;; target_compile_definitions(kdeinit_khelpcenter PRIVATE -DPROJECT_VERSION="${PROJECT_VERSION}")
    (home-page "")
    (synopsis "
Khelpcenter
KDE documentation viewer")
    (description "
Khelpcenter.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public libktorrent
  (package
    (name "libktorrent")
    (version "2.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/ktorrent/5.0/libktorrent-" version
                          ".tar.xz"))
      (sha256
       (base32 "171bx41c3iyic71fjw03hissij4kz0pyv1mn0mbch00djanir2s7"))))
    (properties `((tags . ("Desktop" "KDE" "Internet"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("karchive" ,karchive)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("solid" ,solid)))
;; #XFS
;; find_package(KF5 "${KF5_MIN_VERSION}")
;; find_package(Boost "${Boost_MIN_VERSION}" REQUIRED)
;; find_package(LibGMP "${LibGMP_MIN_VERSION}" REQUIRED)
;; find_package(LibGcrypt "${LibGcrypt_MIN_VERSION}" REQUIRED)
;; find_package(Qca-qt5 CONFIG REQUIRED)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

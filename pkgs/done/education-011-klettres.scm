(define-public klettres
  (package
    (name "klettres")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/klettres-" version ".tar.xz"))
      (sha256
       (base32 "0m3k3zyrw7rwm6ad75c86bap80v2y5k938mdhqjaciglqc9pk83h"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcompletion" ,kcompletion)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kemoticons" ,kemoticons)
      ("ki18n" ,ki18n)
      ("knewstuff" ,knewstuff)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("phonon" ,phonon)
      ("qtbase" ,qtbase)
      ("qtsvg" ,qtsvg)))
    (home-page "https://kde.org/applications/education/org.kde.klettres")
    (synopsis "
Language learning program
foreign alphabet tutor for KDE")
    (description "
KLettres aims to help to learn the alphabet and then to read some syllables
in different languages. It is meant to help learning the very first sounds
of a new language, for children or for adults.

KLettres is an aid for learning how to read and pronounce the alphabet of a
foreign language.

Seven languages are currently available: Czech, Danish, Dutch, English,
French, Italian and Slovak.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

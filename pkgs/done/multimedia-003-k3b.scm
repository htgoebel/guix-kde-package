(define-public k3b
  (package
    (name "k3b")
    (version "2.0.3a")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/k3b/k3b-" version
                           ".tar.xz"))
       (sha256
        (base32 "10f07465g9860chfnvrp9w3m686g6j9f446xgnnx7h82d1sb42rd"))))
    (properties `((tags . ("Desktop" "KDE" "Multimedia"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(
       ("ffmpeg" ,ffmpeg)
       ("flac" ,flac)
       ("kcddb" ,kcddb)
       ("lame" ,lame)
       ("libdvdread" ,libdvdread)
       ("libmad" ,libmad)
       ("libmusicbrainz" ,libmusicbrainz)
       ("libsamplerate" ,libsamplerate
       ("libsndfile" ,libsndfile)
       ("libvorbis" ,libvorbis)
       ("taglib" ,taglib)
       ;; muse??
       ))
    (home-page "http://www.k3b.org")
    (synopsis "Sophisticated CD/DVD burning application")
    (description "K3b is CD-writing software which intends to be feature-rich
and provide an easily usable interface.  Features include burning audio CDs
from .WAV and .MP3 audio files, configuring external programs and configuring
devices.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

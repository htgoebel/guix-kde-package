(define-public kgeography
  (package
    (name "kgeography")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kgeography-" version ".tar.xz"))
      (sha256
       (base32 "1rnk00nj29zimpw36vhm0yrwlmpmxwv9wzxnhr7n2jq5qhbqsp5g"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kitemviews" ,kitemviews)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
    (home-page "hhttps://kde.org/applications/education/org.kde.kgeography")
    (synopsis "
A geography learning program
geography learning aid for KDE")
    (description "
KGeography is a geography learning program.

KGeography is an aid for learning about world geography.  You can use it to
explore a map, show information about regions and features, and play quiz
games to test your geography knowledge.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

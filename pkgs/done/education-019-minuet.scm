(define-public minuet
  (package
    (name "minuet")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/minuet-" version ".tar.xz"))
      (sha256
       (base32 "15c0mw8qj7r6192h39lvsmiy3pqlfqzari4qjg2x44j5gq02ab3c"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtquickcontrols2" ,qtquickcontrols2)
      ("qtsvg" ,qtsvg)))
    (home-page "https://kde.org/applications/education/org.kde.minuet")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

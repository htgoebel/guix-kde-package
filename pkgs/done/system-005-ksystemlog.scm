(define-public ksystemlog
  (package
    (name "ksystemlog")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ksystemlog-" version ".tar.xz"))
      (sha256
       (base32 "0xd6pp02k84a1r6gy10x0br33g4awpbhx45j7n69l1s96szj4aki"))))
    (properties `((tags . ("Desktop" "KDE" "System"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)))
;; find_package(Journald)
    (home-page "http://www.kde.org/")
    (synopsis "
System log viewer tool for KDE 4
system log viewer")
    (description "
This program is developed for being used by beginner users,
which don't know how to find information about their Linux system,
and how the log files are in their computer.
But it is also designed for advanced users,
who want to quickly see problems occuring on their server.

KSystemLog has the following features :

- View all the main log of your system, by selecting them
 directly in a menu
 - Auto display new lines logged in bold
 - Tabbed view to allow displaying several logs at the same time
 - Saving in a file and copying to clipboard the selected log
   lines are also implemented (and email to your friends)
   - It can parse the following log files of your system :

   - X.org (or Xfree) logs
   - System logs (using the Syslog parser of KSystemLog)
   - Kernel logs
   - Daemons logs
   - Cron logs
   - Boot logs
   - Authentication logs
   - Cups logs
   - ACPID logs

KSystemLog show all logs of your system, grouped by General (Default system
log, Authentication, Kernel, X.org...), and optional Services (Apache, Cups,
etc, ...). It includes many features to read nicely your log files:
 * Colorize log lines depending on their severities
 * Tabbed view to allow displaying several logs at the same time
 * Auto display new lines logged
 * Detailed information for each log lines

This package is part of the KDE administration module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

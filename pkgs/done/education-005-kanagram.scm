(define-public kanagram
  (package
    (name "kanagram")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kanagram-" version ".tar.xz"))
      (sha256
       (base32 "183hkxxwf7h335gmfvi2lppsyhpv80lvlg1fg4whq79f1d2lrw47"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knewstuff" ,knewstuff)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("sonnet" ,sonnet)))
;; find_package(Qt5 TextToSpeech)
;; find_package(LibKEduVocDocument REQUIRED)
    (home-page "https://kde.org/applications/education/org.kde.kanagram")
    (synopsis "
Word learning program
jumble word puzzle")
    (description "
Kanagram is a replacement for KMessedWords. Kanagram mixes up the letters
of a word (creating an anagram), and you have to guess what the mixed up
word is. Kanagram features several built-in word lists, hints, and a cheat
feature which reveals the original word. Kanagram also has a vocabulary
editor, so you can make your own vocabularies, and distribute them through
Kanagram's KNewStuff download service.

KAnagram is a game where a random word is shown with its letters scrambled.
To win, the player must rearrange the letters into the correct order.

This package is part of the KDE education module")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

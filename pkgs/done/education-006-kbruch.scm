(define-public kbruch
  (package
    (name "kbruch")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kbruch-" version ".tar.xz"))
      (sha256
       (base32 "12zmp9ix8q9mf3a4xpbsr0y9h4g1srwx48604x1phdwwdhgx0gsj"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
    (home-page "https://kde.org/applications/education/org.kde.kbruch")
    (synopsis "
Practice calculating with fractions
fraction learning aid for KDE")
    (description "
KBruch is a small program to practice calculating with fractions.

KBruch is an aid for learning how to calculate with fractions.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

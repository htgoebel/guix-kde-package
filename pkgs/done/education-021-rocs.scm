(define-public rocs
  (package
    (name "rocs")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/rocs-" version ".tar.xz"))
      (sha256
       (base32 "17iz9ql988mj1vsvywd8w5w7qmbncxal71maf3rldadwmadkvzbl"))))
    (properties `((tags . ("Desktop" "KDE" "Education"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("karchive" ,karchive)
      ("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kitemviews" ,kitemviews)
      ("ktexteditor" ,ktexteditor)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtscript" ,qtscript)
      ("qtsvg" ,qtsvg)
      ("qtwebkit" ,qtwebkit)
      ("qtxmlpatterns" ,qtxmlpatterns)))
;; find_package(Boost "1.49" REQUIRED)
;; find_package(Grantlee5 "5.0.0" REQUIRED)
;; set_package_info(Boost "Boost C++ Libraries" "http://www.boost.org")
    (home-page "https://kde.org/applications/education/org.kde.rocs")
    (synopsis "
Graph Editor and a Programming Environement
Process server with telnet console and log access")
    (description "
Rocs aims to be a Graph Theory IDE for helping professors to show the results
of a graph algorithm and also helping students to do the algorithms.
Rocs has a scripting module, done in Qt Script, that interacts with the drawn
graph and every change in the graph with the script is reflected on the drawn
one.

Rocs aims to be a Graph Theory IDE for helping professors to show the results
of a graph algorithm and also helping students to do the algorithms.

Rocs has a scripting module, done in Qt Script, that interacts with the drawn
graph and every change in the graph with the script is reflected on the drawn
one.

This package is part of the KDE education module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

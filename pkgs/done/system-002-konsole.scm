(define-public konsole
  (package
    (name "konsole")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/konsole-" version ".tar.xz"))
      (sha256
       (base32 "10k7ryvsssbskpxk04iqx2mrp2a91291r8nzvg1780lrhql5rdj7"))))
    (properties `((tags . ("Desktop" "KDE" "System"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kbookmarks" ,kbookmarks)
      ("kcompletion" ,kcompletion)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("kguiaddons" ,kguiaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kinit" ,kinit)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("kparts" ,kparts)
      ("kpty" ,kpty)
      ("kservice" ,kservice)
      ("ktextwidgets" ,ktextwidgets)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtscript" ,qtscript)))
;; find_package(X11)
    (home-page "http://www.kde.org/")
    (synopsis "
A terminal emulator similar to xterm for KDE
X terminal emulator")
    (description "
A terminal emulator, similar to xterm, for KDE.

Konsole is a terminal emulator built on the KDE Platform. It can contain
multiple terminal sessions inside one window using detachable tabs.

Konsole supports powerful terminal features, such as customizable schemes,
saved sessions, and output monitoring.

This package is part of the KDE base applications module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public libkgeomap
  (package
    (name "libkgeomap")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkgeomap-" version ".tar.xz"))
      (sha256
       (base32 "1zz2w7cbabyrvzvw2ph38mxw7khyhjzg86na2lcywla7japlbsd9"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)
      ("qtwebkit" ,qtwebkit)))
;; find_package(Marble CONFIG REQUIRED)
    (home-page "")
    (synopsis "
Runtime library for digikam
")
    (description "
Librairie File needed by digikam

Libkgeomap is a wrapper around world map components as Marble, OpenstreetMap and
GoogleMap,for browsing and arranging photos on a map.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public libkeduvocdocument
  (package
    (name "libkeduvocdocument")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkeduvocdocument-" version ".tar.xz"))
      (sha256
       (base32 "05s79q269m5s78zjwxljxvprrqvpalf6h38n90m589vks82ahxx0"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("karchive" ,karchive)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; find_package(Qt5 5.2)
;; find_package(KF5 5.3.0)
    (home-page "")
    (synopsis "
Runtime library for KDE Education Application
library for reading and writing vocabulary files")
    (description "
Runtime library for KDE Education Application

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

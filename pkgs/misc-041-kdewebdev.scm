(define-public kdewebdev
  (package
    (name "kdewebdev")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kdewebdev-" version ".tar.xz"))
      (sha256
       (base32 "0r9qsm3idfhm7aglik7whzcwjjn572gwmlcgxy2cvpn15nqa344l"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
    (home-page "")
    (synopsis "
A web editor for the KDE Desktop Environment
")
    (description "
A web editor for the KDE Desktop Environment.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

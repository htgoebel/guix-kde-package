(define-public colord-kde
  (package
    (name "colord-kde")
    (version "0.5.0")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/colord-kde/" version
                          "/src/colord-kde-" version ".tar.xz"))
      (sha256
       (base32 "0brdnpflm95vf4l41clrqxwvjrdwhs859n7401wxcykkmw4m0m3c"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kcmutils" ,kcmutils)
      ("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kitemviews" ,kitemviews)
      ("knotifications" ,knotifications)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("plasma-framework" ,plasma-framework)
      ("qtbase" ,qtbase)
      ("qtx11extras" ,qtx11extras)))
;; find_package(X11)
;; find_package(XCB COMPONENTS XCB RANDR)
;; pkg_check_modules(LCMS2 REQUIRED lcms2)
    (home-page "")
    (synopsis "
Interfaces and session daemon to colord for KDE
")
    (description "
Interfaces and session daemon to colord for KDE.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

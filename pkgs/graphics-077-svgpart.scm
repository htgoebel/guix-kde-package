(define-public svgpart
  (package
    (name "svgpart")
    (version "16.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kde/stable/applications/" version
                           "/src/svgpart-" version ".tar.xz"))
       (sha256
        (base32 "0frzqp504dzqwqs9lh544xxa8i6sqi6qj533mqbzkqbjx310ka3w"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
       ("kparts" ,kparts)
       ("qtbase" ,qtbase)
       ("qtsvg" ,qtsvg)))
    (home-page "https://projects.kde.org/projects/kde/kdegraphics/svgpart")
    (synopsis "KDE SVG KPart")
    (description "SvgPart is a small KDE KPart component to display SVG images
in Gwenview and in any other KDE application which uses the KPart system.

This package is part of the KDE graphics module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kde-dev-utils
  (package
    (name "kde-dev-utils")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kde-dev-utils-" version ".tar.xz"))
      (sha256
       (base32 "0svbl7yd5vz285gaymxy5mz0ll6fviamrbd6fjhj7rc4qx1gjgin"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
    (home-page "")
    (synopsis "
K Desktop Environment - Software Development Kit
")
    (description "
Software Development Kit for the K Desktop Environment.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

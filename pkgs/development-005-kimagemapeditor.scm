(define-public kimagemapeditor
  (package
    (name "kimagemapeditor")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kimagemapeditor-" version ".tar.xz"))
      (sha256
       (base32 "0362zcj6by3kydr5v3sr7l6k9kkyfcy16879f93d1qqkjfi11cic"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; set_target_properties(kimagemapeditor_bin PROPERTIES OUTPUT_NAME kimagemapeditor)
    (home-page "")
    (synopsis "
HTML imagemap editor for KDE
HTML image map editor")
    (description "
KImageMapEditor is an HTML imagemap editor for KDE. It allows you to create and
modify HTML imagemaps.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

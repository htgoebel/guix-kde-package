(define-public libkface
  (package
    (name "libkface")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkface-" version ".tar.xz"))
      (sha256
       (base32 "068xixlw0hfhi3c9nxik2y6xyci1ilwwfq4sjm1paqfszp0f4rq8"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("qtbase" ,qtbase)))
;; " OFF)
;;     DETECT_OPENCV(${OPENCV_MIN_VERSION} core face highgui objdetect imgproc)
;;     DETECT_OPENCV(${OPENCV_MIN_VERSION} core highgui objdetect contrib legacy imgproc)
    (home-page "")
    (synopsis "
Runtime library for digikam
")
    (description "
Librairie File needed by digikam

Libkface is a Qt/C++ wrapper around LibFace library to perform face recognition
and detection over pictures.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

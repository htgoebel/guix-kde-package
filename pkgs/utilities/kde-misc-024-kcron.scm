(define-public kcron
  (package
    (name "kcron")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kcron-" version ".tar.xz"))
      (sha256
       (base32 "0whxc9h7qn0fwcca9sq6qy0j4hfb8xlkdypnb6gspl687ws799xz"))))
    (properties `((tags . ("Desktop" "KDE" "Unsorted"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfigwidgets" ,kconfigwidgets)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
    (home-page "http://www.kde.org/")
    (synopsis "
Graphical task scheduler
program scheduler frontend - transitional package")
    (description "
Kcron is a graphical frontend to the cron system, used to schedule regular
tasks on a Unix system.

This package helps migrating from the kcron standalone app to the KDE
config module in kde-config-cron.

It is a transitional package that can be safely removed.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

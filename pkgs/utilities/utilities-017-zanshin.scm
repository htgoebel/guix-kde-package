(define-public zanshin
  (package
    (name "zanshin")
    (version "0.4.1")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "http://files.kde.org/zanshin/zanshin-" version
                          ".tar.bz2"))
      (sha256
       (base32 "1llqm4w4mhmdirgrmbgwrpqyn47phwggjdghf164k3qw0bi806as"))))
    (properties `((tags . ("Desktop" "KDE" "Utilities"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("krunner" ,krunner)
      ("kwallet" ,kwallet)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)))
;; find_package(Boost REQUIRED)
;;  AND
;; 	    "${CMAKE_CXX_COMPILER_VERSION}" VERSION_LESS "6.0.0.6000058")
;;  # Enable C++14, with cmake >= 3.1
;;  # Don't enable gcc-specific extensions
;;    link_libraries("asan")
;; find_package(KF5 AkonadiCalendar AkonadiNotes AkonadiSearch IdentityManagement KontactInterface Ldap)
;; find_package(KF5 Akonadi "5.1")
    (home-page "")
    (synopsis "
Getting Things Done application
to-do list manager")
    (description "
A Getting Things Done application which aims at getting your mind like water.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

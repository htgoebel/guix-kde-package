(define-public kmousetool
  (package
    (name "kmousetool")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kmousetool-" version ".tar.xz"))
      (sha256
       (base32 "1678sjj96f22sn60ccyj6hqi2vghkf4facnx8l15x4xx05yq1vgg"))))
    (properties `((tags . ("Desktop" "KDE" "Utilities"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
    (home-page "http://www.kde.org")
    (synopsis "
Automatic Mouse Click
mouse manipulation tool for the disabled")
    (description "
KMouseTool is a tool that clicks the mouse for you.

KMouseTool clicks the mouse whenever the mouse cursor pauses briefly. It was
designed to help those with repetitive strain injuries, for whom pressing
buttons hurts.

This package is part of the KDE accessibility module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

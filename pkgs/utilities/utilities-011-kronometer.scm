(define-public kronometer
  (package
    (name "kronometer")
    (version "2.1.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/kronometer/" version
                          "/src/kronometer-" version ".tar.xz"))
      (sha256
       (base32 "02rjlncvxva34f4m3m8nklnx6xx3j7yx5cm17jq9c3kxvy8w0n83"))))
    (properties `((tags . ("Desktop" "KDE" "Utilities"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
;; kdoctools_install(po)
    (home-page "")
    (synopsis "

simple stopwatch application")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

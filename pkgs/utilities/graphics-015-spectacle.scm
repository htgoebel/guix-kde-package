(define-public spectacle
  (package
    (name "spectacle")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/spectacle-" version ".tar.xz"))
      (sha256
       (base32 "1fca71a59sgicq9zi8d0im0xpg7iz93s96h3clxxc6p493vsjkx6"))))
    (properties `((tags . ("Desktop" "KDE" "Graphics"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kcoreaddons" ,kcoreaddons)
      ("kdbusaddons" ,kdbusaddons)
      ("kdeclarative" ,kdeclarative)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("knotifications" ,knotifications)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtdeclarative" ,qtdeclarative)
      ("qtx11extras" ,qtx11extras)))
;; find_package(KF5 Kipi)
;; find_package(KDEExperimentalPurpose)
;; find_package(XCB COMPONENTS XFIXES IMAGE UTIL CURSOR)
    (home-page "")
    (synopsis "

RPM Spec file generator and management tool")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

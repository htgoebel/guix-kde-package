(define-public kate
  (package
    (name "kate")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kate-" version ".tar.xz"))
      (sha256
       (base32 "1fbrdlf64bdj71g692fkk7fiym0nm9vvbki2q105carrhl4w9s0r"))))
    (properties `((tags . ("Desktop" "KDE" "Utilities"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kactivities" ,kactivities)
      ("kconfig" ,kconfig)
      ("kcrash" ,kcrash)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kio" ,kio)
      ("kjobwidgets" ,kjobwidgets)
      ("kparts" ,kparts)
      ("ktexteditor" ,ktexteditor)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)
      ("qtscript" ,qtscript)))
;; find_package(KF5 "${KF5_DEP_VERSION}")
;; find_package(KF5 "${KF5_DEP_VERSION}")
    (home-page "http://kate-editor.org/")
    (synopsis "
Advanced text editor
powerful text editor")
    (description "
A multi-document, multi-view text editor for KDE.

Kate is a powerful text editor that can open multiple files simultaneously.

With a built-in terminal, syntax highlighting, and tabbed sidebar, it performs
as a lightweight but capable development environment.  Kate's many tools,
plugins, and scripts make it highly customizable.

Kate's features include:

 * Multiple saved sessions, each with numerous files
 * Scriptable syntax highlighting, indentation, and code-folding
 * Configurable templates and text snippets
 * Symbol viewers for C, C++, and Python
 * XML completion and validation

This package is part of the KDE 4 Base applications module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

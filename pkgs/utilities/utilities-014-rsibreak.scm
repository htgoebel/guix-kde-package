(define-public rsibreak
  (package
    (name "rsibreak")
    (version "0.12.4")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/rsibreak/0.12/rsibreak-" version
                          ".tar.xz"))
      (sha256
       (base32 "0zbj0a04rw3xwhl6llgdd1jvai5lf9wmb5q9qrz83n84cfrdrw2k"))))
    (properties `((tags . ("Desktop" "KDE" "Utilities"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
      ("kconfigwidgets" ,kconfigwidgets)
      ("kcrash" ,kcrash)
      ("kdbusaddons" ,kdbusaddons)
      ("ki18n" ,ki18n)
      ("kiconthemes" ,kiconthemes)
      ("kidletime" ,kidletime)
      ("knotifications" ,knotifications)
      ("knotifyconfig" ,knotifyconfig)
      ("ktextwidgets" ,ktextwidgets)
      ("kwindowsystem" ,kwindowsystem)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "
Assists in the Recovery and Prevention of Repetitive Strain Injury
")
    (description "
Repetitive Strain Injury is an illness which can occur as a result of
working with a mouse and keyboard. This utility can be used to remind
you to take a break now and then.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

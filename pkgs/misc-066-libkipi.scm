(define-public libkipi
  (package
    (name "libkipi")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/libkipi-" version ".tar.xz"))
      (sha256
       (base32 "0f1m0v0cm11dqwm3n9w1mwz25sj3bggx19fi0jj4ij7zqicpykz6"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kconfig" ,kconfig)
      ("kservice" ,kservice)
      ("kxmlgui" ,kxmlgui)
      ("qtbase" ,qtbase)))
    (home-page "")
    (synopsis "
Libkipi runtime library
KDE Image Plugin Interface library")
    (description "
Libkipi is an interface to use kipi-plugins from a KDE image management
program like digiKam (http://www.digikam.org).

This package provides the runtime library.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

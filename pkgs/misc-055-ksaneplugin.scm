(define-public ksaneplugin
  (package
    (name "ksaneplugin")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/ksaneplugin-" version ".tar.xz"))
      (sha256
       (base32 "1z0ziapcjmi7fqfnb0zsbjgd1q05np1s7smj1k8cd8c6f169yrld"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; find_package(KSane REQUIRED)
    (home-page "https://projects.kde.org/projects/kde/kdegraphics/ksaneplugin")
    (synopsis "
KDE Scan Service
KScan plugin for scanning through libksane")
    (description "
This is a KScan plugin that implements the scanning through libksane.

This is a KScan plugin that implements the scanning through libksane.

This package is part of the KDE graphics module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

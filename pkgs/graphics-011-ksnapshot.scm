;; no newer version?
(define-public ksnapshot
  (package
    (name "ksnapshot")
    (version "4.14.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/" version
                          "/src/ksnapshot-" version ".tar.xz"))
      (sha256
       (base32 "10grzlp7sq367g91858d16sadzipzmgwczhnb5xvy0437lqhhz7c"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 REQUIRED)
;; macro_optional_find_package(Kipi)
;; " "Support for capturing the cursor" "http://www.x.org/" FALSE "" "")
;; qt4_add_dbus_adaptor(ksnapshot_SRCS org.kde.ksnapshot.xml ksnapshot.h KSnapshot)
    (home-page "http://www.kde.org/")
    (synopsis "
KDE Screenshot Utility
transitional package for kde-spectacle")
    (description "
KSnapshot is a KDE snapshot tool with many features.
Features:
    - Save in multiple formats
    - Take new snapshot
    - Open with... possibility to open snapshot in external editor.
    - Copy to clipboard
    - Several capture modes, including selected region or single window
    - Snapshot delay

KSnapshot captures images of the screen.  It can capture the whole screen,
a specified region, an individual window, or only part of a window.

This package is part of the KDE graphics module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

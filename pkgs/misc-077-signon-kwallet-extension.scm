(define-public signon-kwallet-extension
  (package
    (name "signon-kwallet-extension")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/signon-kwallet-extension-" version ".tar.xz"))
      (sha256
       (base32 "0fnqdcin471hlw694vb6z9ybgw31778rhnryc7zps40kjwfdxhcc"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kwallet" ,kwallet)
      ("qtbase" ,qtbase)))
;; find_package(SignOnExtension REQUIRED)
    (home-page "")
    (synopsis "

KWallet extension for signond")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

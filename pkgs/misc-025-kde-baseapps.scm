(define-public kde-baseapps
  (package
    (name "kde-baseapps")
    (version "16.08.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kde-baseapps-" version ".tar.xz"))
      (sha256
       (base32 "18a2r2g5cz4s923p3369aam4qr00gsngk6nn4lk3k5a4qqavpsa7"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 4.10.60 REQUIRED)
;; find_package(ZLIB)
;; find_package(KActivities 6.1.0)
    (home-page "")
    (synopsis "

")
    (description "


")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kdesdk-thumbnailers
  (package
    (name "kdesdk-thumbnailers")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kdesdk-thumbnailers-" version ".tar.xz"))
      (sha256
       (base32 "0p2s6pyq16jmjv29r8n9ygvsh1dxgz9zk90mk138cxxhbx9nks9h"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kconfig" ,kconfig)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("qtbase" ,qtbase)))
;; find_package(GettextPO)
    (home-page "")
    (synopsis "
K Desktop Environment - Software Development Kit
")
    (description "
Software Development Kit for the K Desktop Environment.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

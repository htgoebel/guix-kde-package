(define-public kolourpaint
  (package
    (name "kolourpaint")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kolourpaint-" version ".tar.xz"))
      (sha256
       (base32 "1yg3xnbbzvhcgb7i92bya41gq4z0135njcc77848khndpgkxvczb"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `(("kdelibs4support" ,kdelibs4support)
      ("qtbase" ,qtbase)))
;; find_package(KF5 KExiv2)
;; find_package(KF5 Sane)
;;     STRING(REGEX REPLACE "\"" "\\\\\"" _contents "${_contents}" )
;;     STRING(REGEX REPLACE "\n" "\\\\n\"\n\"" _contents "${_contents}" )
;;    # kolourpaint_lib1_SRCS
;;   # kolourpaint_lib2_SRCS
;;   # set(kolourpaint_app_SRCS
    (home-page "http://www.kde.org/")
    (synopsis "
A free, easy-to-use paint program for KDE
simple image editor and drawing application")
    (description "
KolourPaint is a free, easy-to-use paint program for KDE.

It aims to be conceptually simple to understand; providing a level of
functionality targeted towards the average user. It's designed for daily
tasks like:

* Painting - drawing diagrams and "finger painting"
* Image Manipulation - editing screenshots and photos; applying effects
* Icon Editing - drawing clipart and logos with transparency

KolourPaint is a simple drawing and image editing application for KDE.
It aims to be easy to use, providing a level of functionality targeted towards
the average user. It is ideal for common tasks such as drawing simple graphics
and touching-up photos.

This package is part of the KDE graphics module.")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

(define-public kdenetwork-filesharing
  (package
    (name "kdenetwork-filesharing")
    (version "16.12.3")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/applications/" version
                          "/src/kdenetwork-filesharing-" version ".tar.xz"))
      (sha256
       (base32 "0345wq7ayahfd2jlpgfs18c7nrdp9gn9yxig2x75pspqmb5pgxh7"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
      ("ki18n" ,ki18n)
      ("kio" ,kio)
      ("kwidgetsaddons" ,kwidgetsaddons)
      ("qtbase" ,qtbase)))
;; find_package(PackageKitQt5 0.9.5)
    (home-page "")
    (synopsis "
Kdenetwork filesharing
")
    (description "
Networking applications for the KDE 4.

- kdict: graphical client for the DICT protocol
- kit: AOL instant messenger client, using the TOC protocol
- kpf: public fileserver applet
- krfb: Desktop Sharing server, allow others to access your desktop via VNC
- krdc: a client for Desktop Sharing and other VNC servers.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

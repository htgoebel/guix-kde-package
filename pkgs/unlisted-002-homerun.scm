;; Found on download-server, maybe already included in main package – An application launcher for KDE Plasma desktop
(define-public homerun
  (package
    (name "homerun")
    (version "1.2.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/homerun/src/homerun-" version
                          ".tar.xz"))
      (sha256
       (base32 "17n74rhfq6fia5254cykmfdlx1ixm8rs5mlvjz7d6knnnc8nbx93"))))
    (build-system qt-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ;("perl" ,perl)
       ;("pkg-config" ,pkg-config)
       ))
    (inputs
     `())
;; find_package(KDE4 4.10.0 REQUIRED)
;; find_package(KDE4Workspace REQUIRED)
;; find_package(KDeclarative REQUIRED)
;; find_package(LibKonq REQUIRED)
;;     find_package(Msgfmt REQUIRED)
;;     find_package(Gettext REQUIRED)
    (home-page "")
    (synopsis "
Homerun is a fullscreen launcher with content organized in tabs
")
    (description "
Homerun Kicker uses the Homerun technology to provide a traditional
application launcher. Some features include application favorites
sidebar, content organized in tabs, search, and more.
---
It is a fullscreen launcher with content organized in tabs. A tab is made of
several "sources". A source can provide one or more sections to a tab. Homerun
comes with a few built-in sources, but custom sources can be written using
libhomerun.

Homerun can either be used from a launcher button in a panel, like Kickoff, or
as a containment, like the Search and Launch containment.

")
    (license (list license:gpl2+ license:lgpl2.0+ license:fdl1.2+))))

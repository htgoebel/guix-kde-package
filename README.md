Bring KDE into Guix easily
============================================

The repo provides:

* Guix package descriptions for another about 100 KDE packages

* scripts to ease adding the package definition to the respective
  gnu/packages/*.scm file, including commit and commit-message

* Lists about KDE applications per group (education, pim, office, etc.)


Requirements: xclip


TL;DR
=========================

Here is an example how to bringing the package into Guix easily:
```
cd /path/to/your/guix-checkout
git clone https://gitlab.digitalcourage.de/htgoebel/guix-kde-package.git kde-packages
less kde-packages/00-add  # check out how this supports you
kde-packages/00-add NAME
```


Readme
========================

This repo contains prepared Guix package descriptions for another about 100 KDE packages. The idea is to take a lot of stupid work from the shoulders of those who want to add a KDE package and automatically prepare as much as possible.

Prepared are
- name, version, url, hash
- synopsis (two variants)
- description (two variants)
- license (needs to be verified)
- inputs and native-inputs
- store-path of the archive


Also provided are some scripts to ease adding the package definition to the respective gnu/packages/*.scm file. Please have a look at the 00-add.sh, which drives the process.


The synopsis and descriptions are taken from Debian and Mageia, chosen by what was easy to access for me.

The license is taken from Debian but needs to be rechecked. Maybe OpenSuSE is a good place to look since they seem to have a strong focus on this.

The inputs and native-inputs are taken from the `CMakeList.txt` file, which I processed and mapped many known requirements to Guix's package names. For your convenience unprocessed content of the `CMakeList.txt` file has been put into the description, so you hopefully find all necessary information there.

If you are going to process some packages, please submit a merge-request or an issue stating this. This should avoid duplicate work.


Tips and Tricks
======================

For resolving conflicts I personally use `git gui` and call the merge tool from there. `git gui` also provides a fine "amend" function which allows you the change the last commit.


If you need other packages to be mass-prepared, please send me a list with indented filename, package-name, version, download-url, Debian package name.


Thanks for helping and Enjoy!

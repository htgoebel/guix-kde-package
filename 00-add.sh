#!/usr/bin/env bash
# License: AGPLv3
#
# Usage:
#     ./00-add.sh [SECTION] PACKAGENAME
#   or
#     ./00-add.sh [SECTION] FILENAME.scm
#
# Example:
#     ./00-add.sh office flow
#  will add package "flow" from pkgs/*-flow.scm to gnu/packages/kde-office.scm
#
# Requires: xclip <https://github.com/astrand/xclip>,
#        available in many distros and in Guix

if [ $# -ge 2 ] ;then
    SECTION=$1; shift
fi
PKGNAME=${1?programname missing}
PKGNAME=$(basename $NAME)

BASEDIR=$(dirname "$0")

# get full filename if only package-name was given
FILE=$BASEDIR/pkgs/$PKGNAME
if [ ! -r $FILE ] ; then
    FILE=$(ls $BASEDIR/pkgs/*-$PKGNAME.scm 2> /dev/null)
    if [ -z "$FILE" ] ; then
	echo "Not found: '$NAME'"
	exit 10
    fi
    PKGNAME=$(basename $FILE)
fi

# if not given, get section-basename from filename
if [ -z "$SECTION" ] ; then
    SECTION=${NAME%%-*}             # section = first part
fi
SECTION=kde-$SECTION
SECTION_FILE=gnu/packages/$SECTION.scm


# get package-name from filename
PKGNAME=${NAME#*-}                 # remove section
PKGNAME=${NAME#[0-9][0-9][0-9]-}   # remove number (if any)
PKGNAME=${NAME%.scm}

# show whether there are uncommitted changes
git status -z gnu/packages/ | head

# display information
echo $PKGNAME "--" $SECTION "--" $FILE

#emacs +1000000 gnu/packages/$SECTION.scm
#cat $FILE >> $SECTION_FILE

# copy the file-content into the clipboard so it can be pasted into the file
# at the appropriate place
xclip -sel c  -in $FILE

# Search the project at invent.kde.org for easy access to relevant data
# This used to directly open the CMakeLists.txt in the git repo. But since KDE
# moved from cgit to gitlab, repos have be restructured. Thus you need to
# search the correct repo yourself.
xdg-open "https://invent.kde.org/search?utf8=%E2%9C%93&search=$PKGNAME&group_id=&project_id=&snippets=false&repository_ref=&nav_source=navbar"
#xdg-open https://cgit.kde.org/$PKGNAME.git/tree/CMakeLists.txt

read -p "Revert-buffer / reload file "
read -p "Insert (Paste) "
read -p "Format Source "
#read -p "qt-build-system "
read -p "Format inputs "
read -p "Summary "
read -p "Description "
# Since the move to invent, I can not open the tree for you, sorry :-(
#xdg-open https://cgit.kde.org/$PKGNAME.git/tree/
read -p "Check License "

# make guix update the package definition - since the one we have is
# outdated for sure
./pre-inst-env guix refresh -u $PKGNAME

# Commit the package draft to safe the work we've done so far.
git add $SECTION_FILE
git commit -m "gnu: Add $PKGNAME.

* $SECTION_FILE ($PKGNAME): New variable." $SECTION_FILE

# move the input file to the "done" packages
mv -v $FILE $BASEDIR/done

# Try building the package. This loops until some character is entered. This
# help refining the package definition until it is okay for you.
BUILDOKAY=
while [ -z "$BUILDOKAY" ] ; do
    ./pre-inst-env guix build $PKGNAME
    read -p "Build okay? " BUILDOKAY
done

# Try running the application in a container, wait 1 minute and kill the
# spawned dbus process.
#
# To ease debugging, the application is run under "strace", trace-output to be
# found in "/tmp/trace.log"
#
# ATTENTION: Needs adjustment when not building on a foreign distribution
sh ./00-test-gui-app.sh $PKGNAME &
sleep 60s
pgrep -u $(id -u) -f store.*profile.*container.*$PKGNAME | xargs kill
pgrep -u $(id -u) -f store.*dbus-daemon | xargs kill

# Ease amending the commit
#git gui citool --amend
